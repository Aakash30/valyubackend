const productNames = {
  ADVANCE_LOAN_PAY_PER_USE: 'advanceLoan_pay_per_use',
  ADVANCE_LOAN_SUBSCRIPTION: 'advanceLoan_subscription',
};

module.exports = productNames;
