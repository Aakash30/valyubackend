const kudosLoanConstant = {
  SECTOR: 'AL',
  INTERESET_TYPE: 'Flat',
  TENURE: '12',
  CIBIL_SCORE: '750',
  SYSTEM_SCORE: '65',
};

module.exports = kudosLoanConstant;
