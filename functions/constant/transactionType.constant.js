const transactionTypeList = {
	CREDIT: 'CREDIT',
	DEBIT: 'DEBIT',
};
module.exports = transactionTypeList;
