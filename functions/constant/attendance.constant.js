const attendanceConstantList = {
	ABSENT: 'Absent',
	PRESENT: 'Present',
};
module.exports = attendanceConstantList;
