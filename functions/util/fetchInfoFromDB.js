const rdbInstance = require('../helper/rdb');
const rdbFireStoreInstance = require('../helper/fireStore');
const admin = require('../helper/firebase');

const saveUserToFirestore = async (collectionName, key, data) => {
	await rdbFireStoreInstance.collection(collectionName).doc(key).set(data);
};

const updateUserInfoInDB = async (userData, userIdentifier) => {
	await rdbInstance.ref('users/' + userIdentifier).update(userData);
};
const getUserInformation = async (userIdentifier) => {
	let userData = rdbInstance.ref('users/' + userIdentifier);
	userData = await userData.once('value');
	userData = userData.val();
	return userData;
};

const getStatusConstantInformation = async (status) => {
	let statusData = rdbInstance.ref('Constant/loan_ID/' + status + '/');
	statusData = await statusData.once('value');
	statusData = statusData.val();

	return statusData;
};

const getLoanInformation = async (loanId) => {
	const userIdentifier = loanId.split(/[_ ]+/).pop();
	let loanData = rdbInstance.ref(`users/${userIdentifier}/Loan/${loanId}`);
	loanData = await loanData.once('value');
	loanData = loanData.val();
	return { loanData, userIdentifier };
};

const getDisbursedLoanInformation = async (userIdentifier) => {
	let loanData = rdbInstance.ref(`users/${userIdentifier}/Loan`);
	loanData = await loanData.orderByChild('status').equalTo(80).once('value');
	loanData = loanData.val();
	return loanData;
};

const getAllTransactionOnPurpose = async (userIdentifier, purpose) => {
	let transactionData = rdbInstance.ref(`users/${userIdentifier}/Transaction`);
	transactionData = await transactionData
		.orderByChild('purpose')
		.equalTo(purpose)
		.once('value');
	transactionData = transactionData.val();
	return transactionData;
};

const deleteRetryLoanID = async (loanId) => {
	let retryData = rdbInstance.ref(`retryLoans/${loanId}`);
	retryData = await retryData.remove();
};

const getErrorInformation = async (loanId) => {
	let errorData = rdbInstance.ref(`Errors/${loanId}`);
	errorData = await errorData.once('value');
	errorData = errorData.val();
	return errorData;
};

const getClientInformation = async (employerName) => {
	let employerData = rdbInstance.ref('Client/' + employerName);
	employerData = await employerData.once('value');
	employerData = employerData.val();
	return employerData;
};

const getUIConstantInformation = async () => {
	let data = rdbInstance.ref('Constant/UI');
	data = await data.once('value');
	data = data.val();
	return data;
};

const getQueryInformation = async (userIdentifier) => {
	let queryData = rdbInstance.ref('Query/' + userIdentifier);
	queryData = await queryData.once('value');
	queryData = queryData.val();
	return queryData;
};

const sendNotificationsAndSaveData = async (
	userIdentifier,
	deviceToken,
	payload,
	title,
	body
) => {
	if (deviceToken != undefined) {
		admin.messaging().sendToDevice(deviceToken, payload);
	}
	let timestamp = new Date().getTime();
	rdbInstance
		.ref('Notification/' + userIdentifier)
		.child(timestamp)
		.set({ title, body });
	console.log('Create Loan Notification Sent');
};

const updateAttendanceToDb = async (attendanceData, userIdentifier, client) => {
	if (attendanceData != undefined) {
		//attendanceData
		await rdbInstance
			.ref(`users/${userIdentifier}/workProfile/${client}/userData`)
			.child('attendance')
			.update({ ...attendanceData });
	}
	return true;
};

module.exports = {
	getUserInformation,
	getClientInformation,
	sendNotificationsAndSaveData,
	getLoanInformation,
	getQueryInformation,
	getStatusConstantInformation,
	getUIConstantInformation,
	getErrorInformation,
	deleteRetryLoanID,
	updateAttendanceToDb,
	updateUserInfoInDB,
	saveUserToFirestore,
	getDisbursedLoanInformation,
	getAllTransactionOnPurpose,
};
