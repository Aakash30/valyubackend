const productNames = require('../constant/productNames.constant');
const rdbInstance = require('../helper/rdb');
const clientList = require('../constant/clientList');
const { updateAttendanceToDb } = require('../util/fetchInfoFromDB');
const { momentInstance, momentInstanceOnDate } = require('../helper/moment');
const { getAttendanceForPS, getAttendanceForQuess } = require('../attendance');
const calculateEligibility = require('../util/calculateEligibility');
const createAttendanceData = require('../attendance/createAttendanceData');

const getEligibilityPercentageforProduct = async (
	clientData,
	productName,
	salary
) => {
	let eligibilty_percentage;
	if (productName === productNames.ADVANCE_LOAN_SUBSCRIPTION) {
		let rules = clientData[productName].rules;
		let salary_max = false;
		for (let rules_salary in rules) {
			if (rules_salary >= salary) {
				eligibilty_percentage = rules[rules_salary].eligibilty_percentage;
				salary_max = true;
				break;
			}
		}

		if (!salary_max) {
			eligibilty_percentage = rules['max'].eligibilty_percentage;
		}
	} else {
		eligibilty_percentage = clientData[productName].eligibilty_percentage;
	}

	return eligibilty_percentage;
};

const updateEligibility = async (
	userIdentifier,
	clientName,
	clientData,
	productName,
	userSalary,
	employeeId,
	doj,
	dateTillWeNeedToCalculateEligibility
) => {
	const productConfig = clientData[productName];
	let { payrollStartDate, maximum_loan_amount, payrollEndDate } = productConfig;

	let totalworkingDays = (await momentInstanceOnDate(payrollEndDate)).diff(
		payrollStartDate,
		'days'
	);
	const isNotPayrollDay =
		(await momentInstanceOnDate(momentInstance.format('YYYY-MM-DD'))).diff(
			payrollStartDate,
			'days'
		) >= 0;

	let updateData = {
		eligibility_amount: 0.0,
		available_amount: 0.0,
		creditLimit: 0.0,
		pendingPaymentTotal: 0.0,
		accrued_amount: 0.0,
		eligibility_date: dateTillWeNeedToCalculateEligibility,
		totalSuccessfullApplication: 0,
		product_name: productName,
		salary: userSalary,
		employeeId: employeeId,
	};

	//CalculateTotalDaysForEmployee in Company so that we can check if employee is eligible for loan or Not
	/*
	let totaldaysInEmployer = (
		await momentInstanceOnDate(dateTillWeNeedToCalculateEligibility)
	).diff(doj, 'days');
	if (totaldaysInEmployer <= 0) {
		console.log('Employeee Not Eligible For Loan Yet');
		throw new Error('Employee Joining date is today');
	}
	*/

	if (isNotPayrollDay) {
		updateData = await findEligibilityAndReturnUpdatedData(
			userIdentifier,
			clientData,
			productName,
			userSalary,
			payrollStartDate,
			dateTillWeNeedToCalculateEligibility,
			totalworkingDays,
			updateData,
			employeeId,
			clientName,
			maximum_loan_amount
		);
	}

	await rdbInstance
		.ref(`users/${userIdentifier}/workProfile/${clientName}/userData`)
		.update(updateData);
};

const findEligibilityAndReturnUpdatedData = async (
	userIdentifier,
	clientData,
	productName,
	userSalary,
	payrollStartDate,
	dateTillWeNeedToCalculateEligibility,
	totalworkingDays,
	updateData,
	employeeId,
	clientName,
	maximum_loan_amount
) => {
	let { clientName: clientDisplayName, defaultAttendance } = clientData;
	let perDaySalary = userSalary / totalworkingDays;
	let attendance = {};

	let eligibilty_percentage = await getEligibilityPercentageforProduct(
		clientData,
		productName,
		userSalary
	);

	let noOfDaysWorked = (
		await momentInstanceOnDate(dateTillWeNeedToCalculateEligibility)
	).diff(await momentInstanceOnDate(payrollStartDate), 'days'); // TODO: min:0 cant be negative // TODO: we can remove this from here move to dalary calculation logic

	if (productName === productNames.ADVANCE_LOAN_SUBSCRIPTION) {
		updateData.employmentType = 'permanent';
	}

	if (clientDisplayName.toLowerCase() == clientList.QUESSCORP) {
		console.log('Calculating Eligibility for Quess');
		attendance = await getAttendanceForQuess(
			employeeId,
			payrollStartDate,
			dateTillWeNeedToCalculateEligibility,
			userIdentifier
		);
	}

	if (clientDisplayName.toLowerCase() == clientList.PEOPLESTRONG) {
		console.log('Calculating Eligibility for PS');
		attendance = await getAttendanceForPS(
			employeeId,
			payrollStartDate,
			dateTillWeNeedToCalculateEligibility,
			userIdentifier
		);
	}

	let attendanceData = await createAttendanceData(
		payrollStartDate,
		dateTillWeNeedToCalculateEligibility,
		defaultAttendance,
		attendance
	); // Update Data in the DB after making sure all attandance is present in attendance variable

	await updateAttendanceToDb(attendanceData, userIdentifier, clientName);
	console.log(
		noOfDaysWorked,
		dateTillWeNeedToCalculateEligibility,
		payrollStartDate
	);

	updateData = await calculateEligibility(
		attendanceData,
		noOfDaysWorked,
		perDaySalary,
		eligibilty_percentage,
		updateData,
		maximum_loan_amount,
		userSalary,
		clientDisplayName,
		userIdentifier
	);
	return updateData;
};

module.exports = updateEligibility;
