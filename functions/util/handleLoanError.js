const rdbInstance = require('../helper/rdb');
const configureErrorEmailOptions = require('../helper/emailHelper')
	.configureErrorEmailOptions;
const sendMail = require('../util/sendEmail');
const updateLoanInDB = require('./updateLoanInDB');
const asyncForEach = require('./asyncForEach').asyncForEach;
const {
	getUIConstantInformation,
	deleteRetryLoanID,
	getUserInformation,
	getClientInformation,
} = require('./fetchInfoFromDB');
const handleLoanError = async (
	status,
	loan_status,
	loanData,
	partner_loan_id,
	number,
	err,
	pushStatus,
	name
) => {
	loanData.status = status;
	loan_status.push(pushStatus);
	loanData.syncStatus = loan_status;

	const userData = await getUserInformation(number);
	const { employerName, disbursementDate } = loanData;
	const { workProfile, userProfile } = userData;
	const { firstName, lastName } = userProfile;
	const fullname = firstName + ' ' + lastName;
	const { employeeId, doj, product_name } = workProfile[employerName].userData;

	const employerData = await getClientInformation(employerName);
	const { payDate } = employerData[product_name];

	let configureErrorData = {
		employeeId: employeeId,
		employeeName: fullname,
		phoneNumber: number,
		employerName: employerName,
		doj: doj,
		payDate: payDate,
		disbursementDate: disbursementDate,
	};

	let uiConstantData = await getUIConstantInformation();
	const { retryKeyword, retryLoanMaxCount } = uiConstantData;
	let message = err.message ? JSON.stringify(err.message) : 'error occured';
	let emailSendFlag = true;
	let retryCount = loanData.retryCount ? loanData.retryCount + 1 : 1;

	let keyword = retryKeyword.split(',');
	await asyncForEach(keyword, async (value) => {
		if (message.includes(value) && retryCount <= retryLoanMaxCount) {
			await deleteRetryLoanID(partner_loan_id);
			await rdbInstance
				.ref('retryLoans')
				.child(partner_loan_id)
				.set({ startProcess: 'initiated' });
			emailSendFlag = false;
			loanData.retryCount = retryCount;
		}
	});
	await updateLoanInDB(loanData, partner_loan_id, number);
	await addError(
		partner_loan_id,
		loanData,
		err,
		name,
		emailSendFlag,
		configureErrorData
	);
};
const addError = async (
	partner_loan_id,
	original,
	error,
	name,
	emailSendFlag,
	configureErrorData
) => {
	original.error = error;

	// TODO: it should be a function which listen to anything added in error and send mail accordangily
	if (emailSendFlag) {
		const mailOptions = await configureErrorEmailOptions(
			error,
			name,
			partner_loan_id,
			configureErrorData
		);
		await sendMail(mailOptions);
	}

	await rdbInstance.ref('Errors').child(partner_loan_id).set(original);
};

module.exports = handleLoanError;
