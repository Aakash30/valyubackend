const attendanceConstantList = require('../constant/attendance.constant');
const clientList = require('../constant/clientList');
const { DEBIT, CREDIT } = require('../constant/transactionType.constant');
const { PAYMENT } = require('../constant/transactionPurpose.constant');
const { getAllTransactionOnPurpose } = require('../util/fetchInfoFromDB');
const { asyncForEach } = require('./asyncForEach');
const calculateEligibility = async (
	attendanceData,
	days,
	perDaySalary,
	eligibilty_percentage,
	updateData,
	maximum_loan_amount,
	userSalary,
	clientDisplayName,
	userIdentifier
) => {
	let count = 0;
	let calculateSalary = days * perDaySalary;
	console.log(days, 'days');

	for (let date in attendanceData) {
		let { isDefaultAttendanceUsed, actualStatus } = attendanceData[date];
		if (actualStatus === attendanceConstantList.ABSENT) {
			count++;
		}
		isDefaultAttendanceUsed = isDefaultAttendanceUsed
			? isDefaultAttendanceUsed
			: false;
	}
	let accrued_amount = calculateSalary - count * perDaySalary;
	let eligibilityAmount = (accrued_amount * eligibilty_percentage) / 100;
	let creditLimit = (userSalary * eligibilty_percentage) / 100;

	if (clientDisplayName.toLowerCase() == clientList.QUESSCORP) {
		creditLimit = creditLimit + 1000;
	}
	if (clientDisplayName.toLowerCase() == clientList.PEOPLESTRONG) {
		creditLimit = creditLimit + (2 * creditLimit) / 100 + 2000;
	}
	eligibilityAmount =
		eligibilityAmount > maximum_loan_amount
			? maximum_loan_amount
			: eligibilityAmount;

	updateData.accrued_amount = parseFloat(accrued_amount.toFixed(2));
	updateData.eligibility_amount = parseFloat(eligibilityAmount.toFixed(2));

	// 5000 DEBIT
	// 3000 CREDIT
	// 8000 DEBIT
	// 12000 CREDIT
	/* Trsnsections:  
	1. small utility perUSer/perLoan all the loans which are disbursed 80 status will create a key in trnsection with DEBIT - PAYMENT
	2.  Calcuation: pendingPaymentTotal: sum of all transection with Purpose PAYMENT (DEBIT - CREDIT)    ()
	*/
	// getURL: /updateEligibility   post 8743850717,12345121,1231312

	// TODO: what to do with negative value will decide lator setting it to min 0

	//await createTransaction(userIdentifier, clientName);
	let pendingPaymentTotal = await findPendingPaymentTotal(userIdentifier);
	updateData.pendingPaymentTotal = parseFloat(pendingPaymentTotal.toFixed(2));

	updateData.available_amount = parseFloat(
		(eligibilityAmount - pendingPaymentTotal).toFixed(2)
	);
	updateData.creditLimit = parseFloat(creditLimit.toFixed(2));

	return updateData;
};

const findPendingPaymentTotal = async (userIdentifier) => {
	let sum = 0;
	let transactionDataOnType = await getAllTransactionOnPurpose(
		userIdentifier,
		PAYMENT
	);
	if (transactionDataOnType) {
		await asyncForEach(transactionDataOnType, async (transactionData) => {
			let { txn_amount, type } = transactionData;
			if (type == DEBIT) {
				sum = sum + txn_amount;
			} else if (type == CREDIT) {
				sum = sum - txn_amount;
			}
		});
	}
	sum = sum < 0 ? 0 : sum;
	return sum;
};

module.exports = calculateEligibility;
