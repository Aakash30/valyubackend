const rdbInstance = require('../helper/rdb');
const updateLoanInDB = async (original, partnerLoanId, userIdentifier) => {
  await rdbInstance
    .ref('users/' + userIdentifier + '/Loan')
    .child(partnerLoanId)
    .update(original);
};

module.exports = updateLoanInDB;
