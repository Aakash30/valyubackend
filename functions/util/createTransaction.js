const {
	getDisbursedLoanInformation,
	getUserInformation,
} = require('./fetchInfoFromDB');
const { momentInstance } = require('../helper/moment');
const asyncForEach = require('./asyncForEach');
const { DEBIT } = require('../constant/transactionType.constant');
const { PAYMENT } = require('../constant/transactionPurpose.constant');
const updateLoanInDB = require('./updateLoanInDB');
const rdbInstance = require('../helper/rdb');
const createTransaction = async (userIdentifier, clientName) => {
	//getAllLoan with status is disbursed
	let loanData = await getDisbursedLoanInformation(userIdentifier);
	let userData = await getUserInformation(userIdentifier);
	let { workProfile } = userData;
	let { userData: workUserData } = workProfile[clientName];
	let { virtual_account_no, transactionId } = workUserData;
	let transactionDate = momentInstance.format('YYYY-MM-DD');
	let transactionData = {};

	let responseData = {};
	let loanIds = [];
	if (loanData) {
		for (let loanId in loanData) {
			let timestamp = new Date().getTime();
			let { repaymentAmount, isTransactionCreated } = loanData[loanId];
			if (!isTransactionCreated) {
				await updateLoanInDB(
					{ isTransactionCreated: true },
					loanId,
					userIdentifier
				);
				loanIds.push(loanId);
				transactionData[timestamp] = {
					txn_amount: repaymentAmount,
					txn_date: transactionDate,
					txn_reference: 'Reference ' + transactionId,
					userClientName: clientName,
					vpa_id: virtual_account_no,
					txn_id: transactionId,
					type: DEBIT,
					purpose: PAYMENT,
					timestamp: timestamp,
				};
			}
		}
		if (loanIds.length > 0) {
			await rdbInstance
				.ref('users/' + userIdentifier + '/Transaction')
				.set({ ...transactionData });
			responseData.msg = 'Following LoanID Transaction has been created';
			responseData.loanIds = loanIds;
		} else {
			responseData.msg = ` ALL Transaction for ${userIdentifier} has already been created `;
		}
	} else {
		responseData.msg = `No loan Found in disbursement Stage for ${userIdentifier}`;
	}
	return responseData;
};

module.exports = createTransaction;
