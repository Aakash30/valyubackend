const config = require('../config/index.config');
const getKudosConfig = async (productName) => {
  const selectedKudosConfig =
    productName == 'advanceLoan_subscription'
      ? config.KUDOS_SUBSCRIPTION
      : config.KUDOS;
  //const { loan_request_url: url, options } = selectedKudosConfig;

  //return { url, options };

  return selectedKudosConfig;
};
module.exports = getKudosConfig;
