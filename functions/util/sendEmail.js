const rdbFireStoreInstance = require('../helper/fireStore');
const sendMail = async (mailOptions) => {
	let result = undefined;
	let err = undefined;
	try {
		result = await rdbFireStoreInstance.collection('mail').add(mailOptions);
	} catch (error) {
		err = error;
	}
	return { result, err };
};
module.exports = sendMail;
