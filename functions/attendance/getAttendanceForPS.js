//For PeopleStrong
const config = require('../config/index.config');
const handleData = require('../handleData').handleData;
const { configureErrorEmailOptions } = require('../helper/emailHelper');
const sendMail = require('../util/sendEmail');
const {
	momentInstanceOnDate,
	momentInstanceToFormat,
} = require('../helper/moment');
const { asyncForEach } = require('../util/asyncForEach');
const attendanceAbsentString = 'Absent';

const getAttendanceForPS = async (
	employeeId,
	startDate,
	endDate,
	userIdentifier
) => {
	let attendanceData = {};

	let body = await getRequestBody(employeeId, startDate, endDate);

	let { result, error } = await handleData(config.PS_ATTENDANCE_URL, body, {});
	if (error) {
		console.log('Some Error Occured', error);
		const mailOptions = await configureErrorEmailOptions(
			error.response,
			`PeopleStrongAttendanceApi Error for ${employeeId} at ${config.PROJECT_ID}`,
			userIdentifier
		);
		await sendMail(mailOptions);

		throw new Error('PeoplStrong Attendance api Failed');
	}

	let { data } = result;
	let attendance = [];

	if (data != undefined && (data.data == null || data.data.length == 0)) {
		//eligibility_amount = (days * perDaySalary).toFixed(2);
		console.log('No attendance Received for PS');
		const mailOptions = await configureErrorEmailOptions(
			{ message: 'No attendance object present in atendance response ', data },
			`PeopleStrongAttendanceApi Error for ${employeeId} at ${config.PROJECT_ID}`,
			userIdentifier
		);
		await sendMail(mailOptions);
	} else if (data != undefined && data.data.length != 0) {
		console.log('Attendance received');
		attendance = data.data;
	}
	attendanceData = await parseAttendance(attendance);
	return attendanceData;
};

const parseAttendance = async (attendanceData) => {
	//If attendance Found
	let attendanceNewData = {};
	await asyncForEach(attendanceData, async (attendance) => {
		let { ACTUAL_STATUS, STARTDATE } = attendance;
		let date = (await momentInstanceToFormat(STARTDATE, 'DD-MM-YYYY')).format(
			'YYYY-MM-DD'
		);
		let status;
		if (attendanceAbsentString.includes(ACTUAL_STATUS)) {
			status = 'Absent';
		} else {
			status = 'Present';
		}
		attendanceNewData[date] = {
			status: ACTUAL_STATUS,
			isDefaultAttendanceUsed: false,
			actualStatus: status,
		};
	});
	return attendanceNewData;
};

const getRequestBody = async (employeeId, startDate, endDate) => {
	let body;
	const iso = 'T00:00:00Z';
	let days = (await momentInstanceOnDate(startDate)).diff(
		await momentInstanceOnDate(endDate),
		'days'
	); //Date is same

	if (days == 0) {
		let startIsoDate = startDate + iso;
		body = {
			requestorName: 'AltAttendance',
			sysApiCode: 'EmployeeAttendance_solr',
			filterAttrs: [
				{
					objectCode: 'ALTW_ATTENDANCE',
					objectAttrCode: 'ORGANIZATIONID',
					value: '19',
					operator: '=',
				},
				{
					objectCode: 'ALTW_ATTENDANCE',
					objectAttrCode: 'EMPLOYEECODE',
					value: employeeId,
					operator: '=',
				},
				{
					objectCode: 'ALTW_ATTENDANCE',
					objectAttrCode: 'STARTDATE',
					value: startIsoDate,
					operator: '=',
				},
			],
		};
	} else {
		let startIsoDate = startDate + iso;
		let endIsoDate = endDate + iso;
		body = {
			requestorName: 'AltAttendance',
			sysApiCode: 'EmployeeAttendance_solr',
			filterAttrs: [
				{
					objectCode: 'ALTW_ATTENDANCE',
					objectAttrCode: 'ORGANIZATIONID',
					value: '19',
					operator: '=',
				},
				{
					objectCode: 'ALTW_ATTENDANCE',
					objectAttrCode: 'EMPLOYEECODE',
					value: employeeId,
					operator: '=',
				},
				{
					objectCode: 'ALTW_ATTENDANCE',
					objectAttrCode: 'STARTDATE',
					value: startIsoDate,
					operator: '>=',
				},
				{
					objectCode: 'ALTW_ATTENDANCE',
					objectAttrCode: 'ENDDATE',
					value: endIsoDate,
					operator: '<=',
				},
			],
		};
	}

	return body;
};

module.exports = getAttendanceForPS;
