const config = require('../config/index.config');
const handleData = require('../handleData').handleData;
const asyncForEach = require('../util/asyncForEach').asyncForEach;
const { configureErrorEmailOptions } = require('../helper/emailHelper');
const sendMail = require('../util/sendEmail');

const attendanceAbsentString = 'Absent';

const getAttendanceForQuess = async (
	employeeId,
	startDate,
	endDate,
	userIdentifier
) => {
	let attendanceData = {};
	let body = await getRequestBody(employeeId, startDate, endDate);

	let { result, error } = await handleData(
		config.QUESS_ATENDANCE.url,
		body,
		config.QUESS_ATENDANCE.options
	);

	if (error) {
		console.log('Error occured in salaryAttendanceApi for Quess', error);
		const mailOptions = await configureErrorEmailOptions(
			error.response,
			`QuessAttendanceApi Error for ${employeeId} at ${config.PROJECT_ID}`,
			userIdentifier
		);
		await sendMail(mailOptions);
		throw new Error('Quess Attendance api Failed');
	}

	let { data } = result.data;
	let { attendance } = data;

	if (!attendance) {
		console.log('No attendance object present', data, startDate, endDate);
		const mailOptions = await configureErrorEmailOptions(
			{ message: 'No attendance object present in atandance response ', data },
			`QuessAttendanceApi Error for ${employeeId} at ${config.PROJECT_ID}`,
			userIdentifier
		);
		await sendMail(mailOptions);
		attendance = [];
	}

	attendanceData = await parseAttendance(attendance);
	return attendanceData;
};

const parseAttendance = async (attendanceData) => {
	//If attendance Found
	let attendanceNewData = {};
	await asyncForEach(attendanceData, async (attendance) => {
		let { date, status } = attendance;
		let setStatus;
		if (attendanceAbsentString.includes(status)) {
			setStatus = 'Absent';
		} // TODO: Else conditon
		attendanceNewData[date] = {
			status: status,
			isDefaultAttendanceUsed: false,
			actualStatus: setStatus,
		};
	});
	return attendanceNewData;
};

// TODO: match string as 'Absent' for absent

const getRequestBody = async (employeeId, startDate, endDate) => {
	let identifier = config.QUESS_ATENDANCE.identifier;
	let body = {
		identifier: identifier,
		employee_id: employeeId,
		start_date: startDate,
		end_date: endDate,
	};
	return body;
};

module.exports = getAttendanceForQuess;
