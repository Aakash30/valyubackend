const getAttendanceForPS = require('./getAttendanceForPS');
const getAttendanceForQuess = require('./getAttendanceForQuess');
module.exports = {
	getAttendanceForPS,
	getAttendanceForQuess,
};
