const { momentInstanceOnDate } = require('../helper/moment');
const createAttendanceData = async (
	startDate,
	endDate,
	defaultAttendance,
	attendance
) => {
	let attendanceData = {};
	let currDate = (await momentInstanceOnDate(startDate)).startOf('day');
	let lastDate = (await momentInstanceOnDate(endDate)).startOf('day');
	while (currDate.diff(lastDate) <= 0) {
		let date = currDate.format('YYYY-MM-DD');
		attendanceData[date] = {
			status: 'default',
			isDefaultAttendanceUsed: true,
			actualStatus: defaultAttendance,
		};
		currDate.add(1, 'days');
	}

	return { ...attendanceData, ...attendance };
};
module.exports = createAttendanceData;
