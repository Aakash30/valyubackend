const baseConfig = require('./env/base.config');
const devConfig = require('./env/dev.config');
const qaConfig = require('./env/qa.config');
const uatConfig = require('./env/uat.config');
const prodConfig = require('./env/prod.config');
const prodAlphaConfig = require('./env/prodalpha.config');
const testConfig = require('./env/test.config');

let runningEnvConfig;

switch (process.env.SERVER_ENV) {
	case 'dev':
		runningEnvConfig = devConfig;
		break;
	case 'qa':
		runningEnvConfig = qaConfig;
		break;
	case 'uat':
		runningEnvConfig = uatConfig;
		break;
	case 'prod':
		runningEnvConfig = prodConfig;
		break;
	case 'test':
		runningEnvConfig = testConfig;
		break;
	case 'prodalpha':
		runningEnvConfig = prodAlphaConfig;
		break;
	default:
		runningEnvConfig = uatConfig;
		break;
}

const config = {
	...baseConfig,
	...runningEnvConfig,
};

module.exports = config;
