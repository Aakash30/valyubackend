/**
 * Base settings applicable across environments
 *
 * This file can include shared settings across environments
 *
 */
module.exports = {
  APP_ID: 'api-valyu',
  REQUEST_LIMIT: '100kb',
  OPENAPI_SPEC: '/api/v1/spec',
  OPENAPI_ENABLE_RESPONSE_VALIDATION: false,

  PORT: 8080,
  TYPE: 'service_account',
  AUTH_URI: 'https://accounts.google.com/o/oauth2/auth',
  TOKEN_URI: 'https://oauth2.googleapis.com/token',
  AUTH_PROVIDER_x509_CERT_URL: 'https://www.googleapis.com/oauth2/v1/certs',
};
