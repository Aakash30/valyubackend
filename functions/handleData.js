const axios = require('axios');
const qs = require('qs');
const handleData = async (url, data, options, type) => {
  let result = undefined;
  let error = undefined;
  try {
    if (type == 'form') {
      result = await axios.post(url, qs.stringify(data), options);
    } else {
      result = await axios.post(url, data, options);
    }

    return { result, error };
  } catch (error) {
    return { result, error };
  }
};
module.exports = {
  handleData,
};
