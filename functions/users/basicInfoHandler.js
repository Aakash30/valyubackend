const rdbInstnace = require('../helper/rdb');
const moment = require('moment');
const { momentInstance } = require('../helper/moment');
const updateEligibility = require('../util/updateEligibility');
const config = require('../config/index.config');
const productNames = require('../constant/productNames.constant');
const handleData = require('../handleData').handleData;
const sendMail = require('../util/sendEmail');
const configureErrorEmailOptions = require('../helper/emailHelper')
	.configureErrorEmailOptions;
const {
	getClientInformation,
	getUserInformation,
	updateUserInfoInDB,
	saveUserToFirestore,
} = require('../util/fetchInfoFromDB');

exports.userBasicInfoHandler = async (userIdentifier) => {
	console.log('Gathering data from user Provided info for ', userIdentifier);
	try {
		await rdbInstnace.ref('users/' + userIdentifier + '/userProfile').update({
			isAppChangesInProgress: true,
		});
		let fetchflag = false;
		const userData = await getUserInformation(userIdentifier);
		let { userProfile, personal, userProvidedData } = userData;
		let { doj } = userProfile;
		let {
			client,
			salary,
			firstName,
			lastName,
			employeeId,
			clientNameKey,
		} = userProvidedData; // TODO: check if the key exist use lodash get kind thing

		let isDataFetchSuccessfull = personal
			? personal.isDataFetchSuccessfull
				? personal.isDataFetchSuccessfull
				: false
			: false;

		let isSalaryFound = personal
			? personal.isSalaryFound
				? personal.isSalaryFound
				: false
			: false;

		if (!isDataFetchSuccessfull || !isSalaryFound) {
			fetchflag = true;
		}
		if (clientNameKey.includes('quesscorp') && fetchflag) {
			console.log('Fetch userData for QuessCorp on Id: ', employeeId);
			let { partner_name, partner_key, url } = config.QUESS_FETCH_USER_DATA;
			let token = await getEncodedToken(employeeId, partner_name, partner_key);
			let result = await handleQuessUserApi(
				url,
				token,
				partner_name,
				partner_key,
				employeeId,
				userIdentifier
			);
			let insertData = {};
			if (result && result.data) {
				let {
					userProfile,
					account,
					userData,
					employerProfile,
				} = await formatDataInInsertForm(result.data, clientNameKey);
				insertData.userProfile = {
					...userProfile,
				};
				insertData.workProfile = {};
				let { isSalaryFound } = userProfile;
				let { salary: userFoundSalary } = userData;
				salary = isSalaryFound ? userFoundSalary : salary;
				client = client ? client : employerProfile;
				if (client) {
					userData.salary = salary;
					insertData.workProfile[client] = {
						account,
						userData,
					};
				}
				await updateUserInfoInDB(insertData, userIdentifier);
			}

			//userIdentifier = userProfile.mobileNumber;
		}
		// let salary = original.salary;
		let clientData = await getClientInformation(client); // TODO: check if the client Exist
		let { defaultProductName } = clientData;
		defaultProductName = defaultProductName
			? defaultProductName
			: productNames.ADVANCE_LOAN_SUBSCRIPTION;

		const currentDate = momentInstance.startOf('day');
		const previousDate = currentDate
			.clone()
			.subtract(1, 'days')
			.format('YYYY-MM-DD');

		if (salary) {
			await updateEligibility(
				userIdentifier,
				client,
				clientData,
				defaultProductName,
				salary,
				employeeId,
				doj,
				previousDate
			);
		}

		// TODO: move the user_id generation to a different function
		let user_id = firstName.toLowerCase() + new Date().getTime();

		await rdbInstnace.ref('users/' + userIdentifier + '/userProfile').update({
			firstName: firstName,
			lastName: lastName,
			user_id: user_id,
			mobileNumber: userIdentifier,
		});
		//isAppChangesInProgress

		await rdbInstnace.ref('users/' + userIdentifier + '/userProfile').update({
			isAppChangesInProgress: false,
		});

		return true;
	} catch (err) {
		console.error('err:', err);
		await rdbInstnace.ref('users/' + userIdentifier + '/userProfile').update({
			isAppChangesInProgress: false,
		});
	}
};

const getEncodedToken = async (employeeId, partnerName, partnerKey) => {
	let date = momentInstance.format('YYYY-MM-DD');
	let data = {
		partner_name: partnerName,
		partner_key: partnerKey,
		date: date,
		applicant_id: employeeId,
	};
	let dataString = JSON.stringify(data);
	let token = Buffer.from(dataString).toString('base64');
	console.log(token);
	return token;
};

const handleQuessUserApi = async (
	url,
	token,
	partnerName,
	partnerKey,
	employeeId,
	userIdentifier
) => {
	let body = {
		token: token,
		partner_name: partnerName,
		partner_key: partnerKey,
		applicant_id: employeeId,
	};
	console.log(body);

	const { result, error } = await handleData(url, body, {});

	if (error) {
		//	console.log('Some error occured', error);
		await rdbInstnace.ref('users/' + userIdentifier + '/userProfile').update({
			isDataProcess: false,
			mobileNumber: userIdentifier,
		});
		const mailOptions = await configureErrorEmailOptions(
			error.response.data,
			'QuessUserApi',
			userIdentifier
		);
		await sendMail(mailOptions);
		//throw new Error('Error Occured in Quess User Api');
	}
	return result;
};

const formatDataInInsertForm = async (usersData, clientNameKey) => {
	let { response } = usersData;
	let {
		first_name,
		last_name,
		contact_no,
		email_id,
		applicant_id,
		DOB,
		gender,
		Address,
		Pincode,
		City,
		State,
		DOJ,
		account_number,
		ifsc_code,
		father_name,
		work_location,
		gross,
		salary,
		designation,
		marital_status,
		salary_info,
	} = response;

	//Customer_name key
	let userProfile = {
		firstName: first_name,
		address2: '',
		lastName: last_name,
		mobileNumber: contact_no,
		employeeId: applicant_id,
		dob: DOB,
		gender: gender,
		address1: Address,
		pinCode: Pincode,
		city: City,
		state: {
			label: State,
			value: State,
		},
		doj: DOJ,
		matrialStatus: marital_status || '',
		isDataFetchSuccessfull: true,
		isSalaryFound: salary ? true : false,
		isDataProcess: true,
	};
	let account = {
		bankAcNo: account_number || '',
		ifscCode: ifsc_code || '',
	};
	let userData = {
		designation: designation || '',
		doj: DOJ,
		emailId: email_id,
		salary: salary,
		product_name: productNames.ADVANCE_LOAN_SUBSCRIPTION,
	};

	console.log(response);
	let creditedDate, employerProfile;
	if (salary_info && salary_info.length > 0) {
		creditedDate = salary_info[0].credited_date;
		let payDate = moment(creditedDate, 'YYYY-MM-DD').date();

		if (payDate > 26) {
			employerProfile = 'QuessCorp31';
		} else {
			employerProfile = 'QuessCorp' + parseInt(payDate);
		}
	}

	//Add Data to FireStore
	let firestoreData = {
		...userProfile,
		...account,
		...userData,
		fatherName: father_name,
		workLocation: work_location,
		gross,
		currentEmployer: employerProfile,
	};
	await saveUserToFirestore('QuessCorp', contact_no, firestoreData);

	return { userProfile, account, userData, employerProfile };
};
