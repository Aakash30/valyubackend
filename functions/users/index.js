const functions = require('firebase-functions');
const { userBasicInfoHandler } = require('./basicInfoHandler');

const runtimeOpts = {
	timeoutSeconds: 300,
	memory: '1GB',
};

exports.userBasicInfoHandler = functions
	.runWith(runtimeOpts)
	.database.ref('users/{number}/userProvidedData')
	.onWrite(async (change, context) => {
		console.log(
			'Recieved Written request for the on userBasicInfoHandler for ',
			context.params.number
		);
		// console.debug('Change Request trigger with: ', change);
		await userBasicInfoHandler(context.params.number);
	});
