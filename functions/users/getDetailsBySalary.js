const productNames = require('../constant/productNames.constant');
const getDetails = async (employerData, product_name, salary) => {
	let eligibilty_percentage;
	let blackoutStartDate, blackoutEndDate;
	let maxAllowedTransaction = undefined;

	if (product_name == productNames.ADVANCE_LOAN_SUBSCRIPTION) {
		let rules = employerData[product_name].rules;
		let salary_max = false;
		for (let rules_salary in rules) {
			// TODO: make utility
			if (rules_salary >= salary) {
				eligibilty_percentage = rules[rules_salary].eligibilty_percentage;
				blackoutStartDate = rules[rules_salary].blackoutStartDate;
				blackoutEndDate = rules[rules_salary].blackoutEndDate;
				maxAllowedTransaction = rules[rules_salary].maxAllowedTransaction;
				salary_max = true;
				break;
			}
		}
		if (!salary_max) {
			eligibilty_percentage = rules['max'].eligibilty_percentage;
			blackoutStartDate = rules['max'].blackoutStartDate;
			blackoutEndDate = rules['max'].blackoutEndDate;
			maxAllowedTransaction = rules['max'].maxAllowedTransaction;
		}
	} else {
		eligibilty_percentage = employerData[product_name].eligibilty_percentage;
		blackoutStartDate = employerData[product_name].blackoutStartDate;
		blackoutEndDate = employerData[product_name].blackoutEndDate;
	}

	return {
		blackoutStartDate,
		blackoutEndDate,
		eligibilty_percentage,
		maxAllowedTransaction,
	};
};
module.exports = getDetails;
