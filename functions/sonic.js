const HandleData = require('./handleData').HandleData;
const config = require('./config/index.config');
module.exports.pushLoan = async (
  employerName,
  amount,
  employer_data,
  total_loan_applied,
  user_data,
  disbursement_date,
  first_emi_date,
  processing_amount,
  total_gst_on_processing_amount,
  total_gst_on_platform_fee,
  date_diff,
  number,
  partner_loan_id,
  product_name
) => {
  let loan_information = {
    emi_frequency: 'custom',
    loan_amount: amount,
    applied_loan_amount: total_loan_applied,
    disbursement_date,
    first_emi_date,
    tenure: 1,
    loan_tenure_in_days: date_diff,
    fees: {
      processing_fee: {
        fee_amount: processing_amount,
        gst_amount: total_gst_on_processing_amount,
      },
      other_fee: {
        fee_amount: employer_data[product_name].platform_fees,
        gst_amount: total_gst_on_platform_fee,
      },
    },
    interest_type: 'reducing',
    has_pre_emi: false,
    interest_rate: employer_data[product_name].interest_rate,
    'pre-emi_amount': 0.0,
  };
  let partner_tag = employer_data.partner_tag;

  let customer_information = {
    first_name: user_data.userProfile.firstName,
    last_name: user_data.userProfile.lastName,
    current_address: user_data.userProfile.address1,
    permanent_address: user_data.userProfile.address1,
    date_of_birth: user_data.userProfile.dob,
    gender: user_data.userProfile.gender.toLowerCase(),
    employment_status: 'active',
    mtd_payable_days: '30', //verify
    date_of_joining: user_data.workProfile[employerName].userData.doj,
    net_salary: user_data.workProfile[employerName].userData.salary,
    gross_salary: user_data.workProfile[employerName].userData.salary,
    employer_name: employerName,
    emp_id: user_data.workProfile[employerName].userData.employeeId,
    mobile_number: number,
    bank_name: user_data.workProfile[employerName].account.bankName,
    bank_account_number: user_data.workProfile[employerName].account.bankAcNo,
    bank_account_type: 'savings',
    bank_account_name:
      user_data.userProfile.firstName + ' ' + user_data.userProfile.lastName,
    ifsc_code: user_data.workProfile[employerName].account.ifscCode,
    pan: user_data.personal.panNumber,
    scheme_code: employer_data.scheme_code,
  };
  let document_urls = {
    pan: 'PAYM/PL/5f327af7d45277317daa3887/loan_agreement.pdf',
    aadhaar: 'PAYM/PL/5f327af7d45277317daa3887/loan_agreement.pdf',
  };
  console.log(document_urls);
  let body = {
    ...config.PUSH_LOAN_URL.body,
    partner_tag,
    partner_loan_id,
    customer_id: number,
    loan_information,
    customer_information,
    document_urls,
  };

  let { result, error } = await HandleData(
    config.PUSH_LOAN_URL.url,
    body,
    config.PUSH_LOAN_URL.options
  );

  return { result, error };
};
