const Path = require('path');
const config = require('../config/index.config');
const handleLoanError = require('../util/handleLoanError');
const files = require('../convertPdf');
let os = require('os');
const sendEmail = require('../util/sendEmail');
const pdf2base64 = require('pdf-to-base64');
const KUDOS = require('../kudos/index.js');
const loanStatuses = require('../constant/loanStatuses.constant');
const rdbInstance = require('../helper/rdb');
const rdbFireStoreInstance = require('../helper/fireStore');
const storageInstance = require('../helper/storage');
const moment = require('moment-timezone');
const { momentInstance } = require('../helper/moment');
const checkmaxTransactionLimit = require('./checkmaxTransactionLimit');
const isBlackOutPeriod = require('./checkBlackOutPeriod');
const checkLoanAmountisValid = require('./checkLoanAmountisValid');
const forceKycNameCheck = require('./forcedKycNameCheck');
const { configureMailOptions } = require('../helper/emailHelper');
const updateLoanInDB = require('../util/updateLoanInDB');
const disbursementAutomation = require('./disbursementAutomation.js');
const callKudos = async (
	maxAllowedTransaction,
	original,
	isBlackoutPeriod,
	sum,
	user_data,
	employer_data,
	product_name,
	partnerLoanId,
	number,
	startDate,
	timestamp,
	htmlfilename,
	loan_status
) => {
	let err, aadhar_path, pan_path, loan_path, selfie_path;
	let data, response, result, error;
	let {
		employerName,
		disbursementDate,
		firstEmiDate,
		processingFees,
		totalLoanApplied,
	} = original;
	let { maximum_loan_amount, payDate } = employer_data[product_name];
	let { clientName, clientId } = employer_data;

	let time = new Date().getTime();
	let {
		aadharImagePath,
		panImagePath,
		panNumber,
		aadharNumber,
		aadharFrontImagePath,
		aadharBackImagePath,
		isPanConvert,
		selfieImagePath,
	} = user_data.personal;
	panNumber = panNumber.replace(/\s+/g, '');

	let {
		emailId,
		isKycCompleted,
		isSelfieUploadedCompleted,
		isLoanBorrowerInfoCompleted,
		isPanUploadedCompleted,
		isAadharUploadedCompleted,
		isLoanDocumentUploadedCompleted,
		isUserInfoUploaded,
		isLoanManageCompleted,
		kudos_loan_id,
		kudos_borrower_id,
		partner_loan_id,
		virtual_account_no,
		doj,
		employeeId,
		salary,
		totalSuccessfullApplication,
		officeAddress,
		eligibility_amount,
		creditLimit,
		available_amount,
	} = user_data.workProfile[employerName].userData;

	let {
		firstName,
		lastName,
		dob,
		gender,
		address1,
		address2,
		user_id,
		matrialStatus,
		pinCode,
		city,
		state,
	} = user_data.userProfile;
	let fullname = firstName + ' ' + lastName;

	let now = momentInstance.format('MMM DD YYYY h:mm A');

	//Check maxTransactionLimit based on maximum allowed Transaction

	err = await checkmaxTransactionLimit(
		maxAllowedTransaction,
		totalSuccessfullApplication,
		loan_status,
		original,
		partnerLoanId,
		number,
		loanStatuses['6']
	);

	loan_status.push(loanStatuses['5']);
	original.status = 10;
	original.syncStatus = loan_status;
	//Check cut of criteria ,blackout period calculation

	err = await isBlackOutPeriod(
		isBlackoutPeriod,
		loan_status,
		original,
		partnerLoanId,
		number,
		loanStatuses['8']
	);
	loan_status.push(loanStatuses['7']);
	original.status = 10;
	original.syncStatus = loan_status;

	//Check Loan Eligibility

	err = await checkLoanAmountisValid(
		eligibility_amount,
		sum,
		maximum_loan_amount,
		loan_status,
		original,
		partnerLoanId,
		number,
		loanStatuses['9']
	);

	loan_status.push(loanStatuses['10']);
	original.status = 10;
	original.syncStatus = loan_status;
	let transaction_id = time + 'T' + number;
	let virtual_acc_no = undefined;

	let { bankName, bankAcNo, ifscCode } = user_data.workProfile[
		employerName
	].account;
	bankAcNo = bankAcNo.replace(/\s+/g, '');
	ifscCode = ifscCode.replace(/\s+/g, '');

	let age = moment(startDate).diff(moment(dob), 'years');

	aadharNumber = aadharNumber.toString();

	aadharNumber = 'XXXXXXXXXX' + aadharNumber.substring(10);

	let loanStartDate = moment(startDate, 'YYYY-MM-DD').format('MMM DD,YYYY');
	let birthDate = moment(dob, 'YYYY-MM-DD').format('MMM DD,YYYY');

	let applicationformData = {
		NAME_OF_BORROWER: fullname,
		PAN_OF_BORROWER: panNumber,
		DOB_OF_BORROWER: birthDate,
		AADHAR_OF_BORROWER: aadharNumber,
		CONTACT_OF_BORROWER: number,
		MARITAL_STATUS_OF_BORROWER: matrialStatus,
		EMAIL_ID_OF_BORROWER: emailId,
		GENDER_OF_BORROWER: gender,
		LOAN_ADDRESS: officeAddress,
		LANDMARK_ADDRESS: address1 + '' + address2,
		LANDMARK_PINCODE: pinCode,
		LANDMARK_CITY: city,
		LANDMARK_STATE: state.value,
		LOAN_AMOUNT: totalLoanApplied.toFixed(2),
		ACTUAL_AMOUNT: original.amount.toFixed(2),
		PURPOSE_OF_LOAN: original.reason,
		LOAN_TENURE: original.loan_tenure_in_days,
		LOAN_BANK_NAME: bankName,
		LOAN_BANK_ACCOUNT: bankAcNo,
		LOAN_BANK_IFSC: ifscCode,
		LOAN_PRODUCT_NAME: 'Valyu – Credit Line Subscription',
		LOAN_DUE_DATE: firstEmiDate,
		LOAN_RATE_OF_INTEREST: original.interestRate + '%',
		INTEREST_METHOD: 'Daily Interest',
		//LOANOTP
		TIMESTAMP: now,
		LOAN_ID: partnerLoanId,
		LOAN_DATE: loanStartDate,
		//EDI_date
		LOAN_TOTAL_AMOUNT: original.repaymentAmount.toFixed(2),
		LOAN_MOBILE_NUMBER: number,
		LOAN_AGREEMENT_NO: partnerLoanId,
		PROCESSING_FEES: original.processingFees.toFixed(2),
		PARTNER_FEE: (original.processingFees + original.platformFees).toFixed(2),
		//Bussiness of borrower
		STAMPING_CHARGES: 0,
		PLATFORM_FEES: original.platformFees,
		LATE_PAYMENT_CHARGES: 0,
		PREPAYMENT_CHARGES: 0,
		LEGAL_CHARGES: 0,
		CLIENTNAME: clientName,
		NUMBER_OF_EDI: 1,
		PAYMENT_MODE: 'Online',
		LOAN_RATE_OF_INTEREST_PER_ANNUM: 0,
		LOAN_USER_LOCATION: '----',
		BUSINESS_PHONE: 'BUSINESS_PHONE',
		ELIGIBILITY_AMOUNT: eligibility_amount,
		TENURE: 45,
	};
	const isPdfgenerated = await generateLoanDocument(
		htmlfilename,
		applicationformData
	);
	if (isPdfgenerated) {
		await storageInstance.upload(
			Path.join(os.tmpdir(), `${htmlfilename}.pdf`),
			{
				destination: `loanDocuments/${number}/${partnerLoanId}.pdf`,
			}
		);

		original.loan_document_url = `/loanDocuments/${number}/${partnerLoanId}.pdf`;
		loan_path = await getSignedUrlforDocument(
			original.loan_document_url.slice(1)
		);
		loan_path = loan_path[0];
	} else {
		console.log('someErrorOccured');
	}

	if (kudos_loan_id && kudos_borrower_id && partner_loan_id) {
		response = {
			kudos_borrower_id: kudos_borrower_id,
			kudos_loan_id: kudos_loan_id,
			partner_loan_id: partner_loan_id,
			partner_borrower_id: user_id,
		};
	} else {
		({ result, error } = await KUDOS.loanRequest(
			partnerLoanId,
			user_data,
			number,
			original.reason,
			employerName,
			employer_data,
			product_name,
			startDate,
			loan_status,
			original,
			loanStatuses['19'],
			creditLimit
		));
		loan_status.push(loanStatuses['20']);
		original.status = 20;
		original.syncStatus = loan_status;
		data = result.data.data.preparedbiTmpl[0];
		response = {
			partner_loan_id: data.partner_loan_id,
			partner_borrower_id: data.partner_borrower_id,
			kudos_loan_id: data.kudos_loan_id,
			kudos_borrower_id: data.kudos_borrower_id,
		};
		await rdbInstance
			.ref('users/' + number + '/workProfile/' + employerName)
			.child('userData')
			.update({
				kudos_loan_id: data.kudos_loan_id,
				kudos_borrower_id: data.kudos_borrower_id,
				partner_loan_id: data.partner_loan_id,
			});

		kudos_loan_id = data.kudos_loan_id;
	}
	let runnable = false;

	if (!isKycCompleted) {
		if (employer_data.forceKycNameCheck) {
			runnable = await forceKycNameCheck(
				panNumber,
				firstName,
				lastName,
				loan_status,
				original,
				partnerLoanId,
				number,
				loanStatuses['91']
			);
			if (!runnable) {
				return;
			}
		} else {
			runnable = true;
		}
		if (runnable) {
			({ result, error } = await KUDOS.panKyc(
				panNumber,
				config.VALYU_CODE,
				kudos_loan_id,
				number,
				loan_status,
				original,
				partnerLoanId,
				loanStatuses['91']
			));
			if (result && result.data.KycStatus.toLowerCase() == 'success') {
				console.log('KYC API RETURNS SUCCESS');
				loan_status.push(loanStatuses['90']);
				original.status = 20;
				original.syncStatus = loan_status;
				await rdbInstance
					.ref('users/' + number + '/workProfile/' + employerName)
					.child('userData')
					.update({
						isKycCompleted: true,
					});
			} else {
				err = {
					status: 'Kyc error issue',
					message: 'KYC Failed',
				};
				await handleLoanError(
					70,
					loan_status,
					original,
					partnerLoanId,
					number,
					err,
					loanStatuses['91'],
					'KYC'
				);
				throw new Error('KYC FAILED');
			}
		}
	}
	if (!isLoanBorrowerInfoCompleted) {
		({ result, error } = await KUDOS.loanBorrowerInfo(
			response,
			firstEmiDate,
			employerName,
			employer_data,
			user_data,
			doj,
			product_name,
			startDate,
			age,
			original,
			partnerLoanId,
			number,
			loan_status,
			loanStatuses['29'],
			creditLimit
		));

		loan_status.push(loanStatuses['30']);
		original.syncStatus = loan_status;
		await rdbInstance
			.ref('users/' + number + '/workProfile/' + employerName)
			.child('userData')
			.update({
				isLoanBorrowerInfoCompleted: true,
			});
	}

	if (!virtual_account_no) {
		({ result, error } = await KUDOS.getVirtualAccount(
			response,
			loan_status,
			original,
			partnerLoanId,
			number,
			loanStatuses['27']
		));
		loan_status.push(loanStatuses['28']);
		original.syncStatus = loan_status;
		virtual_account_no = result.data.data.generatedVaNumbers[0].va_num;

		await rdbInstance
			.ref('users/' + number + '/workProfile/' + employerName)
			.child('userData')
			.update({
				virtual_account_no: virtual_account_no,
			});
	}

	if (!isAadharUploadedCompleted) {
		if (aadharImagePath) {
			aadhar_path = await getSignedUrlforDocument(aadharImagePath.slice(1));
			aadhar_path = aadhar_path[0];
		} else {
			await files.getImage(aadharFrontImagePath, 'aadharfront');
			await files.getImage(aadharBackImagePath, 'aadharback');
			await files.convertintopdf(
				'aadharfront.jpg',
				'aadharback.jpg',
				'aadhar.pdf'
			);
			let destinationpath = `/userdocuments/${number}/aadhar.pdf`;
			await storageInstance.upload(Path.join(os.tmpdir(), 'aadhar.pdf'), {
				destination: destinationpath.slice(1),
			});
			await rdbInstance
				.ref('users')
				.child(number)
				.child('personal')
				.update({ aadharImagePath: destinationpath });
			aadhar_path = await getSignedUrlforDocument(destinationpath.slice(1));
		}
		aadhar_path = aadhar_path[0];
		if (!aadhar_path) {
			console.log('Aadhar image not exist');
		}
		({ result, error } = await UploadFile(
			aadhar_path,
			response,
			'aadhar_card',
			loan_status,
			original,
			partnerLoanId,
			number,
			'Upload Aadhar document failed',
			'Upload Aadhar document failed',
			'UploadAadhar',
			product_name
		));
		loan_status.push(loanStatuses['26']);
		original.syncStatus = loan_status;
		await rdbInstance
			.ref('users/' + number + '/workProfile/' + employerName)
			.child('userData')
			.update({
				isAadharUploadedCompleted: true,
			});
	} else {
		aadhar_path = await getSignedUrlforDocument(aadharImagePath.slice(1));
		aadhar_path = aadhar_path[0];
	}
	if (!isPanUploadedCompleted) {
		if (isPanConvert) {
			pan_path = await getSignedUrlforDocument(panImagePath.slice(1));
		} else {
			await files.getImage(panImagePath, 'panCard');
			await files.convertintopdf('panCard.jpg', undefined, 'panCard.pdf');
			let destinationpath = `/userdocuments/${number}/panCard.pdf`;
			await storageInstance.upload(Path.join(os.tmpdir(), 'panCard.pdf'), {
				destination: destinationpath.slice(1),
			});
			await rdbInstance.ref('users').child(number).child('personal').update({
				panImagePath: destinationpath,
				isPanConvert: true,
			});
			pan_path = await getSignedUrlforDocument(destinationpath.slice(1));
		}
		pan_path = pan_path[0];
		if (!pan_path) {
			console.log('pan_path not found');
		}
		({ result, error } = await UploadFile(
			pan_path,
			response,
			'pan_card',
			loan_status,
			original,
			partnerLoanId,
			number,
			'Upload Pan document failed',
			'Upload Pan document failed',
			'UploadPan',
			product_name
		));

		loan_status.push(loanStatuses['26.1']);
		original.syncStatus = loan_status;
		await rdbInstance
			.ref('users/' + number + '/workProfile/' + employerName)
			.child('userData')
			.update({
				isPanUploadedCompleted: true,
			});
	} else {
		pan_path = await getSignedUrlforDocument(panImagePath.slice(1));
		pan_path = pan_path[0];
	}
	if (!isLoanDocumentUploadedCompleted) {
		if (loan_path) {
			const contents_in_base64 = await pdf2base64(loan_path);
			({ result, error } = await KUDOS.uploadDocument(
				response,
				contents_in_base64,
				'agreement',
				product_name,
				loan_status,
				original,
				partnerLoanId,
				number,
				loanStatuses['25'],
				loanStatuses['25'],
				'LoanAgrement'
			));
			loan_status.push(loanStatuses['26.2']);
			original.syncStatus = loan_status;
			await rdbInstance
				.ref('users/' + number + '/workProfile/' + employerName)
				.child('userData')
				.update({
					isLoanDocumentUploadedCompleted: true,
				});
		}
	}
	let isSelfieFound = false;
	if (!isSelfieUploadedCompleted) {
		if (!selfieImagePath) {
			console.log('Selfie Image Not Found');
		} else {
			isSelfieFound = true;
			await files.getImage(selfieImagePath, 'selfie');
			await files.convertintopdf('selfie.jpg', undefined, 'selfie.pdf');
			let destinationpath = `/userdocuments/${number}/selfie.pdf`;
			await storageInstance.upload(Path.join(os.tmpdir(), 'selfie.pdf'), {
				destination: destinationpath.slice(1),
			});
			await rdbInstance.ref('users').child(number).child('personal').update({
				selfieImagePath: destinationpath,
			});

			selfie_path = await getSignedUrlforDocument(destinationpath.slice(1));
			selfie_path = selfie_path[0];
		}
		if (isSelfieFound) {
			console.log('selfie_path found');
			({ result, error } = await UploadFile(
				selfie_path,
				response,
				'selfie',
				loan_status,
				original,
				partnerLoanId,
				number,
				'Selfie document failed',
				'Selfie document failed',
				'Selfie',
				product_name
			));

			loan_status.push(loanStatuses['26.3']);
			original.syncStatus = loan_status;
			await rdbInstance
				.ref('users/' + number + '/workProfile/' + employerName)
				.child('userData')
				.update({
					isSelfieUploadedCompleted: true,
				});
		}
	}

	//LoanBody
	let user_path; //From firestore
	if (!isUserInfoUploaded) {
		let userData = await rdbFireStoreInstance
			.collection(clientName)
			.doc(number)
			.get();
		userData = userData.data();
		if (userData) {
			await files.convertintopdf(
				undefined,
				undefined,
				'userInfo.pdf',
				'image',
				userData
			);
			let destinationpath = `/userdocuments/${number}/userInfo.pdf`;
			await storageInstance.upload(Path.join(os.tmpdir(), 'userInfo.pdf'), {
				destination: destinationpath.slice(1),
			});
			await rdbInstance.ref('users').child(number).child('personal').update({
				userInfoImagePath: destinationpath,
			});
			user_path = await getSignedUrlforDocument(destinationpath.slice(1));
			user_path = user_path[0];
			console.log(user_path);
			if (user_path) {
				({ result, error } = await UploadFile(
					user_path,
					response,
					'other',
					loan_status,
					original,
					partnerLoanId,
					number,
					'UserInformation document failed',
					'UserInformation document failed',
					'UserInformation',
					product_name
				));

				loan_status.push(loanStatuses['26.4']);
				original.syncStatus = loan_status;
				await rdbInstance
					.ref('users/' + number + '/workProfile/' + employerName)
					.child('userData')
					.update({
						isUserInfoUploaded: true,
					});
			}
		}
	}

	let sendTo = [config.OPS_EMAIL, 'lalit.chaturvedi@valyu.ai'];
	const mailOptions = await configureMailOptions(
		sendTo,
		partnerLoanId,
		fullname,
		employeeId,
		number,
		emailId,
		doj,
		dob,
		gender,
		address1,
		now,
		salary,
		eligibility_amount,
		original.amount,
		totalLoanApplied,
		transaction_id,
		clientId,
		clientName,
		loan_path,
		aadhar_path,
		pan_path,
		available_amount,
		payDate,
		disbursementDate
	);
	({ result, error } = await sendEmail(mailOptions));

	if (result) {
		console.log('SEND EMAIL TO OPERATION TEAM IS SUCCESS');
		//console.log(result);
	}
	if (employer_data.disbursementAutomation) {
		await disbursementAutomation(
			isLoanManageCompleted,
			response,
			user_data,
			employer_data,
			virtual_acc_no,
			transaction_id,
			original,
			product_name,
			processingFees,
			startDate,
			loan_status,
			partnerLoanId,
			number,
			fullname,
			timestamp,
			sum,
			eligibility_amount,
			totalSuccessfullApplication
		);
	} else {
		await rdbInstance
			.ref('users/' + number + '/workProfile/' + employerName)
			.child('userData')
			.update({
				totalSuccessfullApplication: totalSuccessfullApplication + 1,
			});

		let to = [emailId, config.OPS_EMAIL, 'lalit.chaturvedi@valyu.ai'];
		const mailOptions = await configureMailOptions(
			to,
			partnerLoanId,
			fullname,
			employeeId,
			number,
			emailId,
			doj,
			dob,
			gender,
			address1,
			now,
			salary,
			eligibility_amount,
			original.amount,
			totalLoanApplied,
			transaction_id,
			clientId,
			clientName,
			loan_path,
			aadhar_path,
			pan_path,
			available_amount,
			payDate,
			disbursementDate
		);
		const { result, err_info } = await sendEmail(mailOptions);

		if (result) {
			console.log('SEND EMAIL SUCCESS');
			//console.log(result);
			loan_status.push(loanStatuses['100']);
			original.syncStatus = loan_status;
			original.status = 80;
			updateLoanInDB(original, partnerLoanId, number);
			rdbInstance.ref('LoanHistory').child(partnerLoanId).update(original);
		}
		if (err_info) {
			err = {
				status: 'Email Sending failed',
			};
			await handleLoanError(
				70,
				loan_status,
				original,
				partnerLoanId,
				number,
				err,
				loanStatuses['101'],
				'EmailSending'
			);
			return;
		}
	}
};

const UploadFile = async (
	file,
	response,
	file_type,
	loan_status,
	original,
	partnerLoanId,
	number,
	status,
	statusMessage,
	type,
	product_name
) => {
	const contents_in_base64 = await pdf2base64(file);
	let { result, error } = await KUDOS.uploadDocument(
		response,
		contents_in_base64,
		file_type,
		product_name,
		loan_status,
		original,
		partnerLoanId,
		number,
		status,
		statusMessage,
		type
	);
	return { result, error };
};

const getSignedUrlforDocument = async (documentUrl) => {
	let path = await storageInstance.file(documentUrl).getSignedUrl({
		action: 'read',
		expires: Date.now() + 1000 * 60 * 60 * 24,
	});

	return path;
};

const generateLoanDocument = async (htmlfilename, data) => {
	const isPdfgenerated = await files.htmlToPdf(htmlfilename, data);
	return isPdfgenerated;
};
module.exports = callKudos;
