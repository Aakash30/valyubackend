const handleLoanError = require('../util/handleLoanError');
const isBlackOutPeriod = async (
	check_cut_of_criteria,
	loan_status,
	original,
	partner_loan_id,
	number,
	status
) => {
	let err = undefined;
	if (check_cut_of_criteria) {
		err = {
			status: 'Blackout period is in process',
		};
		await handleLoanError(
			70,
			loan_status,
			original,
			partner_loan_id,
			number,
			err,
			status,
			'Blackout Period'
		);
		throw new Error('Blackout condition Failed');
	}
	return err;
};

module.exports = isBlackOutPeriod;
