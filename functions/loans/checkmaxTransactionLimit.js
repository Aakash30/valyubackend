const handleLoanError = require('../util/handleLoanError');
const checkmaxTransactionLimit = async (
	maxAllowedTransaction,
	transaction_no,
	loan_status,
	original,
	partner_loan_id,
	number,
	status
) => {
	let err = undefined;

	if (maxAllowedTransaction != undefined) {
		if (transaction_no + 1 > maxAllowedTransaction) {
			err = {
				status: 'Limit of maxAllowedTransaction exceeded',
			};
			await handleLoanError(
				70,
				loan_status,
				original,
				partner_loan_id,
				number,
				err,
				status,
				'MaxTransaction'
			);

			throw new Error('Max allowed Transaction condition Failed');
		}
	}
	return err;
};

module.exports = checkmaxTransactionLimit;
