const productNames = require('../constant/productNames.constant');
const calculateFeeAmount = async (
	employerData,
	product_name,
	amount,
	salary
) => {
	let processing_amount = 0;
	let cgst_on_processing_amount = 0;
	let sgst_on_processing_amount = 0;
	let cgst_on_platform_fee = 0;
	let sgst_on_platform_fee = 0;

	let fee = 0;

	if (product_name == productNames.ADVANCE_LOAN_SUBSCRIPTION) {
		//TODO: move to constant
		let salary_max = false;
		let rules = employerData[product_name].rules;
		for (let rules_salary in rules) {
			if (rules_salary >= salary) {
				fee = rules[rules_salary].fee;
				salary_max = true;
				break;
			}
		}
		if (!salary_max) {
			fee = rules['max'].fee; // TODO: move to constant
		}
		// processing_amount = parseFloat(
		// 	(employerData[product_name].fee * amount) / (100).toFixed(2)
		// );
		processing_amount = fee;
		cgst_on_processing_amount = Math.round(
			(fee * employerData[product_name].cgst) / 100
		);
		sgst_on_processing_amount = Math.round(
			(fee * employerData[product_name].sgst) / 100
		);
	} else {
		processing_amount = parseFloat(
			(employerData[product_name].processing_fee * amount) / (100).toFixed(2)
		);
		cgst_on_processing_amount = Math.round(
			(processing_amount * employerData[product_name].cgst) / 100
		);
		sgst_on_processing_amount = Math.round(
			(processing_amount * employerData[product_name].sgst) / 100
		);
		cgst_on_platform_fee = Math.round(
			(employerData[product_name].platform_fees *
				employerData[product_name].cgst_on_platform_fee) /
				100
		);
		sgst_on_platform_fee = Math.round(
			(employerData[product_name].platform_fees *
				employerData[product_name].sgst_on_platform_fee) /
				100
		);
	}
	amount = {
		processing_amount,
		cgst_on_processing_amount,
		sgst_on_processing_amount,
		cgst_on_platform_fee,
		sgst_on_platform_fee,
		fee,
	};
	return amount;
};

module.exports = calculateFeeAmount;
