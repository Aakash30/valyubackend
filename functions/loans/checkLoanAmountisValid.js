const handleLoanError = require('../util/handleLoanError');

const checkLoanAmountisValid = async (
	eligible_amount,
	sum,
	employer_maximumLoanAmount,
	loan_status,
	original,
	partner_loan_id,
	number,
	status
) => {
	let err = undefined;
	if (eligible_amount < sum || sum >= employer_maximumLoanAmount) {
		err = {
			status: 'Maximum Loan amount exceeded',
		};
		await handleLoanError(
			70,
			loan_status,
			original,
			partner_loan_id,
			number,
			err,
			status,
			'AmountEligibilty'
		);
		throw new Error('Loan Amount is not valid');
	}
	return err;
};

module.exports = checkLoanAmountisValid;
