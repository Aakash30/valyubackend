const rdbInstance = require('../helper/rdb');
const updateLoanInDB = require('../util/updateLoanInDB');
const { loanManage, loanUsuage } = require('../kudos/index.js');
const loanStatuses = require('../constant/loanStatuses.constant');

const disbursementAutomation = async (
	loanData,
	userIdentifier,
	userData,
	employerData
) => {
	try {
		const employerName = loanData.employerName;
		const workUserData = userData.workProfile[employerName].userData;
		const {
			isLoanManageCompleted,
			eligibility_amount,
			applied_amount,
		} = workUserData;
		const { amount } = loanData;

		if (!isLoanManageCompleted) {
			const { isSuccess, data } = await loanManage(
				loanData,
				userIdentifier,
				userData,
				employerData
			);
			if (isSuccess) {
				const { syncStatus, transactionId } = data;
				loanData.syncStatus = syncStatus;
				userData.workProfile[
					employerName
				].userData.transactionId = transactionId;
			}
		}

		if (!loanData.isLoanUsageCompleted) {
			const { isSuccess, data } = await loanUsuage(
				loanData,
				userIdentifier,
				userData
			);

			if (isSuccess) {
				const { syncStatus } = data;
				loanData.syncStatus = syncStatus;
			}
		}

		/*
    	({ result, error } = await KUDOS.paytmDisbursal(
    		number,
    		user_data,
    		original.amount,
    		employerName,
    		response,
    		loan_status,
    		original,
    		partnerLoanId,
    		loanStatuses['49']
    	));
    */

		loanData.status = 50;
		await updateLoanInDB(loanData, loanData.id, userIdentifier);
		rdbInstance.ref('LoanHistory').child(loanData.id).update(loanData);

		let update_data = {
			applied_amount: Number(applied_amount) + Number(amount),
			available_amount:
				Number(eligibility_amount) - (Number(applied_amount) + Number(amount)),
		};

		await rdbInstance
			.ref('users/' + userIdentifier + '/workProfile/' + employerName)
			.child('userData')
			.update(update_data);

		console.log('Disbursement Automation done for ', loanData.id);
		return {
			isSuccess: true,
		};
	} catch (err) {
		console.error('Disbursement Automation failed for ', loanData.id);
		console.error(err);
		throw 'DISBURSEMENT_FAILED';
	}
};

module.exports = disbursementAutomation;
