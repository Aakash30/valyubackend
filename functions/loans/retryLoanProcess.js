const createLoan = require('./createLoan');
const { getLoanInformation } = require('../util/fetchInfoFromDB');
const rdbInstance = require('../helper/rdb');

const retryLoanProcess = async (loanId) => {
  try {
    const { loanData, userIdentifier } = await getLoanInformation(loanId);
    await rdbInstance.ref('retryLoans/' + loanId).update({
      startProcess: 'start',
    });
    await createLoan(loanData, loanId, userIdentifier);
    await rdbInstance.ref('retryLoans/' + loanId).update({
      startProcess: 'Done',
    });
  } catch (err) {
    console.error('RETRY_LOAN_FAILED');
    console.error(err);
    await rdbInstance.ref('retryLoans/' + loanId).update({
      startProcess: 'Failed',
    });
  }
};

module.exports = retryLoanProcess;
