const {
	getUserInformation,
	getClientInformation,
	sendNotificationsAndSaveData,
} = require('../util/fetchInfoFromDB');

const clientList = require('../constant/clientList');

const getStatusConstantInformation = require('../helper/peoplestrongHelper');
const { sendStatusToPS } = require('./sendLoanStatus');
const updateLoanInDB = require('../util/updateLoanInDB');
const updateLoanProcess = async (loanId, userIdentifier, loanData) => {
	try {
		let { syncStatus, status, employerName } = loanData;
		const userData = await getUserInformation(userIdentifier);
		const statusData = await getStatusConstantInformation(status);
		const employerData = await getClientInformation(employerName);
		let { clientName } = employerData;
		const { statusCode, updateLoanNotification } = statusData;
		const { body, title } = updateLoanNotification;

		//Send Loan Update Notification
		const payload = {
			notification: {
				title: title,
				body: body,
			},
		};
		const { device_token } = userData.userProfile;
		await sendNotificationsAndSaveData(
			userIdentifier,
			device_token,
			payload,
			title,
			body
		);
		clientName = clientName.toLowerCase();
		if (clientName === clientList.PEOPLESTRONG) {
			const newSyncStatus = await sendStatusToPS(
				userIdentifier,
				statusCode,
				syncStatus
			);
			await updateLoanInDB(
				{ syncStatus: newSyncStatus },
				loanId,
				userIdentifier
			);
		}
	} catch (err) {
		if (err.code === '101' || err.code === '100' || err.code === '200') {
			await updateLoanInDB({ syncStatus: err.message }, loanId, userIdentifier);
		}
		console.error('UPDATE_LOAN_FAILED');
		console.error(err);
	}
};

module.exports = updateLoanProcess;
