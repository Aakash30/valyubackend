const handleLoanError = require('../util/handleLoanError');
const panKyc = require('../kudos/panKyc');
const forcedKycNameCheck = async (
	panNumber,
	firstName,
	lastName,
	loan_status,
	original,
	partner_loan_id,
	number,
	status
) => {
	let runnable = false;
	let err;
	let { result, error } = await panKyc(
		panNumber,
		config.VALYU_CODE,
		null,
		number,
		loan_status,
		original,
		partner_loan_id,
		status
	);

	if (
		result &&
		result.data.FirstName.toLowerCase() === firstName.toLowerCase() &&
		result.data.LastName.toLowerCase() === lastName.toLowerCase()
	) {
		runnable = true;
	} else if (error) {
		err = {
			status: 'Kyc error issue',
			message: error.response.data.Message || error.response.data.message,
		};
		await handleLoanError(
			70,
			loan_status,
			original,
			partner_loan_id,
			number,
			err,
			status,
			'KYC'
		);
		runnable = false;
		throw new Error('KYC FAILED');
	} else {
		err = {
			status: 'Kyc error issue',
			message: 'KYC Failed',
		};
		await handleLoanError(
			70,
			loan_status,
			original,
			partner_loan_id,
			number,
			err,
			status,
			'KYC'
		);
		runnable = false;
		throw new Error('KYC FAILED');
	}
	return runnable;
};

module.exports = forcedKycNameCheck;
