const sendStatusToQuess = require('./statusToQuess');
const sendStatusToPS = require('./statusToPS');

module.exports = {
  sendStatusToQuess,
  sendStatusToPS,
};
