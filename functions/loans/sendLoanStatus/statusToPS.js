const config = require('../../config/index.config');
const handleData = require('../../handleData').handleData;
const getHeader = async (access_token) => {
  const options = {
    headers: {
      ApiKey: config.PUSH_TO_PS.apiKey,
      Authorization: 'Bearer ' + access_token,
    },
  };
  return options;
};

const getBody = async (userIdentifier, statusCode) => {
  const body = {
    custRefNo: userIdentifier,
    partnerCode: 'VLU',
    statusCode: statusCode,
    statusMessage: 'Success',
  };
  return body;
};
const sendStatusToPS = async (userIdentifier, statusCode, syncStatus) => {
  //GET ACCESS TOKEN
  let { result, error } = await handleData(
    config.GET_ACCESS_TOKEN.url,
    config.GET_ACCESS_TOKEN.body,
    {},
    'form'
  );
  if (error) {
    console.error('Some error occured GET_ACCESS_TOKEN');
    throw { code: '100', message: 'GET_ACCESS_TOKEN_FAILED' };
  }
  const { data } = result;
  if (data) {
    console.log('Success For GET_ACESS_TOKEN');
    const { access_token } = data;
    const body = await getBody(userIdentifier, statusCode);
    const options = await getHeader(access_token);
    //send data
    ({ result, error } = await handleData(
      config.PUSH_TO_PS.url,
      body,
      options
    ));
    if (error) {
      console.log('Some error occured PUSH TO PEOPLESTRONG');
      throw { code: '101', message: 'PeopleStrong Api Returns Failure' };
    }
    if (result) {
      console.log('Success For PUSH TO PS');
      syncStatus.push('PeopleStrong Api Returns Success');
    }
  }
  syncStatus = [...new Set(syncStatus)];

  return syncStatus;
};

module.exports = sendStatusToPS;
