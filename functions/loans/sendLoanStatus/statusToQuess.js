const config = require('../../config/index.config');
const handleLoanError = require('../../util/handleLoanError');
const handleData = require('../../handleData').handleData;
const {
	getUserInformation,
	getLoanInformation,
} = require('../../util/fetchInfoFromDB');

const sendStatusToQuess = async (loanId, intimationToQuess) => {
	const { loanData, userIdentifier } = await getLoanInformation(loanId);
	if (!loanData) {
		console.error('LoanID does not exist');
		throw { code: '203', message: 'LoanID does not exist' };
	}
	const userData = await getUserInformation(userIdentifier);
	let {
		employerName,
		disbursementDate,
		disbursement_date,
		amount,
		interestRate,
		repaymentAmount,
		firstEmiDate,
		syncStatus,
		status,
		err,
	} = loanData;
	const { firstName, lastName } = userData.userProfile;
	const { employeeId } = userData.workProfile[employerName].userData;
	//19/03/2021
	let sendDate;
	if (disbursementDate) {
		sendDate = disbursementDate.replaceAll('/', '-');
	} else if (disbursement_date) {
		sendDate = disbursement_date.replaceAll('/', '-');
	}

	if (status != 50) {
		throw { code: '202', message: 'QPAYAPI CAN NOT BE USED' };
	}

	const fullName = firstName + '' + lastName;
	let body = {
		Loan_Number: loanId,
		Name: fullName,
		Employee_code: employeeId,
		Loan_date: sendDate, //"30-01-2020"
		Loan_Amount: amount,
		Tenure: '1',
		LoanStartDate: sendDate,
		Monthly_EMIAMount: repaymentAmount, //repayment amount
		Interest_percentage: interestRate,
		Vendor_ID: config.QUESS_VENDOR_ID, //config
	};
	let { result, error } = await handleData(
		config.QUESS_LOAN_APPROVAL_API_URL,
		body,
		{}
	);
	if (!result) {
		loanData.intimationToQuess = intimationToQuess;
		let err = {};
		err.message = 'QPAY_LOANAPI FAILED';

		await handleLoanError(
			status,
			syncStatus,
			loanData,
			loanId,
			userIdentifier,
			err,
			'QPAY_LOANAPI FAILED',
			'QPAY_LOANAPI'
		);
		throw {
			code: '200',
			message: 'QPAY_LOANAPI FAILED',
		};
	}
	syncStatus.push('QPAY_LOAN_API PASSED');
	body = await getSchedulerApiRequestBody(
		loanId,
		fullName,
		employeeId,
		firstEmiDate,
		repaymentAmount
	);
	({ result, error } = await handleData(
		config.QUESS_SCHEDULE_DETAIL_API_URL,
		body,
		{}
	));
	if (!result) {
		loanData.intimationToQuess = intimationToQuess;
		let err = {};
		err.message = 'QPAY_SCHEDULEAPI FAILED';

		await handleLoanError(
			status,
			syncStatus,
			loanData,
			loanId,
			userIdentifier,
			err,
			'QPAY_SCHEDULEAPI FAILED',
			'QPAY_SCHEDULEAPI'
		);
		throw {
			code: '201',
			message: 'QPAY_SCHEDULEAPI FAILED',
		};
	}
	syncStatus.push('QPAY_SCHEDULEAPI PASSED');
	return syncStatus;
};

const getSchedulerApiRequestBody = async (
	partner_loan_id,
	fullName,
	employeeId,
	firstEmiDate,
	repaymentAmount
) => {
	let body = [
		{
			Loan_Number: partner_loan_id,
			Name: fullName,
			Employee_code: employeeId,
			EMI_duedate: firstEmiDate,
			EMI_Amount: repaymentAmount,
			Vendor_ID: config.QUESS_VENDOR_ID,
		},
	];
	return body;
};

module.exports = sendStatusToQuess;
