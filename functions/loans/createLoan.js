const { momentInstance } = require('../helper/moment');
const productNames = require('../constant/productNames.constant');
const moment = require('moment-timezone');
const getDetailsBySalary = require('../users/getDetailsBySalary');
const {
	getUserInformation,
	getClientInformation,
	sendNotificationsAndSaveData,
} = require('../util/fetchInfoFromDB');
const calculateFeeAmount = require('./calculateFeeAmount');
const callKudos = require('./callKudos');

const createLoan = async (loanData, partnerLoanId, userIdentifier) => {
	try {
		const { amount, created_at, employerName } = loanData;

		let startDate = momentInstance.format('YYYY-MM-DD');
		let userData = await getUserInformation(userIdentifier);
		let {
			product_name,
			salary,
			totalSuccessfullApplication,
			applied_amount,
		} = userData.workProfile[employerName].userData;

		//Get client info
		let employerData = await getClientInformation(employerName);

		let firstEmiDate = employerData[product_name].settlementDate;
		let {
			blackoutStartDate,
			blackoutEndDate,
			maxAllowedTransaction,
		} = await getDetailsBySalary(employerData, product_name, salary);

		let sum = 0;
		//Calculate Loan applied amount
		sum = applied_amount ? applied_amount + amount : amount;

		//Send Notifcation and Save Notification to RD
		let deviceToken = userData.userProfile.device_token;
		let { title, body } = employerData.createLoanNotification;
		const payload = {
			notification: {
				title: title,
				body: body,
			},
		};

		sendNotificationsAndSaveData(
			userIdentifier,
			deviceToken,
			payload,
			title,
			body
		);

		let disbursementDate = momentInstance.format('DD/MM/YYYY'); //Current Date

		// TODO: rename this please..
		let isBlackoutPeriod = moment(
			moment(created_at).format('YYYY-MM-DD')
		).isBetween(moment(blackoutStartDate), moment(blackoutEndDate), null, '[]');

		let dateDiff = moment(moment(firstEmiDate)).diff(moment(startDate), 'days');
		let {
			processing_amount,
			cgst_on_processing_amount,
			sgst_on_processing_amount,
			cgst_on_platform_fee,
			sgst_on_platform_fee,
		} = await calculateFeeAmount(employerData, product_name, amount, salary);

		let totalGstOnProcessingAmount =
			cgst_on_processing_amount + sgst_on_processing_amount;

		let totalGstOnPlatformFee = cgst_on_platform_fee + sgst_on_platform_fee;
		let { platform_fees, interest_rate } = employerData[product_name];
		let totalLoanApplied;
		let SI = 0.0;
		let htmlfilename;
		if (product_name == productNames.ADVANCE_LOAN_SUBSCRIPTION) {
			// TODO: constant use
			htmlfilename = 'clickwrapSubscription';

			if (totalSuccessfullApplication == 0) {
				totalLoanApplied =
					amount + processing_amount + totalGstOnProcessingAmount;
			} else {
				totalLoanApplied = amount;
				processing_amount = 0;
				cgst_on_processing_amount = 0;
				sgst_on_processing_amount = 0;
				totalGstOnProcessingAmount = 0;
			}
		} else {
			htmlfilename = 'clickWrap';
			totalLoanApplied =
				amount +
				processing_amount +
				totalGstOnProcessingAmount +
				platform_fees +
				totalGstOnPlatformFee;
			SI = parseFloat(
				(totalLoanApplied * interest_rate * dateDiff) / (100).toFixed(2)
			);
		}

		let repaymentAmount = totalLoanApplied + parseFloat(SI);
		let { NBFC_CLIENT, cgst, sgst } = employerData[product_name]; // TODO: verify this once..

		let cgst_on_platform_Fee = employerData[product_name].cgst_on_platform_fee;
		let sgst_on_platform_Fee = employerData[product_name].sgst_on_platform_fee;

		if (NBFC_CLIENT == 'Kudos') {
			loanData = await updateData(
				SI,
				loanData,
				totalLoanApplied,
				platform_fees,
				processing_amount,
				repaymentAmount,
				cgst,
				sgst,
				totalGstOnProcessingAmount,
				cgst_on_platform_Fee,
				sgst_on_platform_Fee,
				totalGstOnPlatformFee,
				disbursementDate,
				dateDiff,
				firstEmiDate,
				interest_rate
			);
			let timestamp = momentInstance.unix();
			let loan_status = [];

			await callKudos(
				maxAllowedTransaction,
				loanData,
				isBlackoutPeriod,
				sum,
				userData, //Can call in Kudos
				employerData, // Can call in Kudos
				product_name, // can call in kudos
				partnerLoanId,
				userIdentifier,
				startDate,
				timestamp,
				htmlfilename,
				loan_status
			);
		}
	} catch (err) {
		console.error('CREATE_LOAN_FAILED');
		console.error(err);
		throw new Error('CREATE_LOAN_FAILED');
	}
};

const updateData = async (
	SI,
	loanData,
	totalLoanApplied,
	platform_fees,
	processing_amount,
	repaymentAmount,
	cgst,
	sgst,
	totalGstOnProcessingAmount,
	cgstOnPlatformFee,
	sgstOnPlatformFee,
	totalGstOnPlatformFee,
	disbursementDate,
	dateDiff,
	firstEmiDate,
	interestRate
) => {
	loanData.SI = SI;
	loanData.totalLoanApplied = totalLoanApplied;
	loanData.platformFees = platform_fees || 0;
	loanData.processingFees = processing_amount;
	loanData.repaymentAmount = repaymentAmount;
	loanData.cgstOnProcessingAmount = cgst;
	loanData.sgstOnProcessing_amount = sgst;
	loanData.totalGstOnProcessingAmount = totalGstOnProcessingAmount;
	loanData.cgstOnPlatform_fee = cgstOnPlatformFee || 0;
	loanData.sgstOnPlatform_fee = sgstOnPlatformFee || 0;
	loanData.totalGstOnPlatformFee = totalGstOnPlatformFee;
	loanData.disbursementDate = disbursementDate;
	loanData.loanTenureInDays = dateDiff;
	loanData.firstEmiDate = firstEmiDate;
	loanData.interestRate = interestRate || 0.0;

	return loanData;
};

module.exports = createLoan;
