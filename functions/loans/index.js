const functions = require('firebase-functions');
const createLoan = require('./createLoan');
const retryLoanProcess = require('./retryLoanProcess');
const updateLoanProcess = require('./updateLoanProcess');

const runtimeOpts = {
  timeoutSeconds: 300,
  memory: '1GB',
};

const createLoanFuncCall = functions
  .runWith(runtimeOpts)
  .database.ref('users/{number}/Loan/{loan_id}')
  .onCreate(async (snapshot, context) => {
    // Grab the current value of what was written to the Realtime Database.
    console.log('Cloud Function Create Loan Started');
    let loanData = snapshot.val();
    const userIdentifier = context.params.number;
    let partnerLoanId = context.params.loan_id;
    await createLoan(loanData, partnerLoanId, userIdentifier);
    return true;
  });

const retryLoanProcessFuncCall = functions
  .runWith(runtimeOpts)
  .database.ref('retryLoans/{loan_id}')
  .onCreate(async (snapshot, context) => {
    // Grab the current value of what was written to the Realtime Database.
    console.log('Cloud Function Retry Loan Process');
    let loanId = context.params.loan_id;
    await retryLoanProcess(loanId);
    return true;
  });

const updateLoanFuncCall = functions
  .runWith(runtimeOpts)
  .database.ref('users/{number}/Loan/{loan_id}')
  .onUpdate(async (change, context) => {
    console.log('Cloud Function Update Loan Process');
    let loanId = context.params.loan_id;
    let userIdentifier = context.params.number;
    let updatedLoanData = change.after.val();
    await updateLoanProcess(loanId, userIdentifier, updatedLoanData);
    return true;
  });

module.exports = {
  createLoan: createLoanFuncCall,
  retryLoan: retryLoanProcessFuncCall,
  updateLoan: updateLoanFuncCall,
};
