const { getClientInformation } = require('../util/fetchInfoFromDB');
const rdbFireStoreInstance = require('../helper/fireStore');
const { configureAttendanceEmailOptions } = require('../helper/emailHelper');
const rdbInstance = require('../helper/rdb');
const { momentInstance, momentInstanceOnDate } = require('../helper/moment');
const updateEligibility = require('../util/updateEligibility');
const sendMail = require('../util/sendEmail');
const { asyncForEach } = require('../util/asyncForEach');
const cronJobToUpdateEligibility = async () => {
	try {
		console.log('In cronJob function');
		let currentDate = momentInstance.format('YYYY-MM-DD');
		let clientFromFirestore = await rdbFireStoreInstance.listCollections();
		clientFromFirestore.map(async (collectionName) => {
			if (collectionName.id == 'mail') {
				return;
			}
			let clientUserData = await rdbFireStoreInstance
				.collection(collectionName.id)
				.get();

			clientUserData.forEach(async (userData) => {
				let userIdentifier = userData.id;
				let {
					employeeId,
					product_name,
					salary,
					currentEmployer,
					doj,
				} = userData.data();

				let clientData = await getClientInformation(currentEmployer);
				let { validateAttendanceCount } = clientData;

				let workProfileUserDataInfo = rdbInstance.ref(
					'users/' +
						userIdentifier +
						'/workProfile/' +
						currentEmployer +
						'/userData'
				);
				workProfileUserDataInfo = await workProfileUserDataInfo.once('value');
				workProfileUserDataInfo = workProfileUserDataInfo.val();
				let { eligibility_date } = workProfileUserDataInfo;

				//Check cron already run for today

				let getDay = (await momentInstanceOnDate(currentDate)).diff(
					eligibility_date,
					'days'
				);

				if (getDay == 0) {
					console.log(`Cron already run for user ${userIdentifier} today`);
					return;
				}

				await updateEligibility(
					userIdentifier,
					currentEmployer,
					clientData,
					product_name,
					salary,
					employeeId,
					doj,
					currentDate
				);
				//Get attendance between two dates
				let attendanceData = await retreiveAttendanceFromDB(
					validateAttendanceCount,
					userIdentifier,
					currentEmployer
				);

				let attendanceDateKey = Object.keys(attendanceData);
				await checkUserIsVerified(
					attendanceDateKey,
					validateAttendanceCount,
					userIdentifier,
					currentEmployer,
					attendanceData
				);
			});
		});
		return true;
	} catch (error) {
		//console.log(error);
		console.log('Error', error);
	}
};

const retreiveAttendanceFromDB = async (
	validateAttendanceCount,
	userIdentifier,
	client
) => {
	const attendanceData = await rdbInstance
		.ref('users/' + userIdentifier + '/workProfile')
		.child(client + '/userData')
		.child('attendance')
		.limitToLast(validateAttendanceCount)
		.once('value');
	return attendanceData.val();
};

const checkUserIsVerified = async (
	attendanceDateKey,
	validateAttendanceCount,
	userIdentifier,
	currentEmployer,
	attendanceData
) => {
	if (attendanceDateKey.length != validateAttendanceCount) {
		return;
	}
	let sendEmailFlag = true;
	await asyncForEach(attendanceDateKey, async (date) => {
		let { isDefaultAttendanceUsed } = attendanceData[date];
		if (!isDefaultAttendanceUsed) {
			sendEmailFlag = false;
			return;
		}
	});
	if (sendEmailFlag) {
		let sendData = {
			message: `User attendance is not available for Consecutive ${validateAttendanceCount} days`,
		};
		const mailOptions = await configureAttendanceEmailOptions(
			sendData,
			userIdentifier
		);
		await sendMail(mailOptions);

		await rdbInstance
			.ref('users/' + userIdentifier)
			.child('userProfile')
			.update({ isUserVerified: false });

		await rdbInstance
			.ref('users/' + userIdentifier + '/workProfile/' + currentEmployer)
			.child('companyDetails')
			.update({ disbursementAutomation: false });
	}
	return;
};

module.exports = cronJobToUpdateEligibility;
