const functions = require('firebase-functions');
const cronJobToUpdateEligibility = require('./cronJobToUpdateEligibility');
const runtimeOpts = {
	timeoutSeconds: 300,
	memory: '1GB',
};
exports.cronJobToUpdateEligibility = functions
	.runWith(runtimeOpts)
	.https.onRequest(async (req, res) => {
		console.log(
			'Crone Run On Every Day at 23:59 PM, CURRENT TIME IS ',
			new Date().toLocaleString()
		);
		await cronJobToUpdateEligibility();
		console.log('cronjob works');
		return true;
	});
