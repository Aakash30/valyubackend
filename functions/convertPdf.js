let os = require('os');
let PDFDocument = require('pdfkit');
const admin = require('firebase-admin');
const config = require('./config/index.config');
const Path = require('path');
const fs = require('fs');
const puppeteer = require('puppeteer');
const hb = require('handlebars');
const { compressedImage } = require('./compression');

const getImage = async (path, filename) => {
	path = path.slice(1);
	//console.log(path);
	let file = undefined;
	let destinationpath = Path.join(os.tmpdir(), `${filename}.jpg`);
	let hasToCompressed = true;

	try {
		const { size } = await admin
			.storage()
			.bucket(config.BUCKET_NAME)
			.file(path)
			.getMetadata();

		if (size < 1000000) {
			//In bytes
			hasToCompressed = false;
		}

		file = await admin
			.storage()
			.bucket(config.BUCKET_NAME)
			.file(path)
			.download({
				destination: destinationpath,
			});

		if (hasToCompressed) await compressedImage(destinationpath, filename);

		return file;
	} catch (error) {
		console.log(error);
		return file;
	}
};
const getPdf = async (path) => {
	path = path.slice(1);
	//console.log(path);
	let file = undefined;
	try {
		file = await admin
			.storage()
			.bucket(config.BUCKET_NAME)
			.file(path)
			.download();

		return file;
	} catch (error) {
		//console.log(error);
		return file;
	}
};

const convertintopdf = async (filename, filename2, file, type, data) => {
	let doc = new PDFDocument();
	doc.pipe(fs.createWriteStream(Path.join(os.tmpdir(), file)));

	if (filename != undefined) {
		let documentimgpath1 = Path.join(os.tmpdir(), filename);
		doc.image(documentimgpath1, {
			fit: [500, 400],
			align: 'center',
			valign: 'center',
		});
	}
	if (filename2 != undefined) {
		let documentimgpath2 = Path.join(os.tmpdir(), filename2);
		doc.addPage().image(documentimgpath2, {
			fit: [500, 400],
			align: 'center',
			valign: 'center',
		});
	}
	if (type) {
		let { netSalary, grossSalary, salaryByPS, salary } = data;
		let addEncryptionDataFlag = false;
		data.salary = salaryByPS ? salaryByPS : salary;
		if (salaryByPS) delete data.salaryByPS;
		if (netSalary) {
			addEncryptionDataFlag = true;
			delete data.netSalary;
		}
		if (grossSalary) {
			addEncryptionDataFlag = true;
			delete data.grossSalary;
		}

		delete data.timestamp;
		doc.text(`User Info Request `, { underline: true });
		doc.moveDown(2);
		doc.text(`${JSON.stringify(data)}`);
		doc.moveDown(2);
		if (addEncryptionDataFlag) {
			doc.text(`Salary Decryption Data `, { underline: true });
			doc.moveDown(2);
			doc.text(`${JSON.stringify({ netSalary, grossSalary })}`);
		}
	}
	doc.end();
};

const htmlToPdf = async (filename, data) => {
	let isPdfgenerated = false;
	try {
		let html = fs.readFileSync(
			process.cwd() + `/public/${filename}.html`,
			'utf8'
		);
		const browser = await puppeteer.launch();
		const page = await browser.newPage();
		const template = hb.compile(html, { strict: false }); // we have compile our code with handlebars
		const result = template(data);
		await page.setContent(result);
		await page.evaluateHandle('document.fonts.ready');

		await page.pdf({
			path: Path.join(os.tmpdir(), `${filename}.pdf`),
			format: 'A4',
		});
		isPdfgenerated = true;
		await browser.close();
	} catch (error) {
		console.log(error);
		isPdfgenerated = false;
	}
	console.log(isPdfgenerated);
	return isPdfgenerated;
};

module.exports = {
	getImage,
	getPdf,
	convertintopdf,
	htmlToPdf,
};
