const admin = require('./firebase');
const config = require('../config/index.config');

const storageInstance = admin.storage().bucket(config.BUCKET_NAME);
module.exports = storageInstance;
