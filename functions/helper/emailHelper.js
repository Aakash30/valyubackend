const config = require('../config/index.config');
const configureMailOptions = async (
	to,
	partner_loan_id,
	fullname,
	employeeId,
	number,
	emailId,
	joiningDate,
	birthDate,
	gender,
	address,
	now,
	monthlySalary,
	eligible_amount,
	amount,
	total_loan_applied,
	transaction_id,
	clientId,
	clientName,
	loan_path,
	aadhar_path,
	pan_path,
	available_amount,
	payDate,
	disbursementDate
) => {
	const mailOptions = {
		to: to,
		message: {
			subject: `Advance Salary approval request for ${partner_loan_id} at ${config.PROJECT_ID}`, // email subject
			html: `<div>\
		<p>Hello,</p>\
		<p>One of your employee has requested for Advance Salary disbursal on our platform with the following Details.</p>\
		<ul>\
		<li>Name- ${fullname}</li>\
		<li>Employee Id- ${employeeId}</li>\
		<li>Contact Number – ${number}</li>\
		<li>Email Id – ${emailId}</li>\
		<li>Date of Joining – ${joiningDate}</li>\
		<li>Date of Birth – ${birthDate}</li>\
		<li>Gender – ${gender}</li>\
		<li>Address – ${address}</li>\
		<li>Requested on – ${now}</li>\
		<li>Monthly Salary – ${monthlySalary}</li>\
		<li>Accured Salary – ${eligible_amount}</li>\
		<li>Maximum Eligibility – ${available_amount}</li>\
		<li>Requested Amount – ${amount}</li>\
		<li>Total Settlement / Deduction Amount – ${total_loan_applied}</li>\
		<li>Transaction Reference Number – ${transaction_id}</li>\
		<li>Salary PayoutDate – ${payDate}</li>\
		<li>Planned Disbursement Date – ${disbursementDate}</li>\
		</ul>\
		<p><u>Action required</u></p>
		<p><span>&#10003;</span>Kindly Validate the above-mentioned details as per your records.</p>
		<p><span>&#10003;</span>And please provide your consent for disbursing the requested amount by replying "YES" to this email.</p>
		<p>Please note that after this disbursal, you would be required to deduct ${total_loan_applied} from this month’s payroll of this employee and deposit the same with Valyu.ai against recovery of this disbursal.</p>\
		<p>Thank You,<br>Team Valyu AI</p>\
		<div>\
		 <p>Tech Use Only</p>
		 <ul>\
		 <li>Client Id- ${clientId}</li>\
		 <li>Client Name- ${clientName}</li>\
		 <li>Loan ID- ${partner_loan_id}</li>\
		 <li>PROJECT ID- ${config.PROJECT_ID}</li>\
		 </ul>\
		</div>
		</div>`, // email content in HTML, // email content in HTML
			attachments: [
				{ filename: 'loandocument.pdf', path: loan_path },
				{ filename: 'aadhar.pdf', path: aadhar_path },
				{ filename: 'pan.pdf', path: pan_path },
			],
		},
	};
	return mailOptions;
};

const configureErrorEmailOptions = async (
	data,
	name,
	loan_id,
	configureErrorData
) => {
	const mailOptions = {
		from: config.EMAIL,
		to: ['lalit.chaturvedi@valyu.ai', config.EMAIL],
		message: {
			subject: `Here is Loan Error request for ${loan_id} at ${config.PROJECT_ID}`, // email subject
			html: `<div>\
		<p>Here is the Error information for ${name} for a ${loan_id},</p>\
		<p><u>Data</u></p>
		<div>${JSON.stringify(data)}</div>
		<p><u>User Information are below</u></p>
		<div>${JSON.stringify(configureErrorData)}</div>
		<p>Thank You</p>
		</div>`, // email content in HTML, // email content in HTML
		},
	};
	//	const { result, err_info } = await sendMail(mailOptions);
	return mailOptions;
};
const configureAttendanceEmailOptions = async (data, userIdentifier) => {
	const mailOptions = {
		from: config.EMAIL,
		to: ['lalit.chaturvedi@valyu.ai', config.EMAIL],
		message: {
			subject: `Here is  Error request for ${userIdentifier} at ${config.PROJECT_ID}`, // email subject
			html: `<div>\
		<p>Here is the Error information for UserAttendance for a ${userIdentifier},</p>\
		<p><u>Data</u></p>
		<div>${JSON.stringify(data)}</div>
		<p>Thank You</p>
		</div>`, // email content in HTML, // email content in HTML
		},
	};
	//	const { result, err_info } = await sendMail(mailOptions);
	return mailOptions;
};

module.exports = {
	configureMailOptions,
	configureErrorEmailOptions,
	configureAttendanceEmailOptions,
};
