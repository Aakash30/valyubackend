const moment = require('moment-timezone');

const momentInstance = moment().tz('Asia/Kolkata');

const momentInstanceOnDate = async (date) => {
	return moment(date).tz('Asia/Kolkata');
};

const momentInstanceToFormat = async (date, format) => {
	return moment(date, format).tz('Asia/Kolkata');
};
module.exports = {
	momentInstance,
	momentInstanceOnDate,
	momentInstanceToFormat,
};
