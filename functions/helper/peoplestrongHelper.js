const rdbInstance = require('../helper/rdb');
const getStatusConstantInformation = async (status) => {
  let statusData = rdbInstance.ref('Constant/loan_ID/' + status + '/');
  statusData = await statusData.once('value');
  statusData = statusData.val();

  return statusData;
};
module.exports = getStatusConstantInformation;
