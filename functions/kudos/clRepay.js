const handleData = require('../handleData').handleData;
const handleLoanError = require('../util/handleLoanError');
const selectedKudosConfig = require('../util/getKudosConfig');
const momentInstance = require('../helper/moment');
const loanStatuses = require('../constant/loanStatuses.constant');
const rdbInstance = require('../helper/rdb');
const { PAYMENT } = require('../constant/transactionPurpose.constant');
const { CREDIT } = require('../constant/transactionType.constant');

const clRepay = async (
	txn_amount,
	txn_date,
	txn_id,
	loanData,
	userIdentifier,
	userData
) => {
	const { employerName, status } = loanData;
	const workUserData = userData.workProfile[employerName].userData;
	const { user_id } = userData.userProfile;
	const {
		kudos_borrower_id,
		kudos_loan_id,
		partner_loan_id,
		transactionId,
		product_name,
	} = workUserData;
	const { syncStatus } = loanData;
	const config = await selectedKudosConfig(product_name);
	const { cl_repay: url, options, virtual_address_id } = config;
	let body = [
		{
			kudos_borrower_id,
			kudos_loan_id,
			partner_loan_id,
			partner_borrower_id: user_id,
			vpa_id: virtual_address_id,
			txn_id: txn_id,
			txn_amount: txn_amount,
			txn_date: txn_date,
			txn_reference: 'Reference ' + txn_id,
		},
	];

	let { result, error } = await handleData(url, body, options);
	if (error) {
		error = {
			status: 'Loan CL Repay url failed',
			message: error.response.data.message,
			data: JSON.stringify(error.response.data.data || error.response.data),
		};
		await handleLoanError(
			status,
			syncStatus,
			loanData,
			loanData.id,
			userIdentifier,
			error,
			loanStatuses['52'],
			'ClRepay'
		);
		//throw new Error('CLRepay Failed');
	} else {
		syncStatus.push(loanStatuses['51']);

		await rdbInstance
			.ref('users/' + userIdentifier + '/Loan/' + loanData.id)
			.update({
				syncStatus: syncStatus,
				isRepaymentApiCompleted: true,
			});
		let timestamp = new Date().getTime();
		await rdbInstance
			.ref('users/' + userIdentifier + '/Transaction/')
			.child(timestamp)
			.set({
				txn_amount: txn_amount,
				txn_date: txn_date,
				txn_reference: 'Reference ' + txn_id,
				vpa_id: virtual_address_id,
				loanId: loanData.id,
				txn_id: txn_id,
				type: CREDIT,
				purpose: PAYMENT,
				timestamp: timestamp,
			});
	}

	return {
		isSuccess: error ? false : true,
		result,
		error,
		data: { syncStatus },
	};
};
module.exports = clRepay;
