const handleData = require('../handleData').handleData;
const handleLoanError = require('../util/handleLoanError');
const config = require('../config/index.config');
const panKyc = async (
	pan,
	sub_company_code,
	kudos_loan_id,
	number,
	loanStatus,
	original,
	partnerLoanId,
	status
) => {
	let body = {
		pan,
		sub_company_code,
		kudos_loan_id,
	};

	let { result, error } = await handleData(
		config.KUDOS.pan_kyc,
		body,
		config.KUDOS.options1
	);
	if (error) {
		let err = {
			status: 'Kyc error issue',
			message: error.response.data.Message || error.response.data.message,
		};
		await handleLoanError(
			70,
			loanStatus,
			original,
			partnerLoanId,
			number,
			err,
			status,
			'KYC'
		);
		throw new Error('KYC FAILED');
	}
	return { result, error };
};
module.exports = panKyc;
