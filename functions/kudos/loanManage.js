const handleData = require('../handleData').handleData;
const handleLoanError = require('../util/handleLoanError');
const selectedKudosConfig = require('../util/getKudosConfig');
const loanStatuses = require('../constant/loanStatuses.constant');
const { momentInstance } = require('../helper/moment');
const rdbInstance = require('../helper/rdb');
const loanManage = async (loanData, userIdentifier, userData, employerData) => {
	const employerName = loanData.employerName;
	const workUserData = userData.workProfile[employerName].userData;
	const {
		creditLimit,
		kudos_borrower_id,
		kudos_loan_id,
		partner_loan_id,
		virtual_account_no,
		product_name,
	} = workUserData;
	const { user_id } = userData.userProfile;

	let processingAmount = loanData.processingFees;
	const { platform_fees, interest_rate } = employerData[product_name];
	let startDate = momentInstance.format('YYYY-MM-DD');
	let transactionId = new Date().getTime() + 'T' + userIdentifier;
	let { syncStatus, id } = loanData;

	let body = [
		{
			kudos_borrower_id,
			kudos_loan_id,
			partner_loan_id,
			partner_borrower_id: user_id,
			partner_loan_status: 'ACTIVE',
			opening_bal: creditLimit,
			kudos_proc_fee: processingAmount || 0.1,
			conve_fee_amnt: platform_fees || 0.1,
			usable_balance: creditLimit,
			tenure: 30,
			intrest_rate: interest_rate || 0.1,
			dpd_rate: 10000, //TODO
			vpa_address: virtual_account_no,
			txn_date: startDate,
			txn_reference: 'This is reference for the transaction ' + transactionId,
		},
	];

	const config = await selectedKudosConfig(product_name);
	const { loan_manage_url: url, options } = config;
	const { result, error } = await handleData(url, body, options);
	if (error) {
		let err = {
			status: 'Loan Manage Url failed',
			message: error.response.data.message,
			data: JSON.stringify(error.response.data.data || error.response.data),
		};

		await handleLoanError(
			80,
			syncStatus,
			loanData,
			id,
			userIdentifier,
			err,
			loanStatuses['23'],
			'LoanManage'
		);
		throw new Error('LoanManage Failed');
	} else {
		await rdbInstance
			.ref('users/' + userIdentifier + '/workProfile/' + employerName)
			.child('userData')
			.update({
				transactionId,
				isLoanManageCompleted: true,
			});

		syncStatus.push(loanStatuses['24']);

		await rdbInstance
			.ref('users/' + userIdentifier + '/Loan/' + loanData.id)
			.update({
				syncStatus: syncStatus,
			});
	}

	return {
		isSuccess: error ? false : true,
		result,
		error,
		data: { syncStatus, transactionId },
	};
};

module.exports = loanManage;
