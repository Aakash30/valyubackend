const handleData = require('../handleData').handleData;
const paytmDisbursal = async (
	number,
	user_data,
	amount,
	employerName,
	response,
	loanStatus,
	original,
	partnerLoanId,
	status
) => {
	let { ifscCode, bankAcNo } = user_data.workProfile[employerName].account;
	bankAcNo = bankAcNo.replace(/\s+/g, '');
	ifscCode = ifscCode.replace(/\s+/g, '');
	let body = {
		borrower_mobile: number,
		ifsc_code: ifscCode,
		amount: amount,
		account_no: bankAcNo,
		...response,
		order_id: number + new Date().getTime(),
	};

	let { result, error } = await handleData(
		config.KUDOS.paytm_disbursal_url,
		body,
		config.KUDOS.options1
	);
	if (error) {
		let err = {
			status: 'Paytm disbursal failed',
			message: error.response.data.statusMessage,
			code: error.response.data.statusCode,
		};
		await handleLoanError(
			70,
			loanStatus,
			original,
			partnerLoanId,
			number,
			err,
			status,
			'PaytmDisbursal'
		);
		throw new Error('PaytmDisbursal Failed');
	}

	return { result, error };
};
module.exports = paytmDisbursal;
