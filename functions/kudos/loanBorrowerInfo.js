const handleData = require('../handleData').handleData;
const selectedKudosConfig = require('../util/getKudosConfig');
const handleLoanError = require('../util/handleLoanError');
const rdbInstance = require('../helper/rdb');
const loanBorrowerInfo = async (
	response,
	firstEmiDate,
	employerName,
	employerData,
	userData,
	doj,
	productName,
	startDate,
	age,
	original,
	partner_loan_id,
	number,
	loanStatus,
	status,
	creditLimit
) => {
	const workProfile = userData.workProfile[employerName];
	const { salary, employeeId, employmentType } = workProfile.userData;
	const { address, city, pinCode } = employerData;
	let body = [
		{
			...response,
			loan_app_date: startDate,
			age: age,
			qualification: 'Graduate',
			marital_status: 'single',
			sanction_amount: parseFloat(creditLimit),
			repayment_type: 30,
			employment_state: 'karnataka', //Check
			first_inst_date: firstEmiDate,
			disburse_date: startDate,
			bureau_name: 'CIBIL', //Need To Check
			final_approve_status: 'approved', //check
			final_approve_date: startDate,
			employment_addr_ln_1: address,
			employment_city: city,
			employment_pincode: pinCode,
			monthly_income_of_the_borro: parseFloat(salary),
			annual_income_of_the_borro: parseFloat(salary * 12),
			employer_name: employerName,
			employer_id: employeeId,
			joining_date: doj,
			job_type: employmentType,
			final_approved_by: 'admin',
			total_charges: '0',
			net_disbur_amt: '0',
			cgst_on_pf_amt: '0',
			sgst_on_pf_amt: '0',
			igst_on_pf_amt: '0',
		},
	];

	//

	const config = await selectedKudosConfig(productName);
	const { borrower_info_url: url, options } = config;

	const { result, error } = await handleData(url, body, options);
	if (error) {
		console.log(error);
		let err = {
			status: 'Loan Borrower info failed',
			message: error.response.data.message,
			data: error.response.data.data,
		};
		if (error.response.data.errorCode === '04') {
			await rdbInstance
				.ref('users/' + number + '/workProfile/' + employerName)
				.child('userData')
				.update({
					isLoanBorrowerInfoCompleted: true,
				});
		}
		await handleLoanError(
			70,
			loanStatus,
			original,
			partner_loan_id,
			number,
			err,
			status,
			'LoanBorrower'
		);
		throw new Error('Loan Borrower Info Failed');
	}
	return { result, error };
};
module.exports = loanBorrowerInfo;
