const handleData = require('../handleData').handleData;
const handleLoanError = require('../util/handleLoanError');
const selectedKudosConfig = require('../util/getKudosConfig');
const { momentInstance } = require('../helper/moment');
const loanStatuses = require('../constant/loanStatuses.constant');
const rdbInstance = require('../helper/rdb');
const { PAYMENT } = require('../constant/transactionPurpose.constant');
const { DEBIT } = require('../constant/transactionType.constant');

const loanUsuage = async (loanData, userIdentifier, userData) => {
	const employerName = loanData.employerName;
	const workUserData = userData.workProfile[employerName].userData;
	const { firstName, lastName, user_id } = userData.userProfile;
	const {
		kudos_borrower_id,
		kudos_loan_id,
		partner_loan_id,
		virtual_account_no,
		transactionId,
		product_name,
	} = workUserData;
	const { repaymentAmount, syncStatus } = loanData;
	let transactionDate = momentInstance.format('YYYY-MM-DD');
	let body = [
		{
			kudos_borrower_id,
			kudos_loan_id,
			partner_loan_id,
			partner_borrower_id: user_id,
			ac_holder_name: `${firstName} ${lastName}`,
			vpa_id: virtual_account_no,
			txn_id: transactionId,
			txn_amount: repaymentAmount,
			txn_date: transactionDate,
			txn_reference: 'Reference ' + transactionId,
			txn_timestamp: momentInstance.unix(),
			//	txn_usedtotal:,
			//	txn_balance:,
		},
	];

	const config = await selectedKudosConfig(product_name);
	const { loan_usage_url: url, options } = config;
	const { result, error } = await handleData(url, body, options);
	if (error) {
		const err = {
			status: 'Loan usuage url failed',
			message: error.response.data.message,
			data: JSON.stringify(error.response.data.data || error.response.data),
		};
		await handleLoanError(
			70,
			syncStatus,
			loanData,
			loanData.id,
			userIdentifier,
			err,
			loanStatuses['39'],
			'LoanUsuage'
		);
		throw new Error('LoanUsuage Failed');
	} else {
		syncStatus.push(loanStatuses['40']);
		let timestamp = new Date().getTime();

		await rdbInstance
			.ref('users/' + userIdentifier + '/Transaction/')
			.child(timestamp)
			.set({
				txn_amount: repaymentAmount,
				txn_date: transactionDate,
				txn_reference: 'Reference ' + transactionId,
				vpa_id: virtual_account_no,
				txn_id: transactionId,
				loanId: loanData.id,
				type: DEBIT,
				purpose: PAYMENT,
				timestamp: timestamp,
			});

		await rdbInstance
			.ref('users/' + userIdentifier + '/Loan/' + loanData.id)
			.update({
				syncStatus: syncStatus,
				isLoanUsageCompleted: true,
			});
	}

	return {
		isSuccess: error ? false : true,
		result,
		error,
		data: { syncStatus },
	};
};
module.exports = loanUsuage;
