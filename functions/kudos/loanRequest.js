const {
	SECTOR,
	INTERESET_TYPE,
	TENURE,
	CIBIL_SCORE,
	SYSTEM_SCORE,
} = require('../constant/kudosLoan.constant');
const handleData = require('../handleData').handleData;
const selectedKudosConfig = require('../util/getKudosConfig');
const handleLoanError = require('../util/handleLoanError');
const loanRequest = async (
	partnerLoanId,
	userData,
	number,
	purpose,
	employerName,
	employerData,
	productName,
	start_date,
	loanStatus,
	original,
	status,
	creditLimit
) => {
	const { userProfile, personal, workProfile } = userData;
	const selectedWorkProfileForUser = workProfile[employerName];
	const {
		account: userBankDetails,
		userData: userWorkDetails,
	} = selectedWorkProfileForUser;

	const {
		user_id,
		firstName,
		lastName,
		address1,
		city,
		pinCode,
		dob,
		gender,
	} = userProfile;
	let { panNumber } = personal;
	let { bankName, bankAcNo, ifscCode } = userBankDetails;
	let { interest_rate, processing_fee } = employerData[productName];

	panNumber = panNumber.replace(/\s+/g, '');
	bankAcNo = bankAcNo.replace(/\s+/g, '');
	ifscCode = ifscCode.replace(/\s+/g, '');

	const body = [
		{
			partner_loan_id: partnerLoanId,
			partner_borrower_id: user_id,
			first_name: firstName,
			last_name: lastName,
			sector: SECTOR,
			type_of_addr: 'Owned', //TODO: replace this with FE response
			resi_addr_ln1: address1, // Fix address problems
			city: city,
			state: city, //TODO: Check(Get from the api)
			pincode: pinCode,
			per_addr_ln1: address1,
			per_city: city,
			per_state: city,
			per_pincode: pinCode,
			appl_phone: number, // Need to Change
			appl_pan: panNumber,
			dob: dob,
			gender: gender,
			photo_id_type: 'pan',
			photo_id_num: panNumber,
			addr_id_type: 'pan',
			addr_id_num: panNumber,
			no_year_current_addr: '2', // TODO: Fetch these from FE response
			cibil_score_borro: CIBIL_SCORE,
			purpose_of_loan: purpose,
			borro_bank_name: bankName,
			borro_bank_acc_num: bankAcNo,
			sanction_date: start_date, // YYYY-mm-dd
			applied_amount: parseFloat(creditLimit),
			int_rate_reducing_perc: interest_rate || '0',
			int_type: INTERESET_TYPE,
			loan_tenure: TENURE,
			processing_fees_perc: processing_fee || '0',
			processing_fees_amt: '0',
			borro_bank_ifsc: ifscCode,
			partner_system_score: SYSTEM_SCORE,
		},
	];

	const config = await selectedKudosConfig(productName);
	const { loan_request_url: url, options } = config;
	const { result, error } = await handleData(url, body, options);
	if (error) {
		let err = {
			status: 'Loan request failed',
			message: error.response.data.message,
			data: error.response.data.data,
		};
		await handleLoanError(
			70,
			loanStatus,
			original,
			partnerLoanId,
			number,
			err,
			status,
			'LoanRequest'
		);
		throw new Error('Loan Request Failed');
	}

	return { result, error };
};
module.exports = loanRequest;
