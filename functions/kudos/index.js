const loanRequest = require('./loanRequest.js');
const loanBorrowerInfo = require('./loanBorrowerInfo.js');
const panKyc = require('./panKyc.js');
const loanManage = require('./loanManage.js');
const loanUsuage = require('./loanUsuage.js');
const uploadDocument = require('./uploadDocument.js');
const getVirtualAccount = require('./getVirtualAcc.js');
const paytmDisbursal = require('./paytmDisbursal.js');
module.exports = {
  loanRequest,
  loanBorrowerInfo,
  panKyc,
  loanManage,
  loanUsuage,
  uploadDocument,
  getVirtualAccount,
  paytmDisbursal,
};
