const handleData = require('../handleData').handleData;
const config = require('../config/index.config');
const handleLoanError = require('../util/handleLoanError');
const getVirtualAccount = async (
	response,
	loanStatus,
	original,
	partnerLoanId,
	number,
	status
) => {
	let { result, error } = await handleData(
		config.KUDOS.get_virtual_acc_url,
		[response],
		config.KUDOS.options
	);
	if (error) {
		let err = {
			status: 'Virtual Account creation failed',
			message: error.response.data.message,
			data: error.response.data.data,
		};
		await handleLoanError(
			70,
			loanStatus,
			original,
			partnerLoanId,
			number,
			err,
			status,
			'VirtualAccount'
		);

		throw new Error('Get Virtual Account Failed');
	}
	return { result, error };
};
module.exports = getVirtualAccount;
