const handleData = require('../handleData').handleData;
const selectedKudosConfig = require('../util/getKudosConfig');
const handleLoanError = require('../util/handleLoanError');
const uploadDocument = async (
	response,
	contents_in_base64,
	fileType,
	productName,
	loanStatus,
	original,
	partner_loan_id,
	number,
	status,
	statusMessage,
	name
) => {
	let body = {
		...response,
		fileType: fileType,
		base64pdfencodedfile: contents_in_base64,
	};
	const config = await selectedKudosConfig(productName);
	const { upload_document_url: url, options } = config;
	const { result, error } = await handleData(url, body, options);
	if (error) {
		err = {
			status: statusMessage,
			message: error.response.data.message,
		};
		await handleLoanError(
			70,
			loanStatus,
			original,
			partner_loan_id,
			number,
			err,
			status,
			name
		);
		throw new Error('Upload Document Failed');
	}

	return { result, error };
};
module.exports = uploadDocument;
