const config = require('../config/index.config');
const { getQueryInformation } = require('../util/fetchInfoFromDB');
const sendEmail = require('../util/sendEmail');
const queryHandler = async (userIdentifier) => {
  const queryData = await getQueryInformation(userIdentifier);
  const { phoneNumber, query } = queryData;
  const mailOptions = await configureQueryMailOption(
    config.QUERY_EMAIL,
    userIdentifier,
    phoneNumber,
    query
  );
  let { result, error } = await sendEmail(mailOptions);
  if (result) {
    console.log('email sent');
  }
};
const configureQueryMailOption = async (
  to,
  userIdentifier,
  phoneNumber,
  query
) => {
  const mailOptions = {
    to: to,
    message: {
      subject: `Query from ${userIdentifier} at ${config.PROJECT_ID}`, // email subject
      html: `<div>\
		<p>Hello,</p>\
		<p>user has enquired about the product</p>\
		<ul>\
		<li>Contact Number – ${phoneNumber}</li>\
		<li>Query – ${query}</li>\
		</ul>\
		<p>Thank You,<br>Team Valyu AI</p>\
		</div>`, // email content in HTML, // email content in HTML
    },
  };
  return mailOptions;
};
module.exports = queryHandler;
