const functions = require('firebase-functions');
const queryHandler = require('./handleQuery');
const queryCreationHandle = functions.database
  .ref('Query/{number}')
  .onCreate(async () => {
    console.log('Recieved a query..');
    const userIdentifier = context.params.number;
    await queryHandler(userIdentifier);
    return true;
  });

module.exports = {
  queryCreationHandle,
};
