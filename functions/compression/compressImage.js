const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const Path = require('path');
let os = require('os');

const compressedImage = async (path, filename) => {
	try {
		//let destinationpath = Path.join(os.tmpdir(), `${filename}Compressed.jpg`);
		const files = await imagemin([path], {
			destination: os.tmpdir(),
			plugins: [imageminMozjpeg({ quality: 50 })],
		});
	} catch (error) {
		console.log('CompressionError', error);
	}
};

module.exports = compressedImage;
