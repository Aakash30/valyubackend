let cron = require('cron');
let moment = require('moment');
const axios = require('axios');
const Config = require('../../../functions/config/index.config');

import dbService from '../../api/services/db.service';
moment.suppressDeprecationWarnings = true;

const handleData = async (url, data, options, usefulldata) => {
  let result = undefined;
  let error = undefined;
  try {
    result = await axios.post(url, data, options);
    result.data.imp_data = usefulldata;
    return { result, error };
  } catch (error) {
    return { result, error };
  }
};

const dateChangesCron = async () => {
  let client_data = dbService.findInDB('Client');
  let fetchdata = await client_data.once('value');
  fetchdata = fetchdata.val();

  let client_configdata = dbService.findInDB('ClientConfig');
  let data = await client_data.once('value');
  data = data.val();
  for (let key in data) {
    console.log(data[key]);
  }
};

const cronjob = async (fetch, number = undefined, workProfile) => {
  const startTime = moment().format();
  const iso = 'T00:00:00Z';
  console.log(
    'Crone Run On Every Day at 23:59 PM, CURRENT TIME IS ',
    startTime
  );
  let documents = [];
  let document = await dbService.findOne(workProfile, number, {});
  documents.push(document);
  //console.log(documents);
  let client_data = dbService.findInDB('Client/' + workProfile, {});
  let fetchdata = await client_data.once('value');
  fetchdata = fetchdata.val(); //changes

  let current_date = moment().format('YYYY-MM-DD');

  let temp = current_date.split('-');
  let previous_date = moment().subtract(1, 'days').format('YYYY-MM-DD');
  const employeeUpdated = [];

  documents.forEach(async (doc) => {
    let employeeId = doc.data().employeeId;
    let product_name = doc.data().product_name;
    let salary = doc.data().salary;

    let payrollStartDate = fetchdata[product_name].payrollStartDate;
    let payrollEndDate = fetchdata[product_name].payrollEndDate;

    employeeUpdated.push(employeeId);
    //Check user if he joined before or today
    let date_diff = moment(current_date).diff(doc.data().doj, 'days');
    let totaldays = moment(payrollEndDate).diff(payrollStartDate, 'days') + 1;

    console.log(totaldays);
    let eligibilty_percentage;
    eligibilty_percentage = await getEligibilityPercentageforProduct(
      fetchdata,
      product_name,
      salary
    );
    if (date_diff > 0) {
      if (payrollStartDate.split('-')[2] === temp[2]) {
        await dbService
          .findInDB(
            'users/' + doc.id + '/workProfile',

            {}
          )
          .child(workProfile + '/userData')
          .update({
            eligibility_amount: 0,
            available_amount: 0,
            applied_amount: 0,
            eligibility_date: current_date,
            totalSuccessfullApplication: 0,
          });
      } else {
        let phoneNumber = doc.id;
        let usefulldata = {
          salary: doc.data().salary,
          doj: doc.data().doj,
          phoneNumber: doc.id,
          per_day_salary: doc.data().salary / totaldays,
        };
        let userdata = dbService.findInDB(
          'users/' + doc.id + '/workProfile/' + workProfile + '/userData',
          {}
        );
        userdata = await userdata.once('value');

        let previous_iso_date = previous_date + iso;
        //let start_iso_date = start_date + iso;
        let start_iso_date = payrollStartDate + iso;
        let eligibility_amount = 0;
        let days = moment(previous_date).diff(moment(payrollStartDate), 'days');

        days = days + 1;
        if (workProfile.toLowerCase() == 'peoplestrong') {
          let body = {
            requestorName: 'AltAttendance',
            sysApiCode: 'EmployeeAttendance_solr',
            filterAttrs: [
              {
                objectCode: 'ALTW_ATTENDANCE',
                objectAttrCode: 'ORGANIZATIONID',
                value: '19',
                operator: '=',
              },
              {
                objectCode: 'ALTW_ATTENDANCE',
                objectAttrCode: 'EMPLOYEECODE',
                value: employeeId,
                operator: '=',
              },
              {
                objectCode: 'ALTW_ATTENDANCE',
                objectAttrCode: 'STARTDATE',
                value: start_iso_date,
                operator: '>=',
              },
              {
                objectCode: 'ALTW_ATTENDANCE',
                objectAttrCode: 'ENDDATE',
                value: previous_iso_date,
                operator: '<=',
              },
            ],
          };

          let { result, error } = await handleData(
            Config.PS_ATTENDANCE_URL,
            body,
            {},
            usefulldata
          );

          if (result) {
            let count = 0;
            //	let flag = false;
            if (result.data != undefined && result.data.data == null) {
              //	flag = true;
              eligibility_amount = (
                days * result.data.imp_data.per_day_salary
              ).toFixed(2);
            } else if (
              result.data != undefined &&
              result.data.data.length != 0
            ) {
              let data = JSON.parse(result.data.data);
              for (let i = 0; i < data.data.length; i++) {
                if (data.data[i].ACTUAL_STATUS === 'Absent') {
                  count++;
                }
              }
              //flag = true;
              eligibility_amount = (
                days * result.data.imp_data.per_day_salary -
                count * result.data.imp_data.per_day_salary
              ).toFixed(2);
            } else if (
              result.data != undefined &&
              result.data.data.length == 0
            ) {
              eligibility_amount = (
                days * result.data.imp_data.per_day_salary
              ).toFixed(2);
            }
          }
          if (error) {
            console.log('Some Error Occured', error);
          }
        } else {
          eligibility_amount = (days * usefulldata.per_day_salary).toFixed(2);
          console.log(eligibility_amount);
        }

        eligibility_amount = (
          (eligibility_amount * eligibilty_percentage) /
          100
        ).toFixed(2);

        if (eligibility_amount > fetchdata[product_name].maximum_loan_amount) {
          eligibility_amount = fetchdata[product_name].maximum_loan_amount;
        }

        await dbService
          .findInDB(
            'users/' + phoneNumber + '/workProfile',

            {}
          )
          .child(workProfile + '/userData')
          .update({
            eligibility_amount: parseFloat(eligibility_amount),
            eligibility_date: previous_date,
            totalSuccessfullApplication: 0,
            applied_amount: 0,
            available_amount: parseFloat(eligibility_amount),
          });
      }
    } else {
      console.log('Employeee Not Eligible For Loan Yet');
    }
  });

  await dbService.insertIntoDatabase('cronStats/', new Date().getTime(), {
    startTime,
    endTime: moment().format(),
    employeeUpdated,
  });
  //console.log(collection);
};

const getEligibilityPercentageforProduct = async (
  fetchdata,
  product_name,
  salary
) => {
  let eligibilty_percentage;
  if (product_name == 'advanceLoan_subscription') {
    let rules = fetchdata[product_name].rules;
    let salary_max = false;
    for (let rules_salary in rules) {
      if (rules_salary >= salary) {
        eligibilty_percentage = rules[rules_salary].eligibilty_percentage;
        salary_max = true;
        break;
      }
    }

    if (!salary_max) {
      eligibilty_percentage = rules['max'].eligibilty_percentage;
    }
  } else {
    eligibilty_percentage = fetchdata[product_name].eligibilty_percentage;
  }

  return eligibilty_percentage;
};

module.exports = {
  //Crone Run On Every Day at 23:59 PM
  croneRunAtTheEndOfDay: (run = '* * * * *') => {
    let job = new cron.CronJob(
      run,
      () => {
        cronjob('all');
      },
      null,
      true,
      'Asia/Kolkata'
    );
  },
  cronjob,
  dateChangesCron,
};
