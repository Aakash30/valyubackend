//let models = require('../../model');
let { getInstance } = require('../../common/db');
const db = getInstance();

class Database {
  constructor() {}

  insertIntoDatabase(ref, child, data) {
    return db
      .database()
      .ref(ref)
      .child(child + '')
      .set(data);
  }

  findInDB(ref, options) {
    //return models[collection].findOne({ ...condition }, options);
    console.log(ref);
    return db.database().ref(ref);
  }

  findAll(collection, options) {
    //	return models[collection].find({}, options);
    console.log(collection);
    return db.firestore().collection(collection).get();
  }

  find(collection, fieldname, condition, value, options) {
    //return models[collection].findOne({ ...condition }, options);
    return db
      .firestore()
      .collection(collection)
      .where(fieldname, condition, value)
      .get();
  }

  findOne(collection, id, options) {
    //return models[collection].findOne({ ...condition }, options);
    return db.firestore().collection(collection).doc(id).get();
  }
  getAllCollection() {
    let collection = [];
    return db
      .firestore()
      .listCollections()
      .then((snapshot) => {
        //	console.log(snapshot);
        snapshot.forEach((snaps) => {
          collection.push(snaps['_queryOptions'].collectionId);
        });
        return collection;
      });
  }

  create(collection, key, data, options) {
    try {
      //		return models[collection].create({ ...data }, options);

      return db.firestore().collection(collection).doc(key).set(data);
    } catch (err) {
      console.log(err);
      return err;
    }
  }

  updateOne(collection, id, data, options) {
    /*  return models[collection].updateOne(
			{ ...condition },
			{ $set: { ...data } },
			options
    );
    */
    console.log(collection, id);
    return db.firestore().collection(collection).doc(id).update(data);
  }

  update(collection, condition, data, options) {
    /*  return models[collection].updateOne(
			{ ...condition },
			{ $set: { ...data } },
			options
    );
    */
    return db.collection(collection).where(condition).update({ data });
  }
}

export default new Database();
