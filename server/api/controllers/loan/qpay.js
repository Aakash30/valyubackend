const {
	sendStatusToQuess,
} = require('./../../../../functions/loans/sendLoanStatus');

const quessConstant = {
	start: 'START',
	success: 'SUCCESS',
	failed: 'FAILED',
};
const asyncForEach = require('../../../../functions/util/asyncForEach')
	.asyncForEach;

const updateLoanInDB = require('./../../../../functions/util/updateLoanInDB');
export const create = async (req, res, next) => {
	const { loanId } = req.body;
	if (!loanId) {
		res.status(400).send({ message: 'Please provide loanID' });
	}
	const userIdentifier = loanId.split(/[_ ]+/).pop();
	let intimationToQuess = quessConstant['start'];
	try {
		const newSyncStatus = await sendStatusToQuess(loanId, intimationToQuess);
		intimationToQuess = quessConstant['success'];
		await updateLoanInDB(
			{ syncStatus: newSyncStatus, intimationToQuess: intimationToQuess },
			loanId,
			userIdentifier
		);

		res.status(200).send({ message: 'QPAY_API RUNS PERFECTLY' });
	} catch (error) {
		if (error.code == '200' || error.code === '201') {
			res.status(400).send({ message: error.message });
		} else if (error.code == '202') {
			res.status(400).send({ message: error.message });
		} else {
			next(error);
		}
	}
};

export const sendToQuess = async (req, res, next) => {
	try {
		let { loanId } = req.body;
		if (loanId.length == 0 || typeof loanId == 'string') {
			res.status(400).send({ message: 'Enter valid loanID in Array Format ' });
		} else {
			await sendData(loanId);
			res.send({ message: 'Send Data To Quess is done' });
		}
	} catch (error) {
		next(error);
	}
};
const sendData = async (loanId) => {
	console.log('Send To Quess');
	let response = {};
	let intimationToQuess, userIdentifier, getLoanId;
	let message = '';
	await asyncForEach(loanId, async (id) => {
		try {
			userIdentifier = id.split(/[_ ]+/).pop();
			getLoanId = id;
			intimationToQuess = quessConstant['start'];
			const newSyncStatus = await sendStatusToQuess(
				getLoanId,
				intimationToQuess
			);
			intimationToQuess = quessConstant['success'];
			response[id] = {
				intimationToQuess: 'success',
			};
			await updateLoanInDB(
				{ syncStatus: newSyncStatus, intimationToQuess: intimationToQuess },
				getLoanId,
				userIdentifier
			);
		} catch (error) {
			if (error.code == '200' || error.code === '201') {
				console.log(error);
				intimationToQuess = quessConstant['failed'];
			} else if (error.code == '202') {
				console.log(error);
				message = 'Status is not disbursed';
			} else if (error.code == '203') {
				console.log(error);
				message = 'LoanId does not exist';
			} else {
				console.log(error);
			}
		}
		response[id] = {
			intimationToQuess: intimationToQuess,
			message: message,
		};
	});
	return response;
};
