import { asyncForEach } from '../../../../functions/util/asyncForEach';
import {
	getLoanInformation,
	getUserInformation,
} from '../../../../functions/util/fetchInfoFromDB';
const clRepay = require('./../../../../functions/kudos/clRepay');

const callKudosRepaymentApi = async (repaymentReqData) => {
	let resultData = {};
	await asyncForEach(repaymentReqData, async (repaymentData) => {
		let { loanId, txn_amount, txn_date, txn_id } = repaymentData;
		let { loanData, userIdentifier } = await getLoanInformation(loanId);
		let userData = await getUserInformation(userIdentifier);
		let { isRepaymentApiCompleted, status, isLoanUsageCompleted } = loanData;
		if (loanData == undefined) {
			console.log(`Loan Id does not exist`);
			resultData[loanId] = {
				isSuccess: false,
				result: 'LoanId does not exist',
			};
		} else if (status != 50 || !isLoanUsageCompleted) {
			console.log(
				`CL RepaymentApi for ${loanId} for user ${userIdentifier} cannot be called`
			);
			resultData[loanId] = {
				isSuccess: false,
				result:
					'Either LoanUsuage for Loan is not called or status is not in disbursement',
			};
		} else if (isRepaymentApiCompleted) {
			console.log(
				`CL RepaymentApi for ${loanId} for user ${userIdentifier} is already called for the loanId `
			);
			resultData[loanId] = {
				isSuccess: false,
				result: 'Repayment Api already called',
			};
		} else {
			let { isSuccess, error, result } = await clRepay(
				txn_amount,
				txn_date,
				txn_id,
				loanData,
				userIdentifier,
				userData
			);

			resultData[loanId] = {
				isSuccess: isSuccess,
				error,
			};
		}
	});

	return resultData;
};

module.exports = callKudosRepaymentApi;
