import dbService from '../../services/db.service';
const pushRepaymentData = async (req, res, next) => {
  try {
    const timestamp = new Date().getTime();
    await dbService.insertIntoDatabase('quessRepaymentData', timestamp, {
      ...req.body,
      created_at: new Date().toISOString(),
    });
    res.send({ message: 'Data Saved' });
  } catch (error) {
    //next(error);

    res.status(500).send({ message: 'Some Error Occured' });
  }
};
const pushNonRepaymentData = async (req, res, next) => {
  try {
    const timestamp = new Date().getTime();
    await dbService.insertIntoDatabase('quessNonRepaymentData', timestamp, {
      ...req.body,
      created_at: new Date().toISOString(),
    });
    res.send({ message: 'Data Saved' });
  } catch (error) {
    //next(error);

    res.status(500).send({ message: 'Some Error Occured' });
  }
};

export { pushRepaymentData, pushNonRepaymentData };
