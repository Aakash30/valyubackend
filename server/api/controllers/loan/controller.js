import BaseControllerClass from '../../../common/baseApiController';
import dbService from '../../services/db.service';
const retryLoanProcess = require('../../../../functions/loans/retryLoanProcess');
const disbursementAutomation = require('../../../../functions/loans/disbursementAutomation');
import { asyncForEach } from '../../../../functions/util/asyncForEach';
const callKudosRepaymentApi = require('./callKudosRepaymentApi');

export class LoanController extends BaseControllerClass {
	constructor() {
		super('Loan');
	}
	async create(req, res, next) {
		try {
			console.log('POST CREATE LOAN');
			const number = req.body.number;
			const loan_request_id = Math.floor(Math.random() * 899999 + 100000);
			const timestamp = new Date().getTime();
			const loanData = await dbService.insertIntoDatabase(
				'users' + '/' + number + '/' + this.modelName,
				timestamp + '_' + number,
				{
					...req.body,
					status: 10,
					request: loan_request_id,
					details: 'User Applied for loan of ' + req.body.amount,
					created_at: new Date().toISOString(),
					update_at: new Date().toISOString(),
				}
			);

			let find_loandata = dbService.findInDB(
				'users' +
					'/' +
					number +
					'/' +
					this.modelName +
					'/' +
					timestamp +
					'_' +
					number
			);
			find_loandata = await find_loandata.once('value');

			res.json(find_loandata.val());
		} catch (err) {
			next(err);
		}
		console.log('POST CREATE LOAN ENDS');
	}
	async statusUpdate(req, res, next) {
		try {
			const timestamp = new Date().getTime();
			const loanData = await dbService.insertIntoDatabase(
				'statusUpdate',
				timestamp,
				{
					...req.body,
					created_at: new Date().toISOString(),
					update_at: new Date().toISOString(),
				}
			);

			res.status(200).send({ message: 'Updated Successfully' });
		} catch (err) {
			next(err);
		}
		console.log('POST CREATE LOAN ENDS');
	}
	async getloanHistoryByUser(req, res, next) {
		try {
			const number = req.params.number;

			let data = dbService.findInDB(
				'users' + '/' + number + '/' + 'Loan',

				{}
			);

			data = await data.once('value');

			if (!data.val()) {
				return res.status(404).send({ message: 'No Data Present' });
			}
			res.json(data.val());
		} catch (error) {
			next(error);
		}
	}
	async getloanByID(req, res, next) {
		try {
			const loan_request_id = req.params.loan_request_id;

			const number = req.params.number;
			if (!loan_request_id) {
				return res
					.status(404)
					.send({ message: 'Please provide Loan Request ID' });
			}
			let data;

			data = await dbService
				.findInDB(
					'users' + '/' + number + '/' + 'Loan/',

					{}
				)
				.orderByChild('request')
				.equalTo(parseInt(loan_request_id))
				.once('value');

			if (!data.val()) {
				return res.status(404).send({ message: 'No Data Present' });
			}
			res.json(data.val());
		} catch (error) {
			next(error);
		}
	}

	async update(req, res, next) {
		console.log('UPDATE LOAN Start..');
		const result = {};
		try {
			await asyncForEach(req.body.loan_request_ids, async (loanId) => {
				console.log('Processing LoanId ', loanId);
				result[loanId] = 'processing';
				const number = loanId.split(/[_ ]+/).pop();

				// Fetch Loan Data

				let loanData = await dbService
					.findInDB('users' + '/' + number + '/' + 'Loan/' + loanId, {})
					.once('value');
				loanData = loanData.val();
				if (!loanData) {
					result[loanId] = 'Loan not Found';
				} else if (loanData.status !== 80) {
					result[loanId] = 'Loan Status should be 80 for manual disbursement..';
				} else {
					// Find employer name from loan
					const employerName = loanData.employerName;

					// get user Data
					let userData = await dbService
						.findInDB('users' + '/' + number, {})
						.once('value');
					userData = userData.val();

					// get EmployerData
					let employerData = await dbService
						.findInDB('Client/' + employerName, {})
						.once('value');

					employerData = employerData.val();
					//Data to send in error

					const { isSuccess } = await disbursementAutomation(
						loanData,
						number,
						userData,
						employerData
					);

					result[loanId] = isSuccess ? 'Completed' : 'Error';
				}
				console.log('Completed LoanId ', loanId);
			});
			console.log('UPDATE LOAN Done..');
			res.status(200).send(result);
		} catch (err) {
			res.status(500).send(result);
		}
	}
	async retryLoans(req, res, next) {
		try {
			let { loanIds } = req.body;
			if (!Array.isArray(loanIds)) {
				return res
					.status(400)
					.send({ message: 'Please Enter the LoanIds in Array Format' });
			}
			await asyncForEach(loanIds, async (loanId) => {
				await retryLoanProcess(loanId);
				/*
				await rdbInstance.ref('retryLoans/' + loanId).update({
					startProcess: 'start',
				  });
				  */
			});
			return res
				.status(200)
				.send({ message: 'RetryLoans are called for the provided LoanIds' });
		} catch (error) {
			next(error);
		}
	}
	async callKudosRepaymentApi(req, res, next) {
		try {
			let repaymentReqData = req.body;
			if (!Array.isArray(repaymentReqData)) {
				res.status(400).send({ message: 'Enter valid Data in Array Format ' });
				return;
			}
			const result = await callKudosRepaymentApi(repaymentReqData);
			res.send({
				message: 'Repayment Kudos Api is done',
				resultData: result,
			});
		} catch (error) {
			console.log(error);
			res.status(500).send({});
		}
	}
}

export default new LoanController();
