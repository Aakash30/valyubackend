'use strict';

import { google } from 'googleapis';
import config from '../../../../functions/config/index.config';
import dbService from '../../services/db.service';
const moment = require('moment-timezone');

const {
	getClientInformation,
	getStatusConstantInformation,
	getErrorInformation,
} = require('../../../../functions/util/fetchInfoFromDB');
const productNames = require('../../../../functions/constant/productNames.constant');
const asyncForEach = require('../../../../functions/util/asyncForEach')
	.asyncForEach;

const authorize = () => {
	console.info('authorize: Entering');
	const oAuth2Client = new google.auth.OAuth2(
		config.OAUTH2.CLIENT_ID,
		config.OAUTH2.CLIENT_SECRET
	);

	oAuth2Client.setCredentials({
		access_token: config.OAUTH2.ACCESS_TOKEN,
		refresh_token: config.OAUTH2.REFRESH_TOKEN,
		scope: 'https://www.googleapis.com/auth/spreadsheets',
		token_type: 'Bearer',
		expiry_date: 1582488053429, // new Date().getTime() + 1000 * 60 * 30,
	});
	return oAuth2Client;
};

const updateSheetData = async (range, data, auth, sheetName, sheetID) => {
	console.info('updateSheetData: Entering');
	try {
		const sheets = google.sheets({ version: 'v4', auth });
		let responseData = await sheets.spreadsheets.values.update({
			spreadsheetId: sheetID,
			range: range,
			valueInputOption: 'RAW',
			resource: {
				values: data,
			},
		});
		return JSON.parse(JSON.stringify(responseData.data, null, 2));
	} catch ({ message }) {
		console.error('Failed to update sheet:', message);
		throw new Error(message);
	}
};

export const updateGSheetData = async (req, res) => {
	try {
		let sheetID = req.query.sheetId;
		let sheetName = req.query.sheetName;
		let range = `${sheetName}!A2`;

		const usersData = await dbService.findInDB('users', {}).once('value');
		let tableData = await getLoanData(usersData.val());
		//await processData(userData.val());
		console.info(
			'POST: updateGSheetData: Entering: update Google Data of',
			'with',
			range,
			'for'
		);
		const auth = authorize();
		const updatedSheetData = await updateSheetData(
			range,
			tableData,
			auth,
			sheetName,
			sheetID
		);
		res.send({ message: 'operation done' });

		//	return response.successHandler(statusCode.OK, resCode.OK, updatedSheetData);
	} catch ({ message }) {
		console.log(message);
		res.status(400).send({ message: message });
		console.error(`POST:updateGSheetData:error: ${message}`);
		//	return boom.badImplementation(message);
	}
};

const processData = async (userData) => {
	const users = Object.keys(userData);
	const userArray = await chunk(users, 10);
	for (let i = 0; i < userArray.length; i++) {
		console.log(userArray[i]);
	}
};
const chunk = async (arr, size) => {
	const userArray = Array.from(
		{ length: Math.ceil(arr.length / size) },
		(v, i) => arr.slice(i * size, i * size + size)
	);
	return userArray;
};
const getRowDataForLoan = async (loanKeys, userData, userIdentifier) => {
	let data = [];
	for (let j = 0; j < loanKeys.length; j++) {
		let loanData = userData[userIdentifier].Loan[loanKeys[j]];
		let {
			employerName,
			interestRate,
			disbursementDate,
			amount,
			processing_fees,
			platform_fees,
			total_gst_on_platform_fee,
			total_gst_on_processing_amount,
			loan_tenure_in_days,
		} = loanData;
		let gst = total_gst_on_platform_fee + total_gst_on_processing_amount;
		let {
			kudos_loan_id,
			kudos_borrower_id,
			employeeId,
			product_name,
			salary,
			available_amount,
			eligibility_amount,
		} = userData[userIdentifier].workProfile[employerName].userData;
		let employerData = await getClientInformation(employerName);

		let { clientName } = employerData;
		let { payDate, settlementDate } = employerData[product_name];
		let product;
		let subscriptionFee;
		if (clientName.toLowerCase() == 'peoplestrong') {
			product = 'AS-PPU';
			subscriptionFee = 0.0;
		} else {
			product = 'AS-SUB';
			subscriptionFee = processing_fees;
		}

		let { bankName, ifscCode, bankAcNo } = userData[userIdentifier].workProfile[
			employerName
		].account;
		let createData = [
			disbursementDate,
			null,
			clientName,
			'Kudos',
			product,
			kudos_loan_id,
			kudos_borrower_id,
			null,
			user_id,
			loanKeys[j],
			fullName,
			employeeId,
			userIdentifier,
			bankName,
			ifscCode,
			bankAcNo,
			payDate,
			interestRate,
			disbursementDate,
			settlementDate,
			salary,
			null,
			available_amount,
			eligibility_amount,
			null,
			amount,
			subscriptionFee,
			platform_fees,
			gst,
			null,
			null,
			loan_tenure_in_days,
			null,
			null,
		];
		data.push(createData);
	}
};

const getLoanData = async (usersData) => {
	let data = [];
	const users = Object.keys(usersData);
	let columnData = await getColumnNames();
	data.push(columnData);
	await asyncForEach(users, async (userIdentifier) => {
		let { userProfile, Loan, workProfile } = usersData[userIdentifier];
		if (!userProfile) return;
		let { user_id, firstName, lastName } = userProfile;
		let fullName = firstName + ' ' + lastName;
		if (!Loan) return;
		let loanIds = Object.keys(Loan);

		await asyncForEach(loanIds, async (loanId) => {
			console.log(loanId);
			let loanData = Loan[loanId];
			let {
				employerName,
				interestRate,
				disbursementDate,
				amount,
				repaymentAmount,
				processing_fees,
				platform_fees,
				total_gst_on_platform_fee,
				total_gst_on_processing_amount,
				loan_tenure_in_days,
				status,
				SI,
				disbursement_Date,
				created_at,
				processingFees,
			} = loanData;

			let applicationDate = disbursementDate
				? disbursementDate
				: disbursement_Date
				? disbursement_Date
				: moment(created_at).tz('Asia/Kolkata').format('MM/DD/YYYY');

			let employerData = await getClientInformation(employerName);
			let gst = total_gst_on_platform_fee + total_gst_on_processing_amount;
			let { userData, account } = workProfile[employerName];
			let {
				kudos_loan_id,
				kudos_borrower_id,
				partner_loan_id,
				employeeId,
				product_name,
				salary,
				available_amount,
				eligibility_amount,
				totalSuccessfullApplication,
			} = userData;
			let { clientName } = employerData;
			let { payDate, settlementDate } = employerData[product_name];
			let product, applicationStatus;
			let subscriptionFee;
			if (product_name == productNames.ADVANCE_LOAN_PAY_PER_USE) {
				product = 'AS-PPU';
				subscriptionFee = 0.0;
			} else {
				product = 'AS-SUB';
				subscriptionFee = processing_fees;
			}
			if (status) {
				let statusData = await getStatusConstantInformation(status);
				applicationStatus = statusData.status;
			}
			let { bankName, ifscCode, bankAcNo } = account;
			let loanAmountValyu = repaymentAmount
				? (repaymentAmount - amount).toFixed(2)
				: undefined;

			SI = SI ? (SI = SI.toFixed(2)) : undefined;

			let errorData = await getErrorInformation(loanId);

			let error = errorData
				? errorData.error
					? errorData.error.message + ',' + JSON.stringify(errorData.error.data)
					: 'No error found'
				: 'No error';

			let tableData = [
				applicationDate,
				applicationStatus,
				error,
				clientName,
				'Kudos',
				product,
				kudos_loan_id,
				kudos_borrower_id,
				partner_loan_id,
				user_id,
				loanId,
				fullName,
				employeeId,
				userIdentifier,
				bankName,
				ifscCode,
				bankAcNo,
				payDate,
				interestRate,
				disbursementDate || disbursement_Date,
				settlementDate,
				salary,
				available_amount,
				available_amount,
				eligibility_amount,
				totalSuccessfullApplication,
				amount,
				subscriptionFee,
				processingFees,
				platform_fees,
				gst,
				loanAmountValyu,
				amount,
				loan_tenure_in_days,
				SI,
				repaymentAmount,
			];
			//console.log(tableData);
			data.push(tableData);
		});
	});
	return data;
};

const getColumnNames = async () => {
	let columnData = [
		'Application Date',
		'Application Status',
		'Issue Details',
		'Client',
		'Lender',
		'Product',
		'Lender Loan ID',
		'Lender Borrower ID',
		'kudos_partner_id',
		'Valyu Borrower ID',
		'Valyu Application ID',
		'Borrower Name',
		'Emp ID',
		'Mobile Number',
		'Bank Name',
		'IFSC Code',
		'Bank Account Number',
		'Next Salary Payout Date',
		'Interest Rate',
		'Planned Disbursement Date',
		'Planned Settlement Date',
		'Monthly Salary',
		'Accrued Salary',
		'Availability',
		'Eligibility',
		'Monthly Transaction Count',
		'Requested Amount',
		'Subscription Fee',
		'Processing Fee',
		'Platform Fee',
		'Total GST',
		'Loan Amount (Valyu)',
		'Loan Amount (Lender)',
		'Tenure (In Days)',
		'Borrower Interest Amount',
		'Borrower Settlement Amount',
	];

	return columnData;
};
