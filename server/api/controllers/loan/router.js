import * as express from 'express';
import controller from './controller';
import { updateGSheetData } from './gsheet';
import { create, sendToQuess } from './qpay';
import { pushRepaymentData, pushNonRepaymentData } from './loanRepayment';

export default express
	.Router()
	.post('/', controller.create)
	.get('/export', updateGSheetData)
	.post('/statusUpdate', controller.statusUpdate)
	.post('/retryLoans', controller.retryLoans)
	.post('/kudosRepaymentApi', controller.callKudosRepaymentApi)
	.post('/sendToQuess', sendToQuess)
	.post('/quesscorp', create)
	.post('/repaymentData', pushRepaymentData)
	.post('/nonRepaymentData', pushNonRepaymentData)
	.post('/:number', controller.update)
	.get('/:number', controller.getloanHistoryByUser)
	.get('/:number/:loan_request_id', controller.getloanByID);
