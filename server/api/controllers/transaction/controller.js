import createTransaction from '../../../../functions/util/createTransaction';
import BaseControllerClass from '../../../common/baseApiController';
import dbService from '../../services/db.service';
const { asyncForEach } = require('../../../../functions/util/asyncForEach');

export class TransactionController extends BaseControllerClass {
	constructor() {
		super('Transaction');
	}

	async create(req, res, next) {
		try {
			const number = req.body.mobileNumber;
			if (!number) {
				res.status(404).send({ message: 'Please provide the Phone number' });
			}
			delete req.body.mobileNumber;
			const service_id = Math.floor(Math.random() * 899999 + 100000);
			let key = new Date().getTime() + '_' + number;
			req.body.key = key;
			let document_id;
			if (req.body.partner_loan_id) {
				document_id = req.body.partner_loan_id;
			} else {
				document_id = key + '_' + number;
			}
			let insertdata = await dbService.insertIntoDatabase(
				'users/' + number + '/',
				this.modelName + '/' + document_id,
				{
					...req.body,
					service_id: service_id,
					created_at: new Date().toISOString(),
					update_at: new Date().toISOString(),
				}
			);

			insertdata = await dbService.insertIntoDatabase(
				'TransactionHistory',
				document_id,
				{
					...req.body,
					created_at: new Date().toISOString(),
					update_at: new Date().toISOString(),
				}
			);

			res.status(200).send({ message: 'Transaction Sucessfull' });
		} catch (err) {
			console.log(err);
			next(err);
		}
	}
	async all(req, res, next) {
		try {
			const number = req.params.number;
			let data = dbService.findInDB(
				'users' + '/' + number + '/' + 'Transaction',

				{}
			);

			data = await data.once('value');

			if (!data.val()) {
				return res.status(404).send({ message: 'No Data Present' });
			}
			res.json(data.val());
		} catch (err) {
			next(err);
		}
	}
	async byId(req, res, next) {
		try {
			const service_id = req.params.service_id;
			console.log(service_id);
			const number = req.params.number;
			if (!service_id) {
				return res
					.status(404)
					.send({ message: 'Please provide Loan Request ID' });
			}
			let data;

			data = await dbService
				.findInDB(
					'users' + '/' + number + '/' + 'Transaction',

					{}
				)
				.orderByChild('service_id')
				.equalTo(parseInt(service_id))
				.once('value');

			console.log(data.val());

			if (!data.val()) {
				return res.status(404).send({ message: 'No Data Present' });
			}
			res.json(data.val());
		} catch (error) {
			console.log(error);
			next(error);
		}
	}
	async createTransaction(req, res, next) {
		let responseData = {};
		try {
			console.log('Creating Transaction');
			let userInformation = req.body;
			if (!Array.isArray(userInformation)) {
				return res
					.status(404)
					.send({ message: 'Please provide the data in Array format' });
			} else {
				await asyncForEach(userInformation, async (userData) => {
					let { userIdentifier, clientName } = userData;
					responseData[userIdentifier] = await createTransaction(
						userIdentifier,
						clientName
					);
				});
				res.status(200).send({ ...responseData });
			}
		} catch (error) {
			res.status(404).send({ responseData });
		}
	}
}
export default new TransactionController();
