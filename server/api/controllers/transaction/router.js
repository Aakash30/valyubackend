import * as express from 'express';
import controller from './controller';

export default express
	.Router()
	.post('/', controller.create)
	.post('/createTransaction', controller.createTransaction)
	.get('/:number', controller.all)
	.get('/:number/:service_id', controller.byId);
