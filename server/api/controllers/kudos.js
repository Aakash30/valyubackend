const admin = require('firebase-admin');
let fs = require('fs');

const axios = require('axios');
//const handledata = require('../../../functions/index').handledata;
//admin.initializeApp();

let { getInstance } = require('../../common/db');
const db = getInstance();
let PDFDocument = require('pdfkit');
const moment = require('moment-timezone');

const kudos = require('../../../functions/kudos');

const getImage = async (path, filename) => {
  path = path.slice(1);
  console.log(path);
  let file = undefined;

  try {
    file = await admin
      .storage()
      .bucket('gs://dev-valyu.appspot.com')
      .file(path)
      .download({
        destination: process.cwd() + `/${filename}.jpg`,
      });

    return file;
  } catch (error) {
    console.log(error);
    return file;
  }
};

const convertintopdf = async (file1, file2) => {
  try {
    let doc = new PDFDocument();
    doc.pipe(fs.createWriteStream('output.pdf'));
    doc.image(process.cwd() + '/aadharfront.jpg', {
      fit: [500, 400],
      align: 'center',
      valign: 'center',
    });

    doc.addPage().image(process.cwd() + '/aadharback.jpg', {
      fit: [500, 400],
      align: 'center',
      valign: 'center',
    });

    doc.end();
  } catch (erro) {
    console.log(erro);
  }
};
const getDetails = async (fetchdata, product_name, salary) => {
  let eligibilty_percentage;
  let blackoutStartDate, blackoutEndDate;
  let maxAllowedTransaction = undefined;

  if (product_name == 'advanceLoan_subscription') {
    let rules = fetchdata[product_name].rules;
    let salary_max = false;
    for (let rules_salary in rules) {
      // TODO: make utility
      if (rules_salary >= salary) {
        eligibilty_percentage = rules[rules_salary].eligibilty_percentage;
        blackoutStartDate = rules[rules_salary].blackoutStartDate;
        blackoutEndDate = rules[rules_salary].blackoutEndDate;
        maxAllowedTransaction = rules[rules_salary].maxAllowedTransaction;
        salary_max = true;
        break;
      }
    }
    if (!salary_max) {
      eligibilty_percentage = rules['max'].eligibilty_percentage;
      blackoutStartDate = rules['max'].blackoutStartDate;
      blackoutEndDate = rules['max'].blackoutEndDate;
      maxAllowedTransaction = rules['max'].maxAllowedTransaction;
    }
  } else {
    eligibilty_percentage = fetchdata[product_name].eligibilty_percentage;
    blackoutStartDate = fetchdata[product_name].blackoutStartDate;
    blackoutEndDate = fetchdata[product_name].blackoutEndDate;
  }

  return {
    blackoutStartDate,
    blackoutEndDate,
    eligibilty_percentage,
    maxAllowedTransaction,
  };
};

const calculateAmount = async (employer_data, product_name, amount, salary) => {
  let processing_amount = 0;
  let cgst_on_processing_amount = 0;
  let sgst_on_processing_amount = 0;
  let cgst_on_platform_fee = 0;
  let sgst_on_platform_fee = 0;

  let fee = 0;

  if (product_name == 'advanceLoan_subscription') {
    //TODO: move to constant
    let salary_max = false;
    let rules = employer_data[product_name].rules;
    for (let rules_salary in rules) {
      if (rules_salary >= salary) {
        fee = rules[rules_salary].fee;
        salary_max = true;
        break;
      }
    }
    if (!salary_max) {
      fee = rules['max'].fee; // TODO: move to constant
    }
    // processing_amount = parseFloat(
    // 	(employer_data[product_name].fee * amount) / (100).toFixed(2)
    // );
    processing_amount = fee;
    cgst_on_processing_amount = Math.round(
      (fee * employer_data[product_name].cgst) / 100
    );
    sgst_on_processing_amount = Math.round(
      (fee * employer_data[product_name].sgst) / 100
    );
  } else {
    processing_amount = parseFloat(
      (employer_data[product_name].processing_fee * amount) / (100).toFixed(2)
    );
    cgst_on_processing_amount = Math.round(
      (processing_amount * employer_data[product_name].cgst) / 100
    );
    sgst_on_processing_amount = Math.round(
      (processing_amount * employer_data[product_name].sgst) / 100
    );
    cgst_on_platform_fee = Math.round(
      (employer_data[product_name].platform_fees *
        employer_data[product_name].cgst_on_platform_fee) /
        100
    );
    sgst_on_platform_fee = Math.round(
      (employer_data[product_name].platform_fees *
        employer_data[product_name].sgst_on_platform_fee) /
        100
    );
  }
  amount = {
    processing_amount,
    cgst_on_processing_amount,
    sgst_on_processing_amount,
    cgst_on_platform_fee,
    sgst_on_platform_fee,
    fee,
  };
  return amount;
};

const KudosFinal = async (req, res, next) => {
  console.log('Cloud Function Create Loan Started');
  const original = req.body;
  console.log(original);
  const number = original.number;
  let partner_loan_id = original.loan_id;

  let start_date = moment().tz('Asia/Kolkata').format('YYYY-MM-DD');
  console.log(moment().tz('Asia/Kolkata').format('ha z'));

  //Get user Data
  let user_data = db.database().ref('users/' + number + '/');
  user_data = await user_data.once('value');
  user_data = user_data.val();

  let product_name =
    user_data.workProfile[original.employerName].userData.product_name;
  let salary = user_data.workProfile[original.employerName].userData.salary;
  let loanSuccessfullApplication =
    user_data.workProfile[original.employerName].userData
      .totalSuccessfullApplication;

  //Get client info
  let employer_data = db.database().ref('Client/' + original.employerName);
  employer_data = await employer_data.once('value');
  employer_data = employer_data.val();

  //Get blackout Dates
  let {
    blackoutStartDate,
    blackoutEndDate,
    maxAllowedTransaction,
  } = await getDetails(employer_data, product_name, salary);
  let first_emi_date = employer_data[product_name].settlementDate;

  let sum = 0;
  //Calculate Loan applied amount
  if (user_data.workProfile[original.employerName].userData.applied_amount)
    sum =
      user_data.workProfile[original.employerName].userData.applied_amount +
      original.amount;
  else sum = original.amount;

  //Send Notifcation and Save Notification to RD
  //original.maxAllowedTransaction = maxAllowedTransaction;

  let title = employer_data.createLoanNotification.title;
  let body_data = employer_data.createLoanNotification.body;

  const payload = {
    notification: {
      title: title,
      body: body_data,
    },
  };
  /*
	SendNotificationsAndSaveData(
		number,
		user_data.userProfile.device_token,
		payload,
		title,
		body_data
	);
	*/

  if (!partner_loan_id.includes('_')) {
    // TODO: investigate and remove
    partner_loan_id = partner_loan_id + '_' + number;
  }

  let disbursement_date = moment().tz('Asia/Kolkata').format('DD/MM/YYYY'); //Current Date

  let check_cut_of_criteria = moment(
    moment(original.created_at).format('YYYY-MM-DD')
  ).isBetween(moment(blackoutStartDate), moment(blackoutEndDate), null, '[]');

  let date_diff = moment(moment(first_emi_date)).diff(
    moment(start_date),
    'days'
  );
  let amount_data = await calculateAmount(
    employer_data,
    product_name,
    original.amount,
    salary
  );
  let processing_amount = amount_data.processing_amount;
  let cgst_on_processing_amount = amount_data.cgst_on_processing_amount;
  let sgst_on_processing_amount = amount_data.sgst_on_processing_amount;
  let cgst_on_platform_fee = amount_data.cgst_on_platform_fee;
  let sgst_on_platform_fee = amount_data.sgst_on_platform_fee;
  let total_gst_on_processing_amount =
    cgst_on_processing_amount + sgst_on_processing_amount;

  let total_gst_on_platform_fee = cgst_on_platform_fee + sgst_on_platform_fee;
  let total_loan_applied;
  let SI = 0.0;
  if (product_name == 'advanceLoan_subscription') {
    if (loanSuccessfullApplication == 0) {
      total_loan_applied =
        original.amount + processing_amount + total_gst_on_processing_amount;
    } else {
      total_loan_applied = original.amount;
      processing_amount = 0;
      cgst_on_processing_amount = 0;
      sgst_on_processing_amount = 0;
      total_gst_on_processing_amount = 0;
    }
  } else {
    total_loan_applied =
      original.amount +
      processing_amount +
      total_gst_on_processing_amount +
      employer_data[product_name].platform_fees +
      total_gst_on_platform_fee;
    SI = parseFloat(
      (total_loan_applied *
        employer_data[product_name].interest_rate *
        date_diff) /
        (100).toFixed(2)
    );
  }
  //loan_amount 5130
  //disbursement 5000
  //repayment_amount 5130

  let repayment_amount = total_loan_applied + parseFloat(SI);

  if (employer_data[product_name].NBFC_CLIENT == 'SONIC') {
    let { result, error } = await sonic.pushLoan(
      original.employerName,
      original.amount,
      employer_data,
      total_loan_applied,
      user_data,
      disbursement_date,
      first_emi_date,
      processing_amount,
      total_gst_on_processing_amount,
      total_gst_on_platform_fee,
      date_diff,
      number,
      partner_loan_id,
      product_name
    );
    if (error) {
      console.log(error);
      //	loan_status.push(REQUEST['19']);
    }
    if (result) {
      //	loan_status.push(REQUEST['20']);
      console.log('API RETURNS SUCCESS ');
      original.status = 20;
      //	original.syncStatus = loan_status;
      original.SI = SI;
      original.total_loan_applied = total_loan_applied;
      original.repayment_amount = repayment_amount;
      original.platform_fees = employer_data[product_name].platform_fees;
      original.processing_fees = processing_amount;
      original.cgst_on_processing_amount = employer_data[product_name].cgst;
      original.sgst_on_processing_amount = employer_data[product_name].sgst;
      original.total_gst_on_processing_amount =
        cgst_on_processing_amount + sgst_on_processing_amount;
      original.cgst_on_platform_fee =
        employer_data[product_name].cgst_on_platform_fee;
      original.sgst_on_platform_fee =
        employer_data[product_name].sgst_on_platform_fee;
      original.total_gst_on_platform_fee =
        cgst_on_platform_fee + sgst_on_platform_fee;
      original.disbursement_date = disbursement_date;
      original.loan_tenure_in_days = date_diff;
      original.first_emi_date = first_emi_date;
      original.interest_rate = employer_data[product_name].interest_rate;

      admin
        .database()
        .ref('users/' + number + '/Loan')
        .child(partner_loan_id)
        .update(original);
      admin
        .database()
        .ref('LoanHistory')
        .child(partner_loan_id)
        .update(original);

      console.log('Cloud Function Create Loan End');
    }
  } else if (employer_data[product_name].NBFC_CLIENT == 'Kudos') {
    original.SI = SI;
    original.total_loan_applied = total_loan_applied;
    original.platform_fees = employer_data[product_name].platform_fees || 0;
    original.processing_fees = processing_amount;
    original.repayment_amount = repayment_amount;
    original.cgst_on_processing_amount = employer_data[product_name].cgst;
    original.sgst_on_processing_amount = employer_data[product_name].sgst;
    original.total_gst_on_processing_amount =
      cgst_on_processing_amount + sgst_on_processing_amount;
    original.cgst_on_platform_fee =
      employer_data[product_name].cgst_on_platform_fee || 0;
    original.sgst_on_platform_fee =
      employer_data[product_name].sgst_on_platform_fee || 0;
    original.total_gst_on_platform_fee =
      cgst_on_platform_fee + sgst_on_platform_fee;
    original.disbursement_date = disbursement_date;
    original.loan_tenure_in_days = date_diff;
    original.first_emi_date = first_emi_date;
    original.interest_rate = employer_data[product_name].interest_rate || 0;

    let timestamp = moment().tz('Asia/Kolkata').unix();
    console.log(timestamp);

    await kudos.callKudos(
      maxAllowedTransaction,
      original,
      check_cut_of_criteria,
      sum,
      user_data,
      employer_data,
      product_name,
      partner_loan_id,
      number,
      total_loan_applied,
      processing_amount,
      first_emi_date,
      start_date,
      timestamp
    );
  }
};
const KudosIntegration = async (req, res, next) => {
  //8743850721
  const employerName = 'PeopleStrong';
  const loan_amount = 100;
  let employer_data = db.database().ref('Client/' + employerName);
  employer_data = await employer_data.once('value');
  employer_data = employer_data.val();

  let user_data = db.database().ref('users/' + req.body.number + '/');
  user_data = await user_data.once('value');
  user_data = user_data.val();

  let aadharfront = await getImage(
    user_data.personal.aadharFrontImagePath,
    'aadharfront'
  );
  let aadharback = await getImage(
    user_data.personal.aadharBackImagePath,
    'aadharback'
  );

  await convertintopdf(aadharfront, aadharback);
  await db
    .storage()
    .bucket('gs://dev-valyu.appspot.com')
    .upload(process.cwd() + '/output.pdf', {
      destination: `userdocuments/${req.body.number}/aadhar.pdf`,
    });

  /*
	let start_date = moment().startOf('month').format('YYYY-MM-DD');
	let end_date = moment().endOf('month').format('YYYY-MM-DD');
	let cut_off_date =
		start_date.split('-')[0] +
		'-' +
		start_date.split('-')[1] +
		'-' +
		employer_data.cutoffday;
	let check_cut_of_criteria = moment(moment().format('YYYY-MM-DD')).isBetween(
		moment(cut_off_date).format('YYYY-MM-DD'),
		end_date,
		null,
		'[]'
	);

	console.log(cut_off_date, check_cut_of_criteria, end_date);
	
	//console.log(JSON.parse(user_data.Loan));
	let sum = 0;
	for (let key in user_data.Loan) {
		//console.log(key);

		//console.log(new Date(user_data.Loan[key].created_at).toISOString());
		let date = new Date(user_data.Loan[key].created_at).toISOString();
		date = moment(date).format('YYYY-MM-DD');
		let bool = moment(date).isBetween(start_date, end_date);
		if (bool) {
			sum = sum + user_data.Loan[key].amount;
		}
	}
	//	console.log(user_data.workProfile[employerName].userData.eligibility_amount);
	if (user_data.workProfile[employerName].userData.eligibility_amount > sum) {
		console.log(sum);
	}

	if (employer_data.advanceLoan.NBFC_CLIENT == 'Kudos') {
		let processing_fees_amt = Math.round(
			(employer_data.advanceLoan.processing_fee * loan_amount) / 100
		);
		const body = [
			{
				partner_loan_id: new Date().getTime(),
				partner_borrower_id: user_data.userProfile.user_id,
				first_name: user_data.userProfile.firstName,
				last_name: user_data.userProfile.lastName,
				sector: 'AL', //Check
				type_of_addr: 'Owned',
				resi_addr_ln1: user_data.userProfile.address1,
				city: user_data.userProfile.city,
				state: user_data.userProfile.city, //TODO:,Check(Get from the api)
				pincode: user_data.userProfile.pinCode,
				per_addr_ln1: user_data.userProfile.address1,
				per_city: user_data.userProfile.city,
				per_state: user_data.userProfile.city,
				per_pincode: user_data.userProfile.pinCode,
				appl_phone: req.body.number, //Need to Change
				appl_pan: user_data.personal.panNumber,
				dob: user_data.userProfile.dob,
				gender: user_data.userProfile.gender,
				photo_id_type: 'pan',
				photo_id_num: user_data.personal.panNumber,
				addr_id_type: 'pan',
				addr_id_num: user_data.personal.panNumber,
				no_year_current_addr: '2',
				cibil_score_borro: '750',
				purpose_of_loan: req.body.purpose, //Need to change
				borro_bank_name: user_data.workProfile[employerName].account.bankName,
				borro_bank_acc_num:
					user_data.workProfile[employerName].account.bankAcNo,
				sanction_date: moment().format('YYYY-MM-DD'), //YYYY-mm-dd
				applied_amount: loan_amount, //Need To Change
				int_rate_reducing_perc: employer_data.advanceLoan.interest_rate, //client
				int_type: 'Flat',
				loan_tenure: '1',
				processing_fees_perc: employer_data.advanceLoan.processing_fee,
				processing_fees_amt: processing_fees_amt,
				borro_bank_ifsc: user_data.workProfile[employerName].account.ifscCode,
				partner_system_score: '65',
			},
		];

		let { result, error } = await handleData(
			'https://stagingplatformapi.kudosfinance.in/v3/api/loanrequest',
			body,
			{
				headers: {
					Authorization:
						'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55X2lkIjoxMzksImNvbXBhbnlfY29kZSI6IlZBTDAxMzAiLCJwcm9kdWN0X2lkIjoyMTcsImxvYW5fc2NoZW1hX2lkIjoiMjM2IiwiY3JlZGl0X3J1bGVfZ3JpZF9pZCI6bnVsbCwiYXV0b21hdGljX2NoZWNrX2NyZWRpdCI6MCwidHlwZSI6ImFwaSIsInRva2VuX2lkIjoiMTM5LTIxNy0xNjA2ODA0NjIxODAxIiwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiaWF0IjoxNjA2ODA0NjIxfQ.3bCcqD2wYS_uvDH_YraowBlgpN1j-688mhQRDGemoWk',
					company_code: 'VAL0078',
				},
			}
		);
		if (error) {
			//console.log(error);
			return;
		}
		let data;
		if (result) {
			data = result.data.data.preparedbiTmpl[0];
			//console.log(result);
		}

		let response = {
			partner_loan_id: data.partner_loan_id,
			partner_borrower_id: data.partner_borrower_id,
			kudos_loan_id: data.kudos_loan_id,
			kudos_borrower_id: data.kudos_borrower_id,
		};

		let body7 = {
			pan: user_data.personal.panNumber,
			sub_company_code: 'VAL0078',
			kudos_loan_id: null,
		};
		console.log(body7);

		({ result, error } = await handleData(
			'https://stagingplatformapi.kudosfinance.in/v3/api/kud_pan_kyc',
			body7,
			{
				headers: {
					Authorization:
						'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55X2lkIjoxMzksImNvbXBhbnlfY29kZSI6IlZBTDAxMzAiLCJ0eXBlIjoic2VydmljZSIsInRva2VuX2lkIjoiMTM5LVZBTDAxMzAtMTYwNjgwNDY3NzMxMCIsImVudmlyb25tZW50Ijoic2FuZGJveCIsImlhdCI6MTYwNjgwNDY3N30.WYFMTFwkaEIOAjnZjICxb3ll-0qPqWI57r98NB64tUg',
					company_code: 'VAL0078',
				},
			}
		));

		if (result) {
			console.log(result, 'aakash');
		}
		if (error) {
			console.log(error);
		}
		let doj = user_data.workProfile[employerName].userData.doj;
		doj = moment(doj, 'DD/MM/YYYY').format('YYYY-MM-DD');
		//console.log(doj);
		let body2 = [
			{
				...response,
				loan_app_date: moment().format('YYYY-MM-DD'),
				age: '23',
				qualification: 'B.E', //chck
				marital_status: 'single', //check,
				sanction_amount: loan_amount,
				'repayment_ type': 30,
				employment_state: 'karnataka', //Check
				first_inst_date: moment().endOf('month').format('YYYY-MM-DD'), //Check
				disburse_date: moment().endOf('month').format('YYYY-MM-DD'), //Check
				bureau_name: 'CIBIL', //Need To Check
				final_approve_status: 'approved', //check
				final_approve_date: moment().format('YYYY-MM-DD'),
				employment_addr_ln_1: 'wewf',
				employment_city: 'bangalore',
				employment_pincode: 560100,
				monthly_income_of_the_borro: parseFloat(
					user_data.workProfile[employerName].userData.salary
				),
				annual_income_of_the_borro: parseFloat(
					user_data.workProfile[employerName].userData.salary * 12
				),
				employer_name: employerName,
				employer_id: user_data.workProfile[employerName].userData.employeeId,
				joining_date: doj,
				job_type: user_data.workProfile[employerName].userData.employmentType,
				final_approved_by: 'admin',
				total_charges:
					processing_fees_amt +
					Math.round(
						(employer_data.advanceLoan.cgst * processing_fees_amt) / 100
					) +
					Math.round(
						(employer_data.advanceLoan.sgst * processing_fees_amt) / 100
					) +
					Math.round(
						(employer_data.advanceLoan.igst * processing_fees_amt) / 100
					),
				net_disbur_amt: loan_amount,
				cgst_on_pf_amt: employer_data.advanceLoan.cgst,
				sgst_on_pf_amt: employer_data.advanceLoan.sgst,
				igst_on_pf_amt: employer_data.advanceLoan.igst,
			},
		];

		({ result, error } = await handleData(
			'https://stagingplatformapi.kudosfinance.in/v3/api/borrowerinfo',
			body2,
			{
				headers: {
					Authorization:
						'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55X2lkIjoxMzksImNvbXBhbnlfY29kZSI6IlZBTDAxMzAiLCJwcm9kdWN0X2lkIjoyMTcsImxvYW5fc2NoZW1hX2lkIjoiMjM2IiwiY3JlZGl0X3J1bGVfZ3JpZF9pZCI6bnVsbCwiYXV0b21hdGljX2NoZWNrX2NyZWRpdCI6MCwidHlwZSI6ImFwaSIsInRva2VuX2lkIjoiMTM5LTIxNy0xNjA2ODA0NjIxODAxIiwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiaWF0IjoxNjA2ODA0NjIxfQ.3bCcqD2wYS_uvDH_YraowBlgpN1j-688mhQRDGemoWk',
					company_code: 'VAL0078',
				},
			}
		));
		if (error) {
			//	console.log(error);
		}

		let virtual_acc_no;
		if (
			user_data.workProfile[employerName].userData.virtual_account_no ==
			undefined
		) {
			({ result, error } = await handleData(
				'https://stagingplatformapi.kudosfinance.in/v3/api/get_va_num',
				[response],
				{
					headers: {
						Authorization:
							'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55X2lkIjoxMzksImNvbXBhbnlfY29kZSI6IlZBTDAxMzAiLCJ0eXBlIjoic2VydmljZSIsInRva2VuX2lkIjoiMTM5LVZBTDAxMzAtMTYwNjgwNDY3NzMxMCIsImVudmlyb25tZW50Ijoic2FuZGJveCIsImlhdCI6MTYwNjgwNDY3N30.WYFMTFwkaEIOAjnZjICxb3ll-0qPqWI57r98NB64tUg',
						company_code: 'VAL0078',
					},
				}
			));
			//	console.log(result.data.data.generatedVaNumbers[0].va_num);
			if (error) {
				//			console.log(error);
				//			console.log(response);
			}
			//virtual_acc_no = result.data.data.generatedVaNumbers[0].va_num;
			virtual_acc_no = 7855868586865;
			await db
				.database()
				.ref('users/' + req.body.number + '/workProfile/' + employerName)
				.child('userData')
				.update({
					virtual_account_no: virtual_acc_no,
					kudos_borrower_id: data.kudos_borrower_id,
				});
		} else {
			virtual_acc_no =
				user_data.workProfile[employerName].userData.virtual_account_no;
		}

		let file = await getImage(req.body.number);
		console.log(file, 'aakash');
		//let file = await fs.readFileSync(process.cwd() + '/public/aadhar.pdf');

		//console.log(file, 'aakash');

		const contents_in_base64 = file.toString('base64');

		let body3 = {
			partner_loan_id: data.partner_loan_id,
			partner_borrower_id: data.partner_borrower_id,
			kudos_loan_id: data.kudos_loan_id,
			kudos_borrower_id: data.kudos_borrower_id, //need to save
			fileType: 'aadhar_card',
			base64pdfencodedfile: contents_in_base64,
		};
		({ result, error } = await handleData(
			'https://stagingplatformapi.kudosfinance.in/v3/api/loandocument',
			body3,
			{
				headers: {
					Authorization:
						'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55X2lkIjoxMzksImNvbXBhbnlfY29kZSI6IlZBTDAxMzAiLCJwcm9kdWN0X2lkIjoyMTcsImxvYW5fc2NoZW1hX2lkIjoiMjM2IiwiY3JlZGl0X3J1bGVfZ3JpZF9pZCI6bnVsbCwiYXV0b21hdGljX2NoZWNrX2NyZWRpdCI6MCwidHlwZSI6ImFwaSIsInRva2VuX2lkIjoiMTM5LTIxNy0xNjA2ODA0NjIxODAxIiwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiaWF0IjoxNjA2ODA0NjIxfQ.3bCcqD2wYS_uvDH_YraowBlgpN1j-688mhQRDGemoWk',
					company_code: 'VAL0078',
				},
			}
		));
		if (error) {
			//console.log(error);
		}
		if (result) {
			//console.log(result, 'aakash');
		}

		let body4 = [
			{
				...response,
				partner_loan_status: 'ACTIVE',
				opening_bal: 10000,
				kudos_proc_fee: 200,
				conve_fee_amnt: 100,
				usable_balance: 10000,
				tenure: 30,
				intrest_rate: employer_data.advanceLoan.interest_rate,
				dpd_rate: 10000,
				vpa_address: virtual_acc_no,
				txn_date: moment().endOf('month').format('YYYY-MM-DD'),
				txn_reference: 'This is reference for the transaction',
			},
		];

		({ result, error } = await handleData(
			'https://stagingplatformapi.kudosfinance.in/v3/api/loanmanage',
			body4,
			{
				headers: {
					Authorization:
						'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55X2lkIjoxMzksImNvbXBhbnlfY29kZSI6IlZBTDAxMzAiLCJwcm9kdWN0X2lkIjoyMTcsImxvYW5fc2NoZW1hX2lkIjoiMjM2IiwiY3JlZGl0X3J1bGVfZ3JpZF9pZCI6bnVsbCwiYXV0b21hdGljX2NoZWNrX2NyZWRpdCI6MCwidHlwZSI6ImFwaSIsInRva2VuX2lkIjoiMTM5LTIxNy0xNjA2ODA0NjIxODAxIiwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiaWF0IjoxNjA2ODA0NjIxfQ.3bCcqD2wYS_uvDH_YraowBlgpN1j-688mhQRDGemoWk',
					company_code: 'VAL0078',
				},
			}
		));
		if (error) {
			//console.log(error);
		}

		let body5 = [
			{
				...response,
				ac_holder_name: 'Please mention valid ac holder name',
				vpa_id: virtual_acc_no,
				txn_id: new Date().getTime(),
				txn_amount: 1000,
				txn_date: moment().endOf('month').format('YYYY-MM-DD'),
				txn_reference: 'Please mention valid txn reference',
			},
		];

		({ result, error } = await handleData(
			'https://stagingplatformapi.kudosfinance.in/v3/api/loanusage',
			body5,
			{
				headers: {
					Authorization:
						'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55X2lkIjoxMzksImNvbXBhbnlfY29kZSI6IlZBTDAxMzAiLCJwcm9kdWN0X2lkIjoyMTcsImxvYW5fc2NoZW1hX2lkIjoiMjM2IiwiY3JlZGl0X3J1bGVfZ3JpZF9pZCI6bnVsbCwiYXV0b21hdGljX2NoZWNrX2NyZWRpdCI6MCwidHlwZSI6ImFwaSIsInRva2VuX2lkIjoiMTM5LTIxNy0xNjA2ODA0NjIxODAxIiwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiaWF0IjoxNjA2ODA0NjIxfQ.3bCcqD2wYS_uvDH_YraowBlgpN1j-688mhQRDGemoWk',
					company_code: 'VAL0078',
				},
			}
		));
		if (error) {
			console.log(error);
			console.log('error');
		}
		//console.log('error');

		let body6 = {
			borrower_mobile: req.body.number,
			ifsc_code: user_data.workProfile[employerName].account.ifscCode,
			amount: loan_amount,
			account_no: user_data.workProfile[employerName].account.bankAcNo,
			...response,
			order_id: new Date().getTime(),
		};
		console.log(body6);

		({ result, error } = await handleData(
			'https://stagingplatformapi.kudosfinance.in/v3/api/paytm-disbursal-imps',
			body6,
			{
				headers: {
					Authorization:
						'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55X2lkIjoxMzksImNvbXBhbnlfY29kZSI6IlZBTDAxMzAiLCJ0eXBlIjoic2VydmljZSIsInRva2VuX2lkIjoiMTM5LVZBTDAxMzAtMTYwNjgwNDY3NzMxMCIsImVudmlyb25tZW50Ijoic2FuZGJveCIsImlhdCI6MTYwNjgwNDY3N30.WYFMTFwkaEIOAjnZjICxb3ll-0qPqWI57r98NB64tUg',
					company_code: 'VAL0078',
				},
			}
		));
		if (error) {
			console.log(error);
		}
	}
};

const handleData = async (url, data, options, type) => {
	let result = undefined;
	let error = undefined;
	try {
		if (type == 'form') {
			result = await axios.post(url, qs.stringify(data), options);
		} else {
			result = await axios.post(url, data, options);
		}

		return { result, error };
	} catch (error) {
		return { result, error };
	}
};

const getImage = async (number) => {
	let file = undefined;
	try {
		file = await db
			.storage()
			.bucket('gs://dev-valyu.appspot.com')
			.file(`userdocuments/${number}/aadhar.pdf`)
			.download();
		return file;
	} catch (error) {
		return file;
	}
	*/
};

const sendEmail = async (req, res, next) => {
  const mailOptions = {
    from: 'lalit.chaturvedi@valyu.ai', // Something like: Jane Doe <janedoe@gmail.com>
    to: ['shuklaaakash78@gmail.com'],
    subject: 'Advance Salary approval request', // email subject
    html:
      '<div>\
			<p>Hello,</p>\
			<p>One of your employee has requested for Advance Salary disbursal on our platform. Here are all the details.</p>\
			<ul>\
			<li>Name- [Employee Full Name]</li>\
			<li>Contact Number – [Mobile Number]</li>\
			<li>Requested on – [Date & Time]</li>\
			<li>Accrued Salary – [Amount]</li>\
			<li>Maximum Eligibility – [Amount]</li>\
			<li>Requested Amount – [Amount]</li>\
			<li>Total Settlement / Deduction Amount – [Amount]</li>\
			<li>Transaction Reference Number – [xxx]</li>\
			</ul>\
			<p>Kindly give your confirmation if we can proceed with disbursal of this amount to your employee.</p>\
			<p>After this disbursal, you would be required to deduct [Total Settlement amount] from this month’s payroll of this employee and deposit the same with Valyu.ai against recovery of this disbursal.</p>\
			<p>regards,<br>Valyu Operation Team</p>\
			</div>', // email content in HTML
  };
  const { result, err } = await Email.SENDEMAIL(mailOptions);
  if (result) {
    res.status(200).send({
      statusMessage: 'Email Sent',
    });
  }
};

const cronJob = async (req, res) => {
  // ...
  const startTime = moment().format();
  const iso = 'T00:00:00Z';
  console.log(
    'Crone Run On Every Day at 23:59 PM, CURRENT TIME IS ',
    new Date().toLocaleString()
  );
  let current_date = moment().format('YYYY-MM-DD');
  let temp = current_date.split('-');
  const collection = await admin.firestore().listCollections();
  let employeeUpdated = [];
  collection.map(async (document) => {
    let documents = await admin.firestore().collection(document.id).get();
    let client_data = admin.database().ref('Client/' + document.id);
    let fetchdata = await client_data.once('value');
    fetchdata = fetchdata.val();
    documents.forEach(async (doc) => {
      console.log(doc.data());
      let employeeId = doc.data().employeeId;
      let product_name = doc.data().product_name;
      let salary = doc.data().salary;
      let payrollStartDate = fetchdata[product_name].payrollStartDate;
      let payrollEndDate = fetchdata[product_name].payrollEndDate;
      let totaldays = moment(payrollEndDate).diff(payrollStartDate, 'days') + 1;
      employeeUpdated.push(employeeId);
      let date_diff = moment(current_date).diff(doc.data().doj, 'days');

      let { eligibilty_percentage } = await getDetails(
        fetchdata,
        product_name,
        salary
      );
      if (date_diff > 0) {
        if (payrollStartDate.split('-')[2] - 1 == temp[2]) {
          await admin
            .database()
            .ref('users/' + doc.id + '/workProfile')
            .child(workProfile + '/userData')
            .update({
              eligibility_amount: 0,
              available_amount: 0,
              applied_amount: 0,
              eligibility_date: current_date,
              totalSuccessfullApplication: 0,
            });
        } else {
          let usefulldata = {
            salary: doc.data().salary,
            doj: doc.data().doj,
            phoneNumber: doc.id,
            per_day_salary: doc.data().salary / totaldays,
          };
          let phoneNumber = doc.id;
          let userdata = admin
            .database()
            .ref(
              'users/' + doc.id + '/workProfile/' + document.id + '/userData'
            );
          userdata = await userdata.once('value');
          let body;
          let eligibility_amount = 0;
          if (userdata.val().eligibility_date != undefined) {
            usefulldata.eligibility_date_exist = true;
            usefulldata.applied_amount = userdata.val().applied_amount;
            usefulldata.available_amount = userdata.val().available_amount;
            let duration = moment(current_date).diff(
              moment(userdata.val().eligibility_date),
              'days'
            );
            if (duration == 0) {
              return;
            }
            let current_iso_date = current_date + iso;
            body = {
              requestorName: 'AltAttendance',
              sysApiCode: 'EmployeeAttendance_solr',
              filterAttrs: [
                {
                  objectCode: 'ALTW_ATTENDANCE',
                  objectAttrCode: 'ORGANIZATIONID',
                  value: '19',
                  operator: '=',
                },
                {
                  objectCode: 'ALTW_ATTENDANCE',
                  objectAttrCode: 'EMPLOYEECODE',
                  value: employeeId,
                  operator: '=',
                },
                {
                  objectCode: 'ALTW_ATTENDANCE',
                  objectAttrCode: 'STARTDATE',
                  value: current_iso_date,
                  operator: '=',
                },
              ],
            };
          }
          if (document.id.toLowerCase() == 'peoplestrong') {
            let { result, error } = await handleData(
              config.PS_ATTENDANCE_URL,
              body,
              {},
              usefulldata
            );

            if (error) {
              console.log('Some Error Occured', error);
            }
            if (result) {
              let count = 0;
              if (result.data != undefined && result.data.data == null) {
                if (result.data.imp_data.eligibility_date_exist) {
                  count = 1;
                  eligibility_amount = (
                    result.data.imp_data.eligibility_amount +
                    (count *
                      result.data.imp_data.per_day_salary *
                      eligibilty_percentage) /
                      100
                  ).toFixed(2);
                }
              } else if (
                result.data != undefined &&
                result.data.data.length != 0
              ) {
                let data = JSON.parse(result.data.data);

                for (let i = 0; i < data.data.length; i++) {
                  if (data.data[i].ACTUAL_STATUS === 'Absent') {
                    count++;
                  }
                }
                if (result.data.imp_data.eligibility_date_exist) {
                  if (count > 0) {
                    eligibility_amount = (
                      result.data.imp_data.eligibility_amount -
                      (count *
                        result.data.imp_data.per_day_salary *
                        eligibilty_percentage) /
                        100
                    ).toFixed(2);
                  } else {
                    eligibility_amount = (
                      result.data.imp_data.eligibility_amount +
                      (count *
                        result.data.imp_data.per_day_salary *
                        eligibilty_percentage) /
                        100
                    ).toFixed(2);
                  }
                }
              } else if (
                result.data != undefined &&
                result.data.data.length == 0
              ) {
                if (result.data.imp_data.eligibility_date_exist) {
                  count = 1;
                  eligibility_amount = (
                    result.data.imp_data.eligibility_amount +
                    (count *
                      result.data.imp_data.per_day_salary *
                      eligibilty_percentage) /
                      100
                  ).toFixed(2);
                }
              }
            }
          } else {
            count = 1;
            eligibility_amount = (
              usefulldata.eligibility_amount +
              (count * usefulldata.per_day_salary * eligibilty_percentage) / 100
            ).toFixed(2);
          }
          if (
            eligibility_amount > fetchdata[product_name].maximum_loan_amount
          ) {
            eligibility_amount = fetchdata[product_name].maximum_loan_amount;
          }
          await admin
            .database()
            .ref('users/' + phoneNumber + '/workProfile')
            .child(workProfile + '/userData')
            .update({
              eligibility_amount: parseFloat(eligibility_amount),
              eligibility_date: current_date,
              available_amount: parseFloat(
                (eligibility_amount - usefulldata.applied_amount).toFixed(2)
              ),
            });

          console.log('updated');
        }
      } else {
        //Not eligible,employee just joined
        console.log('Employeee Not Eligible For Loan Yet');
      }
    });
  });
  /*
	await admin
		.database()
		.ref('cronStats')
		.child(new Date().getTime() + '')
		.set({
			startTime,
			endTime: new Date().toLocaleString(),
			employeeUpdated,
		});

	console.log('cronjob works');
	*/
  return true;
};
module.exports = {
  KudosIntegration,
  cronJob,
  sendEmail,
  KudosFinal,
};
