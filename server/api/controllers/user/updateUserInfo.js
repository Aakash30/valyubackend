const rdbInstance = require('../../../../functions/helper/rdb');
const {
	getUserInformation,
} = require('../../../../functions/util/fetchInfoFromDB');

const updateUserInfo = async (userIdentifier, userData) => {
	//Information to be updated
	const {
		panNumber,
		aadharNumber,
		ifscCode,
		bankName,
		bankAcNo,
		firstName,
		lastName,
		dob,
		salary,
	} = userData;

	const userInformation = await getUserInformation(userIdentifier);
	let { personal, userProfile, workProfile } = userInformation;

	let {
		panNumber: existingPanNumber,
		aadharNumber: existingAadharNumber, //Not Entered By Backend
	} = personal;

	personal.panNumber = panNumber ? panNumber : existingPanNumber;
	personal.aadharNumber = aadharNumber
		? aadharNumber
		: existingAadharNumber
		? existingAadharNumber
		: undefined;

	if (personal.aadharNumber == undefined) {
		delete personal.aadharNumber;
	}
	let {
		firstName: existingFirstName,
		lastName: existingLastName,
		dob: existingDob,
	} = userProfile;

	userProfile.firstName = firstName ? firstName : existingFirstName;
	userProfile.lastName = lastName ? firstName : existingLastName;
	userProfile.firstName = dob ? dob : existingDob;

	let clients = Object.keys(workProfile);
	let clientName = clients[0];
	let { userData: userInformationData, account } = workProfile[clientName];

	let { salary: existingUserSalary } = userInformationData;
	workProfile[clientName].userData.salary = salary
		? salary
		: existingUserSalary;

	let {
		bankAcNo: existingBankAcNo,
		bankName: existingBankName,
		ifscCode: existingIfscCode,
	} = account;

	workProfile[clientName].account.bankAcNo = bankAcNo
		? bankAcNo
		: existingBankAcNo;

	workProfile[clientName].account.bankName = bankName
		? bankName
		: existingBankName;

	workProfile[clientName].account.ifscCode = ifscCode
		? ifscCode
		: existingIfscCode;

	/*
	let {
		firstName: existingFirstName,
		lastName: existingLastName,
		dob: existingDob,
	} = userProfile;

	let clients = Object.keys(workProfile);
	let clientName = clients[0];
	let { userData: userInformationData, account } = workProfile[clientName];

	let { salary: existingUserSalary } = userInformationData;

	let {
		bankAcNo: existingBankAcNo,
		bankName: existingBankName,
		ifscCode: existingIfscCode,
	} = account;

	let newPersonalDataInformation = {
		panNumber: panNumber || existingPanNumber,
		aadharNumber: aadharNumber || existingAadharNumber,
	};
	let newUserProfileInformation = {
		firstName: firstName || existingFirstName,
		lastName: lastName || existingLastName,
		dob: dob || existingDob,
	};
	let newUserDataInformation = {
		salary: salary || existingUserSalary,
	};
	let newAcountInformation = {
		bankName: bankName || existingBankName,
		bankAcNo: bankAcNo || existingBankAcNo,
		ifscCode: ifscCode || existingIfscCode,
	};
	let updateData = {
		userProfile: { ...newUserProfileInformation },
		personal: { ...newPersonalDataInformation },
		workProfile: {},
	};
	updateData.workProfile[clientName] = {
		userData: { ...newUserDataInformation },
		account: { ...newAcountInformation },
	};
	//updateUserInformation
    */
	await rdbInstance
		.ref('users/')
		.child(userIdentifier)
		.update({ personal, userProfile, workProfile });
};
module.exports = updateUserInfo;
