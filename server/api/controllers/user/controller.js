import retryLoanProcess from '../../../../functions/loans/retryLoanProcess';
import { getLoanInformation } from '../../../../functions/util/fetchInfoFromDB';
import {
	getClientInformation,
	getUserInformation,
} from '../../../../functions/util/fetchInfoFromDB';
import BaseControllerClass from '../../../common/baseApiController';
import dbService from '../../services/db.service';
const moment = require('moment');
const { momentInstance } = require('../../../../functions/helper/moment');
//let calculateData = require('../../cronjob/cronJobController').cronjob;
const updateEligibility = require('../../../../functions/util/updateEligibility');
const updateUserInfo = require('./updateUserInfo');
const { asyncForEach } = require('../../../../functions/util/asyncForEach');
let { getInstance } = require('../../../common/db');
let csv = require('csvtojson');
const Joi = require('joi').extend(require('@joi/date'));
const axios = require('axios');

export class UserController extends BaseControllerClass {
	constructor() {
		super('users');
	}
	async create(req, res, next) {
		try {
			console.log('POST CREATE USER');
			const schema = Joi.object().keys({
				firstName: Joi.string().required(), //
				lastName: Joi.string().required(), //
				gender: Joi.string()
					.valid('Male', 'Female', 'Others', 'Not Disclosed')
					.required(), //
				address1: Joi.string(), //NM
				pinCode: Joi.number(), //NM
				city: Joi.string(), //NM
				mobileNumber: Joi.string().required(), //
				doj: Joi.date()
					.format('YYYY-MM-DD')
					.max(moment().format('YYYY-MM-DD'))
					.required(), //
				dob: Joi.date()
					.format('YYYY-MM-DD')
					.max(moment().format('YYYY-MM-DD'))
					.required(), //
				emailId: Joi.string().email().required(), //
				employeeId: Joi.string().required(), //
				panNumber: Joi.string().required(), //
				designation: Joi.string(), //NM
				employmentType: Joi.string().required(), //
				officePincode: Joi.number(), //NM
				bankName: Joi.string().required(), //
				bankAcNo: Joi.string().required(), //
				ifscCode: Joi.string().required(), ///
				officeAddress: Joi.string(), //NM
				salary: Joi.any(), //
				fathername: Joi.string().allow(null), //NM
				motherName: Joi.string().allow(null), //NM
				currentEmployer: Joi.string(),
				product_name: Joi.string(),
				token: Joi.any(),
			});
			req.body.employeeId = req.body.employeeCode;
			delete req.body.employeeCode;

			const { value, error } = await schema.validate(req.body);
			//console.log(error);
			if (error) {
				res.status(422).json({
					status: 'error',
					message: 'Invalid request data',
					data: error.details[0].message,
				});
				return;
			}
			const employerName = req.body.currentEmployer;
			const number = req.body.mobileNumber;

			if (!number) {
				res.status(404).send({ message: 'Please provide the Phone number' });
				return;
			}
			if (!employerName) {
				res
					.status(404)
					.send({ message: 'Please provide the Current Employer' });
				return;
			}

			let employerData = dbService.findInDB('Client/' + employerName);
			employerData = await employerData.once('value');
			employerData = employerData.val();

			if (!employerData) {
				res.status(404).send({ message: 'Employer Does not Exist' });
				return;
			}

			let { firstName, product_name, employeeId, doj } = req.body;
			const user_id = firstName.toLowerCase() + new Date().getTime();
			req.body.user_id = user_id;

			// call peopleStrong to decode the salary

			if (employerName.toLowerCase() === 'peoplestrong') {
				// TODO: fix this comparison part
				const salaryResult = await axios.get(
					'https://Paysmart.peoplestrong.com/AltPayrollApi/api/Decript',
					{
						headers: {
							data: req.body.salary,
							key:
								'BwIAAACkAABSU0EyAAQAAAEAAQA1c7J0iJVIjeovnij+pVQpPzPhCe+8NoIoOi/te5sdnZU47bP1ph3nJOdq4zrDVLSJt3Bo6zt0zC+CE2YtXOdE7ZBvv4JkucPCanFmiOZIcbxlHS/5QLaouzwwUSC6wxhueGZfrX+CDTpv7ws+KgWbpvqHzTgmkdkSwp5XHBCRzANqq7Ixv88+HmHpAeFKKAbKOv9ZphILwDR6oi5h3BkOcXGpb25DlumTwBNnTTOdarNuEmuS9KV7Cwp9wVIf8vhnRIaEOwpg7BmREydnpUmV8mPkumFEFVQd6k6bHvRIMaMEUqZFY8Y24k8RF+ojA7CIXkp0WsDyT0b5OeeGAl3Sv5xc1dfC/NI6eTbVgDgguMO6YS2nqVSnhUiVeSwvvoMwkevBKJWJxhTe3fV8XINL8wVgciKKLAPRBvtj7ezdnsnuQxAeGmGha+SvUNzzqGPDH0TDEkCrOi/LjW4M7z8/F4fsVQnuH0HsIH/JPutzekZk1eXm8/e8+gIkdT/h1o+fXLe7ohSRiy85uufYetFOX1hoPZVLKW3AlXfOeRRh2QKmyRRTC1otkyp0VP5LGtPM76FTqCeVDKYiI8RA+a2GmUQcO1MpsNtNKteGxXWTorDJ1FmC/HIe3ELWg6CWNrl9kG5AhKp06b+Jt/PQu/22gI9+bbcmupQ5taxuNZvL/o41WWhjzW2iZbMWhbbvyjV9iyItfuDO+JqOmbKZTXOyomJ03v35YZjgm9SeJCOAJQq/i+nobHYARcrkgqhVNas=',
						},
					}
				);
				req.body.salaryByPS = req.body.salary;
				req.body.salary = salaryResult.data.NetSal;
				req.body.netSalary = salaryResult.data.NetSal;
				req.body.grossSalary = salaryResult.data.Gross;
			}

			const checkuser = await dbService.findOne(employerName, number, {});
			if (!checkuser.exists) {
				let fetchdata = dbService.findInDB(this.modelName + '/' + number, {});
				await dbService.create(employerName, number, { ...req.body }, {});
				fetchdata = await fetchdata.once('value');

				let insert_data;
				if (fetchdata.val() == null) {
					insert_data = {
						userProfile: {
							firstName: firstName,
							lastName: req.body.lastName,
							gender: req.body.gender,
							dob: req.body.dob,
							fathername: req.body.fathername || null,
							motherName: req.body.motherName || null,
							pinCode: req.body.pinCode || null,
							address1: req.body.address1 || null,
							city: req.body.city || null,
							user_id: user_id,
						},
						workProfile: {
							[employerName]: {
								userData: {
									emailId: req.body.emailId,
									employmentType: req.body.employmentType,
									officePincode: req.body.officePincode || null,
									doj: doj,
									designation: req.body.designation || null,
									officeAddress: req.body.officeAddress || null,
									salary: req.body.salary,
									employeeId: employeeId,
									product_name: product_name,
								},
								account: {
									bankName: req.body.bankName,
									bankAcNo: req.body.bankAcNo,
									ifscCode: req.body.ifscCode,
								},
							},
						},

						personal: {
							panNumber: req.body.panNumber,
						},
					};
				} else {
					insert_data = fetchdata.val();
					insert_data.workProfile[employerName] = {
						userData: {
							emailId: req.body.emailId,
							employmentType: req.body.employmentType,
							officePincode: req.body.officePincode || null,
							doj: doj,
							designation: req.body.designation || null,
							officeAddress: req.body.officeAddress || null,
							salary: req.body.salary,
							employeeId: employeeId,
							product_name: product_name,
						},
						account: {
							bankName: req.body.bankName,
							bankAcNo: req.body.bankAcNo,
							ifscCode: req.body.ifscCode,
						},
					};
				}

				const insertdata = await dbService.insertIntoDatabase(
					this.modelName,
					number,
					insert_data
				);

				//res.json(insertdata);
				fetchdata = dbService.findInDB(this.modelName + '/' + number, {});
				fetchdata = await fetchdata.once('value');
				const previousDate = momentInstance
					.clone()
					.subtract(1, 'days')
					.format('YYYY-MM-DD');
				await updateEligibility(
					number,
					employerName,
					employerData,
					product_name,
					req.body.salary,
					employeeId,
					doj,
					previousDate
				);

				res.status(200).send({
					statusMessage: 'Customer Added!',
					statusCode: '200',
					custRefNo: number,
					summary: null,
					header: employerData.headerText, //
					body: employerData.body, //
					footer: employerData.footer, //
					buttonText: 'Get Cash Now',
				});
			} else {
				res.status(201).send({
					statusMessage: 'User Already Exist',
					statusCode: '201',
					custRefNo: number,
					summary: null,
					header: employerData.headerText, //
					body: employerData.body, //
					footer: employerData.footer, //
					buttonText: 'Get Cash Now',
				});
			}
		} catch (err) {
			next(err);
		}
		console.log('POST CREATE USER END');
	}
	async update(req, res, next) {
		try {
			//console.log('POST UPDATE LOAN');
			const number = parseInt(req.params.number);

			let data = dbService.findInDB('users' + '/' + number, {});
			data = await data.once('value');

			let key = Object.keys(data.val().workProfile)[0];

			let current_employer = req.body.currentEmployer || key;

			let update_data = {
				userProfile: {
					firstName:
						req.body.firstName || data.val().userProfile.firstName || '',
					lastName: req.body.lastName || data.val().userProfile.lastName || '',
					gender: req.body.gender || data.val().userProfile.gender || '',
					dob: req.body.dob || data.val().userProfile.dob || '',
					fathername:
						req.body.fathername || data.val().userProfile.fathername || '',
					motherName:
						req.body.motherName || data.val().userProfile.motherName || '',
					pinCode: req.body.pinCode || data.val().userProfile.pinCode || '',
					address1: req.body.address1 || data.val().userProfile.address1 || '',
					city: req.body.city || data.val().userProfile.city || '',
					user_id: data.val().userProfile.user_id,
				},
				workProfile: {
					[current_employer]: {
						userData: {
							emailId:
								req.body.emailId ||
								data.val().workProfile[current_employer].userData.emailId ||
								'',
							employmentType:
								req.body.employmentType ||
								data.val().workProfile[current_employer].userData
									.employmentType ||
								'',
							officePincode:
								req.body.officePincode ||
								data.val().workProfile[current_employer].userData
									.officePincode ||
								'',
							doj:
								req.body.doj ||
								data.val().workProfile[current_employer].userData.doj ||
								'',
							designation:
								req.body.designation ||
								data.val().workProfile[current_employer].userData.designation ||
								'',
							officeAddress:
								req.body.officeAddress ||
								data.val().workProfile[current_employer].userData
									.officeAddress ||
								'',
							salary:
								req.body.salary ||
								data.val().workProfile[current_employer].userData.salary ||
								'',
							employeeId:
								req.body.employeeId ||
								data.val().workProfile[current_employer].userData.employeeId,
						},

						account: {
							bankName:
								req.body.bankName ||
								data.val().workProfile[current_employer].account.bankName ||
								'',
							bankAcNo:
								req.body.bankAcNo ||
								data.val().workProfile[current_employer].account.bankAcNo ||
								'',
							ifscCode:
								req.body.ifscCode ||
								data.val().workProfile[current_employer].account.ifscCode ||
								'',
						},
					},
				},
			};
			let data2 = await dbService
				.findInDB(
					'users' + '/' + number + '/',

					{}
				)
				.update({ ...update_data });
			let data3 = await dbService.updateOne(
				current_employer,
				number + '',
				req.body,
				{}
			);

			res.status(200).send({ message: 'Updated Successfully' });
		} catch (err) {
			next(err);
		}
		//	console.log('POST UPDATE LOAN ENDS');
	}
	async restore(req, res, next) {
		try {
			let collection = await dbService.getAllCollection();
			let documents = [];
			for (let i = 0; i < collection.length; i++) {
				let collectionData = await dbService.findAll(collection[i], {});
				collectionData.forEach((doc) => {
					let id = doc.id;

					documents.push({ [id]: doc.data() });
				});
			}

			let insert_data;
			for (let j = 0; j < documents.length; j++) {
				let key = Object.keys(documents[j])[0];

				let current_employer = documents[j][key].currentEmployer;

				let fetchdata = dbService.findInDB('users' + '/' + parseInt(key), {});
				fetchdata = await fetchdata.once('value');

				if (fetchdata.val() == null) {
					insert_data = {
						userProfile: {
							firstName: documents[j][key].firstName,
							lastName: documents[j][key].lastName,
							gender: documents[j][key].gender,
							dob: documents[j][key].dob,
							fathername: documents[j][key].fathername,
							motherName: documents[j][key].motherName,
							pinCode: documents[j][key].pinCode,
							address1: documents[j][key].address1,
							city: documents[j][key].city,
						},
						workProfile: {
							[current_employer]: {
								userData: {
									emailId: documents[j][key].emailId,
									employmentType: documents[j][key].employmentType,
									officePincode: documents[j][key].officePincode,
									doj: documents[j][key].doj,
									designation: documents[j][key].designation,
									officeAddress: documents[j][key].officeAddress,
									salary: documents[j][key].salary,
									employeeId: documents[j][key].employeeId,
								},
								account: {
									bankName: documents[j][key].bankName,
									bankAcNo: documents[j][key].bankAcNo,
									ifscCode: documents[j][key].ifscCode,
								},
							},
						},

						personal: {
							panNumber: documents[j][key].panNumber,
						},
					};
				} else {
					insert_data = fetchdata.val();
					insert_data.workProfile[current_employer] = {
						userData: {
							emailId: documents[j][key].emailId,
							employmentType: documents[j][key].employmentType,
							officePincode: documents[j][key].officePincode,
							doj: documents[j][key].doj,
							designation: documents[j][key].designation,
							officeAddress: documents[j][key].officeAddress,
							salary: documents[j][key].salary,
							employeeId: documents[j][key].employeeId,
						},
						account: {
							bankName: documents[j][key].bankName,
							bankAcNo: documents[j][key].bankAcNo,
							ifscCode: documents[j][key].ifscCode,
						},
					};
				}

				let insertdata = await dbService.insertIntoDatabase(
					'users',
					parseInt(key),
					insert_data
				);
			}

			let loanHistory = dbService.findInDB('LoanHistory', {});
			loanHistory = await loanHistory.once('value');
			let keys_id = Object.keys(loanHistory.val());
			loanHistory = loanHistory.val();

			for (let i = 0; i < keys_id.length; i++) {
				let loan_history_id = keys_id[i];

				let timestamp = parseInt(loan_history_id.split('_')[0]);
				let data = loanHistory[loan_history_id];

				let loanData = await dbService.insertIntoDatabase(
					'users' + '/' + data.number,
					'Loan' + '/' + timestamp,
					data
				);
			}

			let transactionHistory = dbService.findInDB('TransactionHistory', {});
			transactionHistory = await transactionHistory.once('value');
			keys_id = Object.keys(transactionHistory.val());
			transactionHistory = transactionHistory.val();

			for (let i = 0; i < keys_id.length; i++) {
				let transaction_history_id = keys_id[i];

				let timestamp = parseInt(transaction_history_id.split('_')[0]);
				let number = parseInt(transaction_history_id.split('_')[1]);
				let data = transactionHistory[transaction_history_id];

				let transactionData = await dbService.insertIntoDatabase(
					'users' + '/' + number,
					'Transaction' + '/' + timestamp,
					data
				);
			}
			res.status(200).send({ message: 'Operation SuccesFull' });
		} catch (error) {}
	}
	async imageUpload(req, res, next) {
		try {
			// 		var form = new formidable.IncomingForm();

			// form.parse(req);

			// form.on('fileBegin', function (name, file){
			//     file.path = __dirname + '../uploads/' + file.name;
			// });

			// form.on('file', function (name, file){

			// });

			let file = req.file;
			if (file) {
				uploadImageToStorage(file).then((success) => {
					res.status(200).send({
						status: 'success',
					});
				});
			}
		} catch (error) {}
	}
	async uploadFile(req, res, next) {
		let storage = getInstance().storage().bucket();

		let filename = req.file;

		const metadata = {
			metadata: {
				// This line is very important. It's to create a download token.
				//	firebaseStorageDownloadTokens: uuid(),
			},

			cacheControl: 'public, max-age=31536000',
		};

		// Uploads a local file to the bucket
		await storage.upload(filename, {
			// Support for HTTP requests made with `Accept-Encoding: gzip`
			gzip: true,
			metadata: metadata,
		});
	}
	async createUser(req, res, next) {
		//console.log(req.body.file);
		let filePath = process.cwd() + '/public/result.csv';
		const jsonArray = await csv().fromFile(filePath);
		//console.log(jsonArray);
		let pushdata = {};
		for (let i = 0; i < jsonArray.length; i++) {
			let number = jsonArray[i].mobileNumber;
			let current_employer = jsonArray[i].currentEmployer;
			let user_id = jsonArray[i].firstName.toLowerCase() + new Date().getTime();
			let checkuser = await dbService.findOne(current_employer, number, {});
			if (!checkuser.exists) {
				let fetchdata = dbService.findInDB('users' + '/' + number, {});
				let data = await dbService.create(
					current_employer,
					number,
					{ ...jsonArray[i] },
					{}
				);
				fetchdata = await fetchdata.once('value');

				let insert_data;
				if (fetchdata.val() == null) {
					insert_data = {
						userProfile: {
							firstName: jsonArray[i].firstName,
							lastName: jsonArray[i].lastName,
							gender: jsonArray[i].gender,
							dob: jsonArray[i].dob,
							fathername: jsonArray[i].fathername,
							motherName: jsonArray[i].motherName,
							pinCode: jsonArray[i].pinCode,
							address1: jsonArray[i].address1,
							city: jsonArray[i].city,
							user_id: user_id,
						},
						workProfile: {
							[current_employer]: {
								userData: {
									emailId: jsonArray[i].emailId,
									employmentType: jsonArray[i].employmentType,
									officePincode: jsonArray[i].officePincode,
									doj: jsonArray[i].doj,
									designation: jsonArray[i].designation,
									officeAddress: jsonArray[i].officeAddress,
									salary: jsonArray[i].salary,
									employeeId: jsonArray[i].employeeId,
								},
								account: {
									bankName: jsonArray[i].bankName,
									bankAcNo: jsonArray[i].bankAcNo,
									ifscCode: jsonArray[i].ifscCode,
								},
							},
						},

						personal: {
							panNumber: jsonArray[i].panNumber,
						},
					};
				} else {
					insert_data = fetchdata.val();
					insert_data.workProfile[current_employer] = {
						userData: {
							emailId: jsonArray[i].emailId,
							employmentType: jsonArray[i].employmentType,
							officePincode: jsonArray[i].officePincode,
							doj: jsonArray[i].doj,
							designation: jsonArray[i].designation,
							officeAddress: jsonArray[i].officeAddress,
							salary: jsonArray[i].salary,
							employeeId: jsonArray[i].employeeId,
						},
						account: {
							bankName: jsonArray[i].bankName,
							bankAcNo: jsonArray[i].bankAcNo,
							ifscCode: jsonArray[i].ifscCode,
						},
					};
				}
				//pushdata.push({ [number]: insert_data });
				//	pushdata[number] = insert_data;

				const insertdata = await dbService.insertIntoDatabase(
					'users',
					number,
					insert_data
				);
				calculateData('one', number, current_employer);
			} else {
				console.log('Employee Already Exist');
			}
		}
		//console.log(pushdata);

		res.status(200).send({ message: 'Operations Successfully' });
	}
	async generateClientConfig(req, res, next) {
		console.log('API');
		let baseClientconfig = {
			address: 'Vikaspuri',
			advanceLoan_subscription: {
				NBFC_CLIENT: 'Kudos',
				cgst: 9,
				cutoffDate: '2021-03-22',
				final_step_message: 'Your request will be processed with in 48 Hours!',
				maximum_loan_amount: 50000,
				min_loan_amount: 100,
				payDate: '2021-03-22',
				payrollEndDate: '2021-03-22',
				payrollStartDate: '2021-02-23',
				rules: {
					'15000': {
						blackoutEndDate: '2021-03-08',
						blackoutStartDate: '2021-02-23',
						eligibilty_percentage: 40,
						fee: 130,
						maxAllowedTransaction: 4,
					},
					'20000': {
						blackoutEndDate: '2021-03-08',
						blackoutStartDate: '2021-02-23',
						eligibilty_percentage: 40,
						fee: 150,
						maxAllowedTransaction: 4,
					},
					'25000': {
						blackoutEndDate: '2021-03-08',
						blackoutStartDate: '2021-02-23',
						eligibilty_percentage: 40,
						fee: 175,
						maxAllowedTransaction: 4,
					},
					max: {
						blackoutEndDate: '2021-03-08',
						blackoutStartDate: '2021-02-23',
						eligibilty_percentage: 40,
						fee: 225,
						maxAllowedTransaction: 4,
					},
				},
				settlementDate: '2021-03-22',
				sgst: 9,
				welcomeText:
					'Get up to TRANSACTIONCOUNT instant advances on your salary anytime after mid of your pay cycle. You can avail this offer at a flat monthly fee of Rs. FEE only.',
			},
			body: 'Welcome back to Valyu',
			city: 'Bangalore',
			clientId: 4002,
			clientName: 'QuessCorp',
			createLoanNotification: {
				body: 'Your Loan request has been raised',
				title: 'Loan Request',
			},
			description: 'IT Service Based Company',
			disbursementAutomation: false,
			footer: 'Valyu.ai',
			forceKycNameCheck: false,
			headerText: 'Get instant salary',
			loanVerificationEmailId: 'shuklaaakash78@gmail.com',
			logoUrl: '/logo/quess-logo.png',
			pinCode: 560100,
			welcomeText:
				'Get up to TRANSACTIONCOUNT instant advances on your salary anytime after mid of your pay cycle. You can avail this offer at a flat monthly fee of Rs. FEES only.',
		};

		let allClientconfig = {};
		let quessclientList = {};
		let start = 23;
		let end = 28;
		for (let i = start; i <= end; i++) {
			quessclientList[i] = `QuessCorp${i}`;
			allClientconfig[`QuessCorp${i}`] = {
				...baseClientconfig,
				clientId: baseClientconfig.clientId + i,
				advanceLoan_subscription: {
					...baseClientconfig.advanceLoan_subscription,
					cutoffDate: moment(
						baseClientconfig.advanceLoan_subscription.cutoffDate,
						'YYYY-MM-DD'
					)
						.add(i - start, 'days')
						.format('YYYY-MM-DD'),
					payDate: moment(
						baseClientconfig.advanceLoan_subscription.payDate,
						'YYYY-MM-DD'
					)
						.add(i - start, 'days')
						.format('YYYY-MM-DD'),
					payrollEndDate: moment(
						baseClientconfig.advanceLoan_subscription.payrollEndDate,
						'YYYY-MM-DD'
					)
						.add(i - start, 'days')
						.format('YYYY-MM-DD'),
					payrollStartDate: moment(
						baseClientconfig.advanceLoan_subscription.payrollStartDate,
						'YYYY-MM-DD'
					)
						.add(i - start, 'days')
						.format('YYYY-MM-DD'),
					rules: {
						'15000': {
							blackoutEndDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['15000']
									.blackoutEndDate,
								'YYYY-MM-DD'
							)
								.add(i - start, 'days')
								.format('YYYY-MM-DD'),
							blackoutStartDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['15000']
									.blackoutStartDate,
								'YYYY-MM-DD'
							)
								.add(i - start, 'days')
								.format('YYYY-MM-DD'),
							eligibilty_percentage: 40,
							fee: 130,
							maxAllowedTransaction: 4,
						},
						'20000': {
							blackoutEndDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['20000']
									.blackoutEndDate,
								'YYYY-MM-DD'
							)
								.add(i - start, 'days')
								.format('YYYY-MM-DD'),
							blackoutStartDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['20000']
									.blackoutStartDate,
								'YYYY-MM-DD'
							)
								.add(i - start, 'days')
								.format('YYYY-MM-DD'),
							eligibilty_percentage: 60,
							fee: 150,
							maxAllowedTransaction: 4,
						},
						'25000': {
							blackoutEndDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['25000']
									.blackoutEndDate,
								'YYYY-MM-DD'
							)
								.add(i - start, 'days')
								.format('YYYY-MM-DD'),
							blackoutStartDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['25000']
									.blackoutStartDate,
								'YYYY-MM-DD'
							)
								.add(i - start, 'days')
								.format('YYYY-MM-DD'),
							eligibilty_percentage: 40,
							fee: 175,
							maxAllowedTransaction: 4,
						},
						max: {
							blackoutEndDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['max']
									.blackoutEndDate,
								'YYYY-MM-DD'
							)
								.add(i - start, 'days')
								.format('YYYY-MM-DD'),
							blackoutStartDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['max']
									.blackoutStartDate,
								'YYYY-MM-DD'
							)
								.add(i - start, 'days')
								.format('YYYY-MM-DD'),
							eligibilty_percentage: 40,
							fee: 225,
							maxAllowedTransaction: 4,
						},
					},
				},
			};
		}
		let i;
		let constant = 3;
		for (let k = 1; k <= 23; k++) {
			i = end - start + k;
			quessclientList[k] = `QuessCorp${k}`;
			allClientconfig[`QuessCorp${k}`] = {
				...baseClientconfig,
				clientId: baseClientconfig.clientId + i,
				advanceLoan_subscription: {
					...baseClientconfig.advanceLoan_subscription,
					cutoffDate: moment(
						baseClientconfig.advanceLoan_subscription.cutoffDate,
						'YYYY-MM-DD'
					)
						.add(i + constant, 'days')
						.format('YYYY-MM-DD'),
					payDate: moment(
						baseClientconfig.advanceLoan_subscription.payDate,
						'YYYY-MM-DD'
					)
						.add(i + constant, 'days')
						.format('YYYY-MM-DD'),
					payrollEndDate: moment(
						baseClientconfig.advanceLoan_subscription.payrollEndDate,
						'YYYY-MM-DD'
					)
						.add(i + constant, 'days')
						.format('YYYY-MM-DD'),
					payrollStartDate: moment(
						baseClientconfig.advanceLoan_subscription.payrollStartDate,
						'YYYY-MM-DD'
					)
						.add(i, 'days')
						.format('YYYY-MM-DD'),
					rules: {
						'15000': {
							blackoutEndDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['15000']
									.blackoutEndDate,
								'YYYY-MM-DD'
							)
								.add(i, 'days')
								.format('YYYY-MM-DD'),
							blackoutStartDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['15000']
									.blackoutStartDate,
								'YYYY-MM-DD'
							)
								.add(i, 'days')
								.format('YYYY-MM-DD'),
							eligibilty_percentage: 40,
							fee: 130,
							maxAllowedTransaction: 4,
						},
						'20000': {
							blackoutEndDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['20000']
									.blackoutEndDate,
								'YYYY-MM-DD'
							)
								.add(i, 'days')
								.format('YYYY-MM-DD'),
							blackoutStartDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['20000']
									.blackoutStartDate,
								'YYYY-MM-DD'
							)
								.add(i, 'days')
								.format('YYYY-MM-DD'),
							eligibilty_percentage: 60,
							fee: 150,
							maxAllowedTransaction: 4,
						},
						'25000': {
							blackoutEndDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['25000']
									.blackoutEndDate,
								'YYYY-MM-DD'
							)
								.add(i, 'days')
								.format('YYYY-MM-DD'),
							blackoutStartDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['25000']
									.blackoutStartDate,
								'YYYY-MM-DD'
							)
								.add(i, 'days')
								.format('YYYY-MM-DD'),
							eligibilty_percentage: 40,
							fee: 175,
							maxAllowedTransaction: 4,
						},
						max: {
							blackoutEndDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['max']
									.blackoutEndDate,
								'YYYY-MM-DD'
							)
								.add(i, 'days')
								.format('YYYY-MM-DD'),
							blackoutStartDate: moment(
								baseClientconfig.advanceLoan_subscription.rules['max']
									.blackoutStartDate,
								'YYYY-MM-DD'
							)
								.add(i, 'days')
								.format('YYYY-MM-DD'),
							eligibilty_percentage: 40,
							fee: 225,
							maxAllowedTransaction: 4,
						},
					},
				},
			};
		}
		let data2 = await dbService
			.findInDB(
				'Client',

				{}
			)
			.update({ ...allClientconfig });

		await dbService
			.findInDB(
				'Constant/' + 'quessClientList',

				{}
			)
			.update({ ...quessclientList });
		res.status(200).send({ allClientconfig, quessclientList });
	}
	async updateUserAndRunRetryLoans(req, res, next) {
		try {
			let { loanId, userData } = req.body;
			if (!loanId || !userData) {
				return res
					.status(400)
					.send({ message: 'Please provide loanId and userData' });
			}
			const { loanData, userIdentifier } = await getLoanInformation(loanId);
			if (!loanData) {
				return res.status(400).send({ message: 'LoanId does not exist' });
			}
			await updateUserInfo(userIdentifier, userData);
			await retryLoanProcess(loanId);
			return res
				.status(200)
				.send({ message: 'Retry Loan is called after updating user Info' });
		} catch (error) {
			next(error);
		}
	}
	async updateEligibilityForUser(req, res, next) {
		try {
			let { userIdentifiers } = req.body;
			if (!Array.isArray(userIdentifiers)) {
				return res.status(404).send({
					message: 'Please provide userIdentifiers key in Array format',
				});
			} else {
				const previousDate = momentInstance
					.clone()
					.subtract(1, 'days')
					.format('YYYY-MM-DD');
				await asyncForEach(userIdentifiers, async (userIdentifier) => {
					let userInformation = await getUserInformation(userIdentifier);
					let { workProfile } = userInformation;
					let clients = Object.keys(workProfile);
					let clientName = clients[0];
					let { userData } = workProfile[clientName];
					let { product_name, salary, employeeId, doj } = userData;
					let clientData = await getClientInformation(clientName);
					await updateEligibility(
						userIdentifier,
						clientName,
						clientData,
						product_name,
						salary,
						employeeId,
						doj,
						previousDate
					);
				});
				return res.status(200).send({
					message: `Eligibility has been updated for Users ${userIdentifiers}`,
				});
			}
		} catch (error) {
			return res.status(404).send({});
		}
	}
}
export default new UserController();
