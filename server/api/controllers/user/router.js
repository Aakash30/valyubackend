import * as express from 'express';
import controller from './controller';
//const googleStorage = require('@google-cloud/storage');

// const storage = googleStorage({
//   projectId: "dev-valyu",
//   keyFilename: "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCrH4it9cGS3X7E\n8NB3ybnZ8QGtmoGCEXlV0wCWx+v6Wn1ntdv3PZHLBLUL7+OGs8mOzUMq3NqrJhRb\nKta4o5aNfTvP6p1P4jKtjhm4Ot+S6FVdJcJGfW2NhuCeLeBp9bbEDx8dBRBQhTnG\nWLI5s1TyyUwimxIAnDaVhB2Z46IWfleMu4kYvP6fPpu57UR4n/Mi8JUrsCUtTg2N\nhJFt0jNWUVxJLfht74tfFKOy5t6Xg4Ht5n4VeGgLCMyAXj+Dmun1PkkuRghtb56x\npKE3GwVcGDjlIdAGvb55lJ5izWWxy607yzAJU1nGnAUlFqiptihAptxD3pDbQc8V\n58FgeZxnAgMBAAECggEAP2gOUYMTyFL5I4klfZVKH5a9K/HplePapTHrcHNAnYHF\nINYSXNC8AlFKslMfZ+ybPSYSgsHV5n/UIBnF+SFoaiHitT42LwDeDU/veMObzwf2\nhW61xz+3ha7Z8NKH6kso/wmpWivjqSpMR5Ha7CVmTxXGd7Yk3AQF9629+CUQDKwF\nse2lCu7vYDU05Wk3cgAa/jWRhfxBdLIBiOtIli+NbrgaQBhgwzTwfDqSWoOqi4Gj\nIZNN+XhieFMdi1sNxOQvPuTZFqqy4BooKenxYKBhqjDwrnsGk3h0ybItgUD56dE0\ntg8wZK/72J8BPBndpcG2BlsxK64h6yn/aQxj9KfUsQKBgQDRXcc3if2lyc9ZloD9\nCcyl5wwOYibj29fENAqG0pXeYc4tUrQngapBn4haYrv1tCpWMRZwJ43vQnh8IZAB\na0EP/bD+oskVeTzVONwSFGY7ZG4pVRWyFLnLAAkE7kuL1e1yOcfUQslyREz1cUsj\n4Q77TZPTQEe86YKO5O2Ljnk6cQKBgQDRPRqGCnL5/A9kb5wcN+kulas7maPpXENM\nCzgVoOaN9B8719wxCjwLvi1KUatu/iex9TKw/11YR6NDyzsBJXXrHkRVopKEjwkj\nMfiEe2T8thSfP2HJfLEPkA2aW3Avlvain61XJhq+DmTyxgl/ofcCGwWBDFNN8gLy\nQLgJhZLAVwKBgQDKoPxiIEdA4WSwzBziyU0rc8YTVaNV/DuDROq7OayaogYtj1GU\nusXQ5VnOQKixUEF5UeDpgx63ZZm+aa058lfx5hIbH+mFwuTBaFp1lRCsgrKrhhGN\nXMnEUrBn+UlceRtp5colAM5+rOPe2Ftkyjj5t0dOuXO0vjsBmUbePnHNwQKBgQC1\nckeN9YG/RtYSQrcdwV51B1V2WcXKmDfDufboyp8keo6bVpWb8UQg6spbqHcByWSi\n7f8Hfmt0IqtyHssMM3AOdDDOEiz8xw0leDAKsvbgLwWzE0O5dmgVFta1BJZErqUy\nYUXwTRrjtZfU7+dMxydsrAIZ5pZXAYeMjJMLrsmUAQKBgG3chdiPtCUsC2EOLhPX\n8POVPW6ssuMxbarIf1ftiyayLNTSTUKMLOZJm9TuuU913+B49pZH3MZnxiAEttNA\nDSMXbM7VssAPbLznGkBdPDn8crQRNGL4gRjDUZFvNMfN+Qdqmvsou/1/U+dqow7L\n0L8Vff+tqm05VpsqIYFDcfZH\n-----END PRIVATE KEY-----\n"
// });
// will replace this key
// const bucket = storage.bucket("https://dev-valyu-df194.firebaseio.com/");

export default express
	.Router()
	.post('/', controller.create)
	.post('/image', controller.uploadFile)
	.post('/updateUserAndRetryLoan', controller.updateUserAndRunRetryLoans)
	.post('/updateEligibility', controller.updateEligibilityForUser)
	.post('/createUser', controller.createUser)
	.post('/:number', controller.update)
	.get('/restore', controller.restore)
	.get('/', controller.all)
	.get('/clientConfig', controller.generateClientConfig)
	.get('/:id', controller.byId);
