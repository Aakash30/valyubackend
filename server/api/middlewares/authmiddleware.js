const { model } = require('mongoose');

const instance = require('../../common/db').getInstance;

async function decodeIDToken(req, res, next) {
  if (req.headers?.authorization?.startsWith('Bearer ')) {
    const idToken = req.headers.authorization.split('Bearer ')[1];

    try {
      const decodedToken = await instance().auth().verifyIdToken(idToken);

      req['currentUser'] = decodedToken;
      if (decodedToken) {
        next();
      }
    } catch (err) {
      //console.log(err);

      res.status(400).send({ message: 'Authentication Failed' });
    }
  }
}
const rp = require('request-promise');

const getIdToken = async (uid) => {
  const customToken = await instance()
    .auth()
    .createCustomToken('MiIbuEVI2AVhZVjx7hCLQt9dba73');
  const res = await rp({
    url: `https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken?key=AIzaSyDfD36gSa1mOpH2VuiSoY5Wt-NU6bxcTc0`,
    method: 'POST',
    body: {
      token: customToken,
      returnSecureToken: true,
    },
    json: true,
  });
  console.log(res.idToken);
};
module.exports = {
  decodeIDToken,
  getIdToken,
};
