import l from './common/logger';
import jwt from 'jsonwebtoken';
import Config from '../functions/config/index.config';

function logErrors(err, req, res, next) {
	l.error(err);
	next(err);
}

// eslint-disable-next-line no-unused-vars
function errorHandler(err, req, res, next) {
	res.status(500).send({ error: err.message });
}

import userRouter from './api/controllers/user/router';
import loanRouter from './api/controllers/loan/router';
import transactionRouter from './api/controllers/transaction/router';
import cronJobToUpdateEligibility from '../functions/cron/cronJobToUpdateEligibility';

const getIdToken = require('./api/middlewares/authmiddleware').decodeIDToken;
const getToken = require('./api/middlewares/authmiddleware').getIdToken;
//getToken();
function authMiddleware(req, res, next) {
	try {
		let authHeader = req.headers?.authorization?.replace('Bearer ', '');

		if (!authHeader) {
			l.info('FORBIDDEN entry..');

			return res.status(403).json({
				status: 403,
				message: 'FORBIDDEN',
			});
		}

		const data = jwt.verify(authHeader, Config.SECRET_KEY);

		if (!data) {
			l.info('unauthorised entry..');
			return res.status(403).json({
				status: 401,
				message: 'UNAUTHRIZED',
			});
		}

		const { id, userName, name, _id } = data;

		res.locals.auth = {
			_id,
			id,
			name,
			userName,
		};

		next();
	} catch (err) {
		next(err);
	}
}
function authByKey(req, res, next) {
	let apiKey = req.headers['api-key'];
	req.body.currentEmployer = Config.APIKEYS[apiKey].client_name;
	req.body.product_name = Config.APIKEYS[apiKey].product_name;
	if (!req.body.currentEmployer) {
		return res.status(400).send({ message: 'Employer Configuraion not added' });
	}
	next();
}

function authByHeader(req, res, next) {
	let apiKey = req.headers['api-key'];
	if (Config.APIKEYS.global !== apiKey) {
		return res.status(401).send({ message: 'Unauthorized' });
	}
	next();
}

export default function routes(app) {
	//app.use('/api/v1/users-old', userRouter);
	//app.use('/api/v1/users', authMiddleware, userRouter); // TODO: add authentication

	//app.use('/api/v1/customer', userRouter);
	app.use('/customer', authByKey, userRouter);
	app.use('/loan', authByHeader, loanRouter);
	app.use('/transaction', authByHeader, transactionRouter);
	app.use('/runCronJob', authByHeader, cronJobToUpdateEligibility);
	app.use(logErrors);
	app.use(errorHandler);
}
