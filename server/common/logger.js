import pino from 'pino';
const Config = require('../../functions/config/index.config');

const l = pino({
  name: Config.APP_ID,
  level: Config.LOG_LEVEL,
});

export default l;
