import Express from 'express';
import * as path from 'path';
import errorHandler from '../api/middlewares/error.handler';
import { OpenApiValidator } from 'express-openapi-validator';
import Config from '../../functions/config/index.config';

export default function oas(app, routes) {
  const apiSpec = path.join(__dirname, 'api.yml');
  const validateResponses = !!(
    Config.OPENAPI_ENABLE_RESPONSE_VALIDATION &&
    Config.OPENAPI_ENABLE_RESPONSE_VALIDATION.toLowerCase() === 'true'
  );
  return new OpenApiValidator({
    apiSpec,
    validateResponses,
  })
    .install(app)
    .then(() => {
      app.use(Config.OPENAPI_SPEC || '/spec', Express.static(apiSpec));
      routes(app);
      app.use(errorHandler);
    });
}
