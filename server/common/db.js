//const mongoose = require('mongoose');

//import l from './logger';
let dbInstance;
const admin = require('firebase-admin');
const Config = require('../../functions/config/index.config');

//const key = require('./../firebase/service.json');
let serviceAccount = {
  type: Config.TYPE,
  project_id: Config.PROJECT_ID,
  private_key_id: Config.PROJECT_KEY_ID,
  private_key: Config.PRIVATE_KEY,
  client_email: Config.CLIENT_EMAIL,
  client_id: Config.CLIENT_ID,
  auth_uri: Config.AUTH_URI,
  token_uri: Config.TOKEN_URI,
  auth_provider_x509_cert_url: Config.AUTH_PROVIDER_X509_CERT_URL,
  client_x509_cert_url: Config.CLIENT_X509_CERT_URL,
};

const getInstance = () => {
  try {
    if (dbInstance) {
      return dbInstance;
    }

    dbInstance = admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      databaseURL: Config.DATABASE_URL,
      storageBucket: `gs://${Config.STORAGE_BUCKET}`,
    });

    return dbInstance;
  } catch (err) {
    // TODO: add a retry machanish here, or may be an alternative source of data
    //	l.error(err);
    return null;
  }
};

module.exports = {
  getInstance,
};
