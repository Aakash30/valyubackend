import Express from 'express';
import * as path from 'path';
import * as bodyParser from 'body-parser';
import * as http from 'http';
import * as os from 'os';
import cookieParser from 'cookie-parser';
import mongoSanitize from 'express-mongo-sanitize';
import helmet from 'helmet';
import xss from 'xss-clean';
const env = require('./env');

import oas from './oas';
let { getInstance } = require('../common/db');

import l from './logger';
import Config from '../../functions/config/index.config';

const app = new Express();
const formData = require('express-form-data');
const exit = process.exit;

//cron.dateChangesCron();

export default class ExpressServer {
	constructor() {
		const root = path.normalize(`${__dirname}/../..`);
		app.set('appPath', `${root}client`);
		app.use(formData.parse());
		app.use(bodyParser.json({ limit: Config.REQUEST_LIMIT || '100kb' }));

		/*
		app.use(
			bodyParser.urlencoded({
				extended: true,
				limit: Config.REQUEST_LIMIT || '100kb',
			})
		);
		*/
		// app.use(bodyParser.text({ limit: Config.REQUEST_LIMIT || '100kb' }));
		app.use(cookieParser(Config.SESSION_SECRET));

		app.use(mongoSanitize());
		app.use(helmet());
		app.use(xss());
		// Stoping static content serving for now
		app.use(Express.static(`${root}/public`));
	}

	router(routes) {
		//console.log(routes);
		this.routes = routes;
		return this;
	}

	listen(port = Config.PORT) {
		const welcome = (p) => () =>
			l.info(
				`up and running in ${
					Config.NODE_ENV || 'development'
				} @: ${os.hostname()} on port: ${p}}`
			);
		const db = getInstance();

		if (!db) {
			l.error(`Not able to connect to DB ... Please Varify again..`);
			exit(1);
		}

		oas(app, this.routes)
			.then(() => {
				http.createServer(app).listen(port, welcome(port));
			})
			.catch((e) => {
				l.error(e);
				exit(1);
			});

		return app;
	}
}
