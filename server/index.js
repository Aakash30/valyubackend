import './common/env';
import Server from './common/server';
import routes from './routes';
import Config from '../functions/config/index.config';

export default new Server().router(routes).listen(Config.PORT | 8080);
